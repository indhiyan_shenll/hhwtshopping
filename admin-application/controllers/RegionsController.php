<?php

class RegionsController extends AdminBaseController {

    private $canView;
    private $canEdit;
    private $admin_id;

    public function __construct($action) {
        
        $ajaxCallArray = array('listing', 'form', 'setup');
        if (!FatUtility::isAjaxCall() && in_array($action, $ajaxCallArray)) {
            die("Invalid Action");
        }
        $this->admin_id = AdminAuthentication::getLoggedAdminAttribute("admin_id");
        $this->canView = AdminPrivilege::canViewRegion($this->admin_id);
        $this->canEdit = AdminPrivilege::canEditRegion($this->admin_id);
        if (!$this->canView) {
            if (FatUtility::isAjaxCall()) {
                FatUtility::dieJsonError('Unauthorized Access!');
            }
            FatUtility::dieWithError('Unauthorized Access!');
        }
        parent::__construct($action);
        $this->set("canView", $this->canView);
        $this->set("canEdit", $this->canEdit);
    }

    public function index() {
        $this->set('search', $this->getSearchForm());
        $brcmb = new Breadcrumb();
        $brcmb->add("REGIONS");
        $this->set('breadcrumb', $brcmb->output());
        $this->_template->render();
    }

    private function getSearchForm() {
        $frm = new Form('frmSearch');
        $f1 = $frm->addTextBox('Name', 'region_name', '', array('class' => 'search-input'));
        $field = $frm->addSubmitButton('', 'btn_submit', 'Search', array('class' => 'themebtn btn-default btn-sm'));
        return $frm;
    }

    public function listing($page = 1) {
        $pagesize = static::PAGESIZE;
        $searchForm = $this->getSearchForm();
        $data = FatApp::getPostedData();
        $post = $searchForm->getFormDataFromArray($data);
        $search = Regions::getSearchObject();
        if (!empty($post['region_name'])) {
            $search->addCondition('region_name', 'like', '%' . $post['region_name'] . '%');
        }
        $page = empty($page) || $page <= 0 ? 1 : $page;
        $page = FatUtility::int($page);
        $search->setPageNumber($page);
        $search->setPageSize($pagesize);
        $rs = $search->getResultSet();
        $records = FatApp::getDb()->fetchAll($rs);
        $this->set("arr_listing", $records);
        $this->set('totalPage', $search->pages());
        $this->set('page', $page);
        $this->set('postedData', $post);
        $this->set('pageSize', $pagesize);
        $htm = $this->_template->render(false, false, "regions/_partial/listing.php", true, true);
        FatUtility::dieJsonSuccess($htm);
    }

    public function form() {
        if (!$this->canEdit) {
            FatUtility::dieJsonError('Unauthorized Access!');
        }
        $post = FatApp::getPostedData();

        $post['region_id'] = empty($post['region_id']) ? 0 : FatUtility::int($post['region_id']);
        $form = $this->getForm($post['region_id']);
        if (!empty($post['region_id'])) {
            $fc = new Regions($post['region_id']);
            if (!$fc->loadFromDb()) {
                FatUtility::dieWithError('Error! ' . $fc->getError());
            }
            $form->fill($fc->getFlds());
        }

        $adm = new Admin();
        $this->set("frm", $form);
        $htm = $this->_template->render(false, false, "regions/_partial/form.php", true, true);
        FatUtility::dieJsonSuccess($htm);
    }

    private function getForm($record_id = 0) {
        if (!$this->canEdit) {
            FatUtility::dieJsonError('Unauthorized Access!');
        }
        $action = 'Add';
        if ($record_id > 0) {
            $action = 'Update';
        }
        $frm = new Form('action_form', array('id' => 'action_form'));

        $frm->addHiddenField("", 'region_id', $record_id,array('id' => 'region_id'));
        $fld = $frm->addRequiredField('Name', 'region_name');
        $fld->developerTags['col'] = 4;
        $fld->setUnique('tbl_regions', 'region_name', 'region_id', 'region_id', 'region_id');
        $frm->addSelectBox('Status', 'region_active', Info::getStatus())->developerTags['col'] = 4;
        $frm->setFormTagAttribute('action', FatUtility::generateUrl("regions", "setup"));
        $frm->setFormTagAttribute('onsubmit', 'submitForm(formValidator,"action_form"); return(false);');
        $frm->addSubmitButton('', 'btn_submit', $action, array('class' => 'themebtn btn-default btn-sm'))->htmlAfterField = "<input type='button' name='cancel' value='Cancel' class='themebtn btn-default btn-sm' onclick='closeForm()'>";
        return $frm;
    }

    public function setup() {
	
        if (!$this->canEdit) {
            FatUtility::dieJsonError('Unauthorized Access!');
        }
        $frm = $this->getForm();
        $data = $frm->getFormDataFromArray(FatApp::getPostedData());
        if (false === $data) {
		
            FatUtility::dieWithError(current($frm->getValidationErrors()));
        }
        $countryId = FatApp::getPostedData('region_id', FatUtility::VAR_INT);
        unset($data['region_id']);
        $country = new Regions($countryId);
		
        $country->assignValues($data);

        if (!$country->save()) {
            FatUtility::dieWithError($country->getError());
        }
        $this->set('msg', 'Region Setup Successful');
        $this->_template->render(false, false, 'json-success.php');
    }

}
