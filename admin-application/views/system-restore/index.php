<?php defined('SYSTEM_INIT') or die('Invalid Usage'); ?> 
	<!--main panel start here-->

		<div class="fixed_container">
			<div class="row">
				<div class="col-sm-12">  
					<h1>System Restore Point</h1>  
					<section class="section">
                        <div class="sectionhead">
                            <h4>System Restore Point</h4>
                        </div>
						<div class="sectionbody space">
							<?php 
							$restore_point_frm->developerTags['fld_default_col'] = 12;
							echo $restore_point_frm->getFormHtml();
							?>	
						</div>
					</section>
				</div>          
			</div>          
		</div>          
	         
<style>
	.onoffswitch-checkbox {
		display: none;
	}
	.onoffswitch {
		-moz-user-select: none;
		position: relative;
		width: 90px;
	}
	.onoffswitch-label {
		border: 2px solid #999999;
		border-radius: 20px;
		cursor: pointer;
		display: block;
		overflow: hidden;
	}
	.onoffswitch-inner::before {
		background-color: #34a7c1;
		color: #ffffff;
		content: "ON";
		padding-left: 10px;
	}
	.onoffswitch-inner::before, .onoffswitch-inner::after {
		box-sizing: border-box;
		color: white;
		display: block;
		float: left;
		font-family: Trebuchet,Arial,sans-serif;
		font-size: 14px;
		font-weight: bold;
		height: 30px;
		line-height: 30px;
		padding: 0 0 0 10px;
		width: 50%;
	}
	.onoffswitch-inner::after {
		background-color: #eeeeee;
		color: #999999;
		content: "OFF";
		padding-right: 10px;
		text-align: right;
	}

	.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
		margin-left: 0;
	}
	.onoffswitch-inner {
		display: block;
		margin-left: -100%;
		transition: margin 0.3s ease-in 0s;
		width: 200%;
	}

	.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
		right: 0;
	}
	.onoffswitch-switch {
		background: #ffffff none repeat scroll 0 0;
		border: 2px solid #999999;
		border-radius: 20px;
		bottom: 0;
		display: block;
		margin: 6px;
		position: absolute;
		right: 56px;
		top: 0;
		transition: all 0.3s ease-in 0s;
		width: 18px;
	}
</style>