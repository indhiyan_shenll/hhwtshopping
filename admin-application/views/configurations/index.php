<?php
defined('SYSTEM_INIT') or die('Invalid Usage.');
?>

           
            <div class="fixed_container">
                <div class="row">
                  
                    <div class="col-sm-12">  
                        <h1>General Settings</h1>  
                        <div class="tabs_nav_container responsive flat">
                            
                            <ul class="tabs_nav">
                                <li><a class="active" rel="tabs_01" href="javascript:form(1)"> Emails And Site Info </a></li>
                                <li><a rel="tabs_02" href="javascript:form(2)"> Meta Settings</a></li>
                                <li><a rel="tabs_03" href="javascript:form(3)">  Social Sites Link</a></li>
                                <li><a rel="tabs_04" href="javascript:form(4)"> Third Party API Secrets</a></li>
                                <li><a rel="tabs_05" href="javascript:form(5)">General Settings</a></li>
                                <li><a rel="tabs_06" href="javascript:form(6)">Email Settings</a></li>
                                
                            </ul>
                            
                             <div class="tabs_panel_wrap">
                                        
									<!--tab1 start here-->
									<span class="togglehead active" rel="tabs_01">Emails And Site Info </span>
									<div id="tabs_01" class="tabs_panel">
											
									</div>
									<!--tab1 end here-->
                                    
                                    <!--tab2 start here-->
                                        <span class="togglehead" rel="tabs_02">Payment Settings</span>
                                        <div id="tabs_02" class="tabs_panel">
                                              
                                        </div>
									<!--tab2 end here--> 
									
									<!--tab3 start here-->
                                        <span class="togglehead" rel="tabs_03">MetaSettings</span>
                                        <div id="tabs_03" class="tabs_panel">
                                              
                                        </div>
                                    <!--tab3 end here-->
                                 
                                    <!--tab4 start here-->
                                        <span class="togglehead" rel="tabs_04">Social Sites Link</span>
                                        <div id="tabs_04" class="tabs_panel">
                                             
                                        </div>
                                    <!--tab4 end here-->
                                 
                                 
                                    <!--tab5 start here-->
                                        <span class="togglehead" rel="tabs_05">Third Party API Secrets</span>
                                        <div id="tabs_05" class="tabs_panel">
                                              
                                        </div>
                                    <!--tab5 end here-->
									
									<!--tab6 start here-->
                                        <span class="togglehead" rel="tabs_06">Email Settings</span>
                                        <div id="tabs_06" class="tabs_panel">
                                              
                                        </div>
                                    <!--tab6 end here-->
                             </div>      
                        
                        </div> 
                    
                    </div>
            </div>
        </div>  
        
        <!--main panel end here-->
        
        
