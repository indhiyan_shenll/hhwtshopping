<?php
defined('SYSTEM_INIT') or die('Invalid Usage.');
?>

    <div class="fixed_container">
        <div class="row">  
            <div class="col-sm-12">  
                <h1>Database Files</h1>   
                <section class="section">
                    <div class="sectionhead">
                        <h4>Files</h4> 
                    </div>


                    <div class="sectionbody">
                        <?php
                        $arr_flds = array(
                            'listserial' => 'Sr no.',
                            'name' => 'Name',
                            'created_date' => 'File Date',
                            'action' => 'Action',
                        );

                        $tbl = new HtmlElement('table', array('width' => '100%', 'class' => 'table table-responsive'));
                        $th = $tbl->appendElement('thead')->appendElement('tr');

                        foreach ($arr_flds as $val) {
                            $e = $th->appendElement('th', array(), $val);
                        }
                            
                        $sr_no=0;    
                        if (count($arr_listing) == 0) {
                            $tbl->appendElement('tr')->appendElement('td', array('colspan' => count($arr_flds)), 'No records found');
                        } else {
                            foreach ($arr_listing as $sn => $row) {
                                $sr_no++;
                                $tr = $tbl->appendElement('tr');

                                foreach ($arr_flds as $key => $val) {
                                    $td = $tr->appendElement('td');
                                    switch ($key) {
                                        case 'listserial':
                                            $td->appendElement('plaintext', array(), $sr_no);
                                            break;
                                        case 'action':
                                            $ul = $td->appendElement("ul", array("class" => "actions"));

                                            $li = $ul->appendElement("li");
                                            $li->appendElement('a', array('href' => "javascript:;", 'title' => 'Restore', "onclick" => ""), '<i class="ion-edit icon"></i>', true);
                                            break;
                                        default:
                                            $td->appendElement('plaintext', array(), $row[$key]);
                                            break;
                                    }
                                }
                            }
                        }
                        echo $tbl->getHtml();
                        ?>	
                    </div>
                </section>   
            </div>  
        </div>
    </div>

