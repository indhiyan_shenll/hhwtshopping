<?php

require_once dirname(__DIR__) . '/conf/conf.php';
require_once dirname(__DIR__) . '/public/application-top.php';

ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~E_DEPRECATED);

define('CRON_INIT', true);

function add_include_path($path)
{
    set_include_path(get_include_path() . PATH_SEPARATOR . $path);
}
add_include_path(CONF_INSTALLATION_PATH . 'library');
add_include_path(CONF_INSTALLATION_PATH . 'public/includes');

try
{
	$restoreEnabled = FatApp::getConfig('CONF_AUTO_RESTORE_ON', FatUtility::VAR_INT, 0);
	if($restoreEnabled  == 0)
	{
		throw new Exception('Please check configuration settings.');
	}
	
	$restoreObj = new fatRestore();
	
	if(!isset($_GET['passkey']) || $_GET['passkey'] != 'funaway-restore')
	{
		throw new Exception('Access denied!!');
	}
	
	$source = CONF_INSTALLATION_PATH."restore/user-uploads";
	$target = CONF_UPLOADS_PATH;
	if($restoreObj->full_copy($source,$target))
	{
		echo "Uploads folder restored.<br/>";
	}
	else
	{
		FatUtility::dieWithError($restoreObj->getError());
	}
	
	$file = CONF_INSTALLATION_PATH."restore/database/funaway-dbfile.sql";
	if($restoreObj->restoreDatabase($file,false))
	{
		echo "Database restored.<br/>";
	}
	else
	{
		FatUtility::dieWithError($restoreObj->getError());
	}
	
}
catch(Exception $e)
{
   echo $e->getMessage();
}