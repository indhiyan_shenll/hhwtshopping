(function($){
	$(document).ready(function() {
		/* Registration form validation */
		$("#hhwt-form-registration").validate({
	        rules:{
	            'register[user_email]':{
	                required: true,
	                email: true
	            },
	            'register[user_password]':{
	                required:true,
	                minlength:6
	            },
	            'register[user_login]':{
	                required:true
	            },
	            'register[first_name]':{
	                required:true
	            },
	            'register[last_name]':{
	                required:true
	            },
	            'register[user_country]':{
	            	required:true
	            }
	        },
	        messages:{
	            'register[user_email]':{
	                required: "Enter email",
	                email: "Enter valid email address"
	            },
	            'register[user_password]':{
	                required:"Enter password",
	                minlength:"password minlength 6"
	            },
	            'register[user_login]':{
	                required:"Enter username"
	            },
	            'register[firs_tname]':{
	                required:"Enter firstname"
	            },
	            'register[last_name]':{
	                required:"Enter lastname"
	            },
	            'register[user_country]':{
	            	required:"Enter country"
	            }
		    }
	    });
	    /* Login form validation */
	    $("#hhwt-form-login").validate({
	        rules:{
	            user_email:{
	                required:true,
	                email:true
	            },
	            user_password:{
	                required:true
	            }
	        },
	        messages:{
	            user_email:{
	                required:"Email is required",
	                email: "Enter valid email address"
	            },
	            user_password:{
	                required:"Password is required"
	            }
	        }
	    });

	    /* Forgot password validation */
		$("#hhwt-forget-password").validate({
			rules:{
				forgetemail:{
					required:true,
					email:true
				}
			},
			messages:{
				forgetemail:{
					required:"Email is required",
					email:"Please enter valid email"
				}
			}
	    });
	});
    // $("#signuptxt, #login-email, .hhwt-back, #forget-password, #hhwt-back-forget").on("click",function(){
    $(document).on("click","#signuptxt, #login-email, .hhwt-back, #forget-password, #hhwt-back-forget",function() {
        var clickEleID;
        if ($(this).hasClass("hhwt-back") === true ) {
            clickEleID = $(this).attr("class");
        } else {
            clickEleID = $(this).attr("id");
        }
        switch(clickEleID) {
            case "signuptxt":
                $(".hhwt-signup-form").show();
                $(".hhwt-signup-account").hide();
                break;
            case "login-email":
                $(".hhwt-login-form").show();
                $(".hhwt-signup-account").hide();
                break;
            case "hhwt-back":
                $(".hhwt-signup-form").hide();
                $(".hhwt-login-form").hide();
                $(".hhwt-signup-account").show();
                break;
            case "forget-password":
			    $(".hhwt-login-form").hide();
			    $(".hhwt-forget-form").show();
		        break;
		    case "hhwt-back-forget":
			    $(".hhwt-forget-form").hide();
			    $(".hhwt-login-form").show();
		        break;
		    default:
                // code block
        }
    });

    $('#hhwt-modal-signup.modal').on('hidden.bs.modal', function(){
		$(this).find("#hhwt-form-registration")[0].reset();
		$(this).find("#hhwt-form-login")[0].reset();
		$(this).find("#hhwt-forget-password")[0].reset();
		$(this).find("input").removeClass('error');
		$(".hhwt-signup-account").css('display','block');
		$(".hhwt-login-form, .hhwt-forget-form, .hhwt-signup-form, #hhwt-modal-signup.modal label").css('display','none');
	});

	$(".next").click(function(){
		var curStep = $(this).closest("fieldset");
		curStepBtn = curStep.attr("id");
		 if(curStepBtn=="Step1"){
			var validator = $( "#hhwt-form-registration" ).validate();
			var email     = validator.element("[name='register[user_email]']");
			var pass      = validator.element("[name='register[user_password]']");
			if(pass== false || email== false){
				return false;
			} else{
				$(".hhwt-signup-form #Step2").css("display" , "block");
				$(".hhwt-signup-form #Step1").css("display" , "none");
			}
		}
	});

	$(".prev").click(function(){
		$(".hhwt-signup-form #Step2").css("display" , "none");
		$(".hhwt-signup-form #Step1").css("display" , "block");
	});

	$(document).on("click", '.hhwt-trg-login', function(){		
		$('#hhwt-modal-signup').modal('show');
	});

	$(document).on("click", '.hhwt-mob-search', function(){
		$(".hhwt-navbar").find("#hhwt-mob-nav-search.collapse").toggleClass("in");
	});

	$(document).on("click",".carousel-control.left",function() {

		var parentElement = $(this).attr("data-parent");
		if (parentElement != '') {
			carouselSlidePrevNext(parentElement, 'prev');
		}
	    
	});

	$(document).on("click",".carousel-control.right",function() {
		
	    var parentElement = $(this).attr("data-parent");
		if (parentElement != '') {
			carouselSlidePrevNext(parentElement, 'next');
		}
	});

	function carouselSlidePrevNext(carouselElement, slideto) {
		$('#'+carouselElement).carousel(slideto);
		return true;
	}
}(jQuery));