<?php
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'conf-common.php';

define('CONF_APPLICATION_PATH', CONF_INSTALLATION_PATH . 'admin-application/');
define('CONF_THEME_PATH', CONF_APPLICATION_PATH . 'views/');
define('CONF_UTILITY_PATH', CONF_APPLICATION_PATH . 'utilities/');

define('CONF_USER_UPLOADS_PATH', CONF_INSTALLATION_PATH . 'user-uploads/');
define('CONF_DB_BACKUP_DIRECTORY', 'database-backups');
define('CONF_DB_BACKUP_DIRECTORY_FULL_PATH', CONF_USER_UPLOADS_PATH . CONF_DB_BACKUP_DIRECTORY.'/');

define('CONF_WEBROOT_URL', (strlen(CONF_BASE_DIR) > 0 ? CONF_BASE_DIR : '').'admin/');
define('CONF_WEBROOT_URL_TRADITIONAL', '/'.CONF_ROOT_DIR.'/public/admin.php?url=');

define('CONF_USE_FAT_CACHE', true);
define('CONF_FAT_CACHE_DIR', CONF_INSTALLATION_PATH . 'public/cache/');
define('CONF_FAT_CACHE_URL', 'cache/');
define('CONF_DEF_CACHE_TIME', 77760000); // in seconds (77760000 = 30 days)