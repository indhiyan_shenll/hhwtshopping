<?php
/**
 *
 * General configurations
 */

define('CONF_DEVELOPMENT_MODE', false);
define('CONF_LIB_HALDLE_ERROR_IN_PRODUCTION', true);
define ('CONF_URL_REWRITING_ENABLED', true);
define ('CONF_DATE_FORMAT_MYSQL', 'YYYY-mm-dd');
define('PASSWORD_SALT', 'ewoiruqojfklajreajflfdsaf');

// if( $_SERVER['SERVER_NAME'] == 'staging.havehalalwilltravel.net' ) {
if( $_SERVER['SERVER_NAME'] == '13.229.104.139' ) {

    define('CONF_DB_SERVER', 'halaldb.cp1u9dl2s50g.ap-southeast-1.rds.amazonaws.com');
    define('CONF_DB_USER', 'funawaystg');
    define('CONF_DB_PASS', 'Fun@w@Y$tg17');
    define('CONF_DB_NAME', 'funaway');
    define('CONF_ROOT_DIR', 'shopping');
    define('SERVER_PROTOCOL', 'https://');
    define('USER_REGISTER_API_URL', 'http://34.207.104.235:8080/api/register');
    define('USER_LOGIN_API_URL', 'http://34.207.104.235:8080/api/login');     

} else if ($_SERVER['SERVER_NAME'] == 'localhost') {

	define('CONF_DB_SERVER', 'localhost');
    define('CONF_DB_USER', 'root');
    define('CONF_DB_PASS', '');
    define('CONF_DB_NAME', 'funaway');
    define('CONF_ROOT_DIR', 'hhwtshopping');
    define('SERVER_PROTOCOL', 'http://');
    define('USER_REGISTER_API_URL', 'http://34.207.104.235:8080/api/register');
    define('USER_LOGIN_API_URL', 'http://34.207.104.235:8080/api/login');
} else {
    die('configurations not set for live environment');
}

define ( 'CONF_INSTALLATION_PATH', dirname ( dirname ( __FILE__ ) ) . DIRECTORY_SEPARATOR );
define ( 'CONF_UPLOADS_PATH', CONF_INSTALLATION_PATH . 'user-uploads' . DIRECTORY_SEPARATOR );
// funaway-test.4demo.biz
if ($_SERVER ['SERVER_NAME'] == 'funaway-test.4demo.biz') {
	define ( 'CONF_CORE_LIB_PATH', '/etc/fatlib/' );
} else {
	define ( 'CONF_CORE_LIB_PATH', CONF_INSTALLATION_PATH . 'library/core/' );
}

// // Added Indhiyan Oct 25, 2017
// define('CONF_ROOT_DIR', 'testfunaway');
// define('CONF_SYSTEM_BASE_DIR', CONF_ROOT_DIR.'/');
// define('CONF_WEBROOT_URL', '/'.CONF_ROOT_DIR.'/');
// define('CONF_WEBROOT_URL_TRADITIONAL', CONF_WEBROOT_URL . 'public/index.php?url=');


//define('CONF_DB_TABLE_CACHE_PATH', CONF_INSTALLATION_PATH . 'user-uploads/db-def/');
$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
// define('CONF_BASE_URL', $protocol.$_SERVER['SERVER_NAME'].CONF_WEBROOT_URL);
define('CONF_BASE_DIR', '/'.CONF_ROOT_DIR.'/');

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'cache-constants.php';