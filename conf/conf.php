<?php

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'conf-common.php';

define('CONF_APPLICATION_PATH', CONF_INSTALLATION_PATH . 'application/');
define('CONF_THEME_PATH', CONF_APPLICATION_PATH . 'views/');

define('SYSTEM_FRONT', TRUE);

// Commented Indhiyan Oct 25, 2017
// define('CONF_WEBROOT_URL', '/');
// define('CONF_WEBROOT_URL_TRADITIONAL', CONF_WEBROOT_URL . 'public/index.php?url=');

// Added Indhiyan Oct 25, 2017
// define('CONF_ROOT_DIR', 'testfunaway');
define('CONF_SYSTEM_BASE_DIR', CONF_ROOT_DIR.'/');
define('CONF_WEBROOT_URL', '/'.CONF_ROOT_DIR.'/');
define('CONF_WEBROOT_URL_TRADITIONAL', CONF_WEBROOT_URL . 'public/index.php?url=');


if (CONF_URL_REWRITING_ENABLED){
    define('CONF_USER_ROOT_URL', CONF_WEBROOT_URL);
}
else {
    define('CONF_USER_ROOT_URL', CONF_WEBROOT_URL . 'public/');
}

define('CONF_USE_FAT_CACHE', true);
define('CONF_FAT_CACHE_DIR', CONF_INSTALLATION_PATH . 'public/cache/');
// define('CONF_FAT_CACHE_URL', '/'.CONF_ROOT_DIR.'/cache/');
define('CONF_FAT_CACHE_URL', '/'.CONF_ROOT_DIR.'/public/cache/');
define('CONF_DEF_CACHE_TIME', 77760000); // in seconds (77760000 = 30 days)