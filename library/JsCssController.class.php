<?php


require_once("minify/vendor/autoload.php");

class JsCssController {

    private function checkModifiedHeader() {
        $headers = FatApp::getApacheRequestHeaders();
        if (isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) == $_GET['sid'])) {
            header('Cache-Control: public, max-age=2592000, stale-while-revalidate=604800');
            header("Expires: " . date('D, d M Y H:i:s', strtotime("+30 Day")));
            header('Last-Modified: ' . date('D, d M Y H:i:s', $_GET['sid']), true, 304);
            exit;
        }
    }

    private function setHeaders($contentType) {
        header('Content-Type: ' . $contentType);
        header('Cache-Control: public, max-age=2592000, stale-while-revalidate=604800');
        header("Expires: " . date('D, d M Y H:i:s', strtotime("+30 Day")));
        if (isset($_GET['sid'])) {
            header('Last-Modified: ' . date('D, d M Y H:i:s', $_GET['sid']));
        }

        if (!in_array('ob_gzhandler', ob_list_handlers())) {
            if (substr_count($_SERVER ['HTTP_ACCEPT_ENCODING'], 'gzip')) {
                ob_start("ob_gzhandler");
            } else {
                ob_start();
            }
        }
    }

    function css() {
		
        $this->checkModifiedHeader();

        $this->setHeaders('text/css');

        $arr = explode(',', $_GET['f']);
	
        $str = '';
		$minifier = new \MatthiasMullie\Minify\CSS();
        foreach ($arr as $fl) {
            if (substr($fl, '-4') != '.css')
                continue;
            $file = CONF_THEME_PATH . $fl;
		
			
            if (file_exists($file)){
				$minifier->add($file);
			}
              
        }

    $content =  $minifier->minify();
			$cacheKey = $_SERVER['REQUEST_URI'];
			 FatCache::set($cacheKey, $content, '.css');
			 echo $content;

    }

    function cssCommon() {
		
        $this->checkModifiedHeader();

       $this->setHeaders('text/css');
		$minifier = new \MatthiasMullie\Minify\CSS();
        if (isset($_GET['f'])) {
            $files = $_GET['f'];
			
			$arr = explode(',', $files);

			$str = '';
			foreach ($arr as $fl) {
				if (substr($fl, '-4') != '.css')
					continue;
				$file = CONF_THEME_PATH . 'common-css' . DIRECTORY_SEPARATOR . $fl;
				if (file_exists($file))
					$minifier->add($file);
			}
			
			
        } else {
            $pth = CONF_THEME_PATH . 'common-css';
            $dir = opendir($pth);
            $last_updated = 0;
           

            $arrCommonfiles = scandir($pth, SCANDIR_SORT_ASCENDING);
			
            foreach ($arrCommonfiles as $fl) {
                if (!is_file($pth . DIRECTORY_SEPARATOR . $fl))
                    continue;
                if ('.css' != substr($fl, -4))
                    continue;
                if ('noinc-' == substr($fl, 0, 6))
                    continue;
			
                $minifier->add($pth . DIRECTORY_SEPARATOR . $fl);
            }
        }


			$content =  $minifier->minify();
			$cacheKey = $_SERVER['REQUEST_URI'];
			 FatCache::set($cacheKey, $content, '.css');
			 echo  $content;
    }

    function js() {
        $this->checkModifiedHeader();

        $this->setHeaders('application/javascript');

        $arr = explode(',', $_GET['f']);

        $str = '';
			$minifier = new \MatthiasMullie\Minify\JS();
        foreach ($arr as $fl) {
            if (substr($fl, '-3') != '.js')
                continue;
            if (file_exists(CONF_THEME_PATH . $fl))
                $filePath = CONF_THEME_PATH . $fl;
			     $minifier->add($filePath);
        }
		
             $content =  $minifier->minify();
			$cacheKey = $_SERVER['REQUEST_URI'];
			 FatCache::set($cacheKey, $content, '.js');
			 echo $content;
    }

    function jsCommon() {
        $this->checkModifiedHeader();

        $this->setHeaders('application/javascript');
		$minifier = new \MatthiasMullie\Minify\JS();

        if (isset($_GET['f'])) {
            $files = $_GET['f'];
			
			  $arr = explode(',', $files);

			$str = '';
			foreach ($arr as $fl) {
				if (substr($fl, '-3') != '.js')
					continue;
				if (file_exists(CONF_THEME_PATH . 'common-js' . DIRECTORY_SEPARATOR . $fl))
					$minifier->add(CONF_THEME_PATH . 'common-js' . DIRECTORY_SEPARATOR . $fl);
			}
			
        } else {
            $pth = CONF_THEME_PATH . 'common-js';
            $dir = opendir($pth);
            $last_updated = 0;
            $files = '';
            $arrCommonfiles = scandir($pth, SCANDIR_SORT_ASCENDING);
            foreach ($arrCommonfiles as $fl) {
                if (!is_file($pth . DIRECTORY_SEPARATOR . $fl))
                    continue;
                if ('.js' != substr($fl, -3))
                    continue;
                if ('noinc-' == substr($fl, 0, 6))
                    continue;
				$minifier->add($pth . DIRECTORY_SEPARATOR . $fl);
            }
        }

	        $content =  $minifier->minify();
		
			$cacheKey = $_SERVER['REQUEST_URI'];
			 FatCache::set($cacheKey, $content, '.js');
			 echo $content;
    }

}
