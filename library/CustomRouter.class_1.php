<?php

class CustomRouter
{
	static function setRoute(&$controller, &$action, &$queryString)
	{
		if(defined('SYSTEM_FRONT') && SYSTEM_FRONT === true && !FatUtility::isAjaxCall())
		{
			
			if(in_array($controller,array('island', 'destinations')))
			{
				$controller ='island';
				if(!in_array($action, array('activities', 'lists','listing'))){
					array_unshift($queryString, $action);
					$action='index';
				}
				return;
			}
			if(in_array($controller,array('services', 'experiences')))
			{
				$controller ='services';
				if(!in_array($action, array('listing','sub-service', 'get-activities'))){
					array_unshift($queryString, $action);
					$action='index';
				}
				return;
			}
			
			$url = $_SERVER['REQUEST_URI'];
			
			$url = substr($url, strlen(CONF_WEBROOT_URL));
			
			$url = rtrim($url, '/');
			
			$srch = UrlRewrite::search();
			
			$srch->addCondition(UrlRewrite::DB_TBL_PREFIX . 'custom', 'LIKE', preg_replace('/_/', '\_', $url));
			
			$rs = $srch->getResultSet();
			
			if ( ! $row = FatApp::getDb()->fetch($rs) )
			{
				if(!empty($controller)){
					$cms = new Cms();
					$cms_data = $cms->getCmsBySlug($controller);
					
					if(!empty($cms_data)){
						array_unshift($queryString, $controller);
						$controller ='cms';
						$action ='view';
						if($cms_data['cms_type'] == 1){
							$action ='terms';
						}
					}
					return;
				}
			
				if ($controller == '') $controller = 'home';
			
				if ($action == '') $action = 'error404';
				return;
			}

			$url = $row['url_rewrite_original'];
			
			$arr = explode('/', $url);
			$controller = (isset($arr[0]))?$arr[0]:'';
			array_shift($arr);
			
			$action = (isset($arr[0]))?$arr[0]:'';
			array_shift($arr);
			
			$queryString = $arr;
			
			if ($controller != '' && $action == '')
			{
				$action = 'index';
			}
			
			if ($controller == '') $controller = 'home';
			
			if ($action == '') $action = 'error404';
			return;
		}
	}
}

//Any logic to modify the controller, action and querystring.
/* if(in_array($controller,array('contact-us', 'contactUs','contactus.html','contacts'))){
	$controller ='cms';
	$action='contactUs';
} */
/* if(in_array($controller,array('hosts'))){ 10
	$controller ='guestUser';
	$action='hostForm';
} */
/* if(in_array($controller,array('host-help', 'hostHelp'))){ 9
	$controller ='cms';
	$action='hostHelp';
} */
/* if(in_array(strtolower($controller),array('cancellation-policy','cancellationpolicy')) && in_array(strtolower($action),array('traveler','host'))){ // 7,8
	$controller ='cancellationPolicy';
	$queryString[0] = $action;
	$action='index';
} */
/* if(in_array($controller,array('terms'))){
	$queryString[0] = $action;
	$controller ='cms';
	$action='terms';
	
}
if(in_array($controller,array('privacy'))){
	$controller ='cms';
	$action='view';
	$queryString[0] = 'privacy';
} */
/* if(in_array($controller,array('partnerships'))){
	$controller ='cms';
	$action='view';
	$queryString[0] = 'partnerships';
} */
/* if(in_array($controller,array('cancellation'))){ //5
	$controller ='cms';
	$action='view';
	$queryString[0] = 'cancellation';
} */
/* if(in_array($controller,array('careers'))){ //4
	$controller ='cms';
	$action='view';
	$queryString[0] = 'careers';
} */

/* if(in_array($controller,array('about-us', 'aboutUs','aboutus.html'))){
	$controller ='cms';
	$action='aboutUs';
} */

/* if(in_array(strtolower($controller),array('Help', 'help','faq'))){ //2
	$controller ='cms';
	$action='help';
} */