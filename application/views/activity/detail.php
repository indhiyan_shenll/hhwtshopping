<script src='https://api.mapbox.com/mapbox.js/v2.4.0/mapbox.js'></script>
<link href="https://api.mapbox.com/mapbox.js/v2.4.0/mapbox.css" rel="stylesheet">
<main id="MAIN" class="site-main site-main--light activity-page">
    <div class="container-fluid activity-banner-fluid">
        <div class="container activity-banner-container">
            <div class="activity-banner-title">
                <h1>Discover experiences curated for Muslim travelers</h1>
            </div>
        </div>
    </div>
    <div class="container-fluid activity-menu-fluid">
        <div class="container hhwt-single-category">
            <div class="col-md-6 col-sm-12 col-xs-12 hhwt-destinations-category">
                <p>
                    <a href="javascript:void(0)">Home</a>
                    <span class="fa fa-angle-right icon-right"></span>
                    <a href="javascript:void(0)">Tokyo</a>
                    <span class="fa fa-angle-right icon-right"></span>
                    <a href="javascript:void(0)">Photoshoot in Tokyo Hidden Spots</a>
                </p>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 city-experience-menu">
                <div class="col-md-6 col-sm-12 col-xs-12 hhwt-city-menu ">
                    <a href="javascript:void(0)">Highlights</a>
                    <a href="javascript:void(0)">Details</a>
                    <a href="javascript:void(0)">Directions</a>
                    <a href="javascript:void(0)">Host</a>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 hhwt-social-share-btn ">
                    <button type="button" class="save-bucket hhwt-save-button"><i class="fa fa fa-heart-o" aria-hidden="true"></i> Save</button>
                    <button type="button" class="fb-btn" ><i class="fa fa-facebook" aria-hidden="true"></i></button>
                    <button type="button" class="tw-btn"><i class="fa fa-twitter" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="container activity-content-cotainer">
        <div class="activity-content col-sm-8">
            <h1>Photo Shoot in Tokyo Hidden Spots</h1>
            <p class="main-rating">4.3<img src="../images/star.png"></p>
            <h3 class="main-price">s$300</h3>
            <p class="main-count">per person</p>
            <h3>Activity Highlights</h3>
            <p>I will show you some of my favorite spots in empty morning of Harajuku/Jingumae area. This is where I often shoot my photo works, so I know in and outs of neighborhood such as galleries, local cafes and small back alleys.</p>
            <h3>Activity Description</h3>
            <p>Harajku has an image of pop and touristic area, but there are some hidden places which convey old and traditional feelings. I will take pictures of you while guiding you though my Tokyo. You can enjoy exploring new places without being distracted by trying to capture everything you see.</p>
            <h3>About Tour Host</h3>
            <div class="activity-host-content">
                <img class="author-image" src="../images/01.png">
                <div class="activity-host-detail">
                    <span><a href="javascript:void(0)">Julia</a></span>
                    <p>An Adventure enthusiast myself, I love to do new adventure activities and also make other feel the adrenaline.<p>
                    <a class="ask-host" href="javascript:void(0)">Ask Question</a>
                </div>
            </div>
            <h3>Details</h3>
            <table class="activity-table-details">
                <tr>
                    <td>Duration</td>
                    <td>2.5 hours</td>
                </tr>
                <tr>
                    <td>Cancellation</td>
                    <td>can be cancelled and fully refunded within 24 hours of ppurchase</td>
                </tr>
                <tr>
                    <td>max participants</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>Languages</td>
                    <td>Chinese, English, Japanese</td>
                </tr>
            </table>
            <h3>Notes</h3>
            <p>Please note this experience is weather dependent. In case of bad weather, there is a chance for cancellation and full refund.</p>
            <h3>Inclusion</h3>
            <ul>
                <li>Drinks at cafe</li>
                <li>Photos</li>
                <li>25 retouched photos</li>
            </ul>
            <div class="activity-location">
                <h3>Directions</h3>
                <p>We will meet at Shinjuku Station</p>
                <iframe frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Istanbul Saray,&amp;center=35.0095273,135.7672722&amp;key=AIzaSyBFQYEpr5jlXE4DS-Y_GN3f39D3RJroarI&amp;zoom=12"></iframe>
            </div>
            <div class="activity-review-section">
                <h3>Reviews</h3>
                <div class="activity-review">
                    <img class="author-image" src="../images/01.png">
                    <div class="activity-review-details">
                        <p class="author-name">elainetee</p>
                        <span class="review-time">3 weeks ago</span>
                        <img class="author-rating" src="../images/star.png">
                        <p class="review-title">Consectetur adipisning</p>
                        <p class="review-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit ne omnis dolorum delicatissimi, vix ne ilium elitr graecis. Sea an impedit definitionem, putant melius docendi ne per. Sonet putant pri te, has possit ancillae ei.</p>
                    </div>
                </div>
                <div class="activity-review">
                    <img class="author-image" src="../images/01.png">
                    <div class="activity-review-details">
                        <p class="author-name">elainetee</p>
                        <span class="review-time">3 weeks ago</span>
                        <img class="author-rating" src="../images/star.png">
                        <p class="review-title">Consectetur adipisning</p>
                        <p class="review-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit ne omnis dolorum delicatissimi, vix ne ilium elitr graecis. Sea an impedit definitionem, putant melius docendi ne per. Sonet putant pri te, has possit ancillae ei.</p>
                    </div>
                </div>
                <a class="view-review" href="javascript:void(0)">View All Reviews</a>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 hhwt-activity-booking">
            <h1>Follow the default design for booking dates</h1>
        </div>
    </div>
    <div class="container-fluid hhwt-destination-fluid hhwt-activity-slider">
        <div class="container hhwt-destination-container">
            <h4 class="col-sm-10 col-xs-8 hhwt-destination-title">Other Experiences in Tokyo</h4>
            <button class="btn hhwt-destination-btn col-sm-2">View More<span class="fa fa-chevron-right"></span></button>
        </div>
        <div id="hhwt-post" class="carousel slide" data-ride="carousel">
            <div class="custom-container carousel-inner" role="listbox">
                <div class="item active">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">11 Days Tokyo-Kyoto with Trekking in Jap</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$300</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>80 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/01.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">Beyond Tokyo:Roof of Japan Experience</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$90</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>91 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/02.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Tokyo Minakami Onsan F&E</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>70 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/03.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Basic Tokyo Mt Fuji & Disney Fun</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>102 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/04.png" class="img-responsive thumb">
                        </article>
                    </div>
                </div>
                <div class="item">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">11 Days Tokyo-Kyoto with Trekking in Jap</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$300</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>80 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/01.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">Beyond Tokyo:Roof of Japan Experience</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$90</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>91 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/02.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Tokyo Minakami Onsan F&E</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>70 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/03.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Basic Tokyo Mt Fuji & Disney Fun</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../../images/star.png">
                                        <p>102 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/04.png" class="img-responsive thumb">
                        </article>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#hhwt-post" role="button" data-slide="prev">
                <span class="fa fa-chevron-left" aria-hidden="true"></span>
            </a>
            <a class="right carousel-control" href="#hhwt-post" role="button" data-slide="next">
                <span class="fa fa-chevron-right" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</main>
<div id="send-msg" style="display:none;"></div>
<div id="write-review" style="display:none;"></div>
<div id='more-review' style="display:none;" ></div>
<div id='activity-abuse' style="display:none;" ></div>
<script>
    var mapbox_access_token = '<?php echo FatApp::getConfig('mapbox_access_token') ?>';
    var city_id = <?php echo FatUtility::int($cityId) ?>;
    var activityMemberCount = '<?php echo $activity['activity_members_count']; ?>';
    function facebookTrackEvent() {
<?php echo TrackingCode::getTrackingCode(1); ?>
    }
    function facebookWishListTrack() {
<?php echo TrackingCode::getTrackingCode(3); ?>
    }
    $('.gallery').modaal({
        type: 'image'
    });
    $('.video-gallery').modaal({
        type: 'video',
        animation: 'fade',
        before_image_change: function (current_item, incoming_item) {
            $(current_item).find('iframe').attr('src', '');
            var nextLink = $(incoming_item).find('iframe').attr('data-src');
            $(incoming_item).find('iframe').attr('src', nextLink);

        },
    });
    function openGallery() {
        $('.gallery:eq(0)').click();

    }
    function openVideoGallery() {
        $('.video-gallery:eq(0)').click();

    }

    showMap(<?php echo $lat ?>,<?php echo $long ?>);

</script>