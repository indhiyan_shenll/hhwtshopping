<?php
defined('SYSTEM_INIT') or die('Invalid Usage.');

$frm->setFormTagAttribute('action', Route::getRoute('search'));
$frm->setFormTagAttribute('class', 'form form--vertical form--default');
$frm->setFormTagAttribute('method', 'get');

$submit_btn = $frm->getField('search');
$submit_btn->developerTags['noCaptionTag'] = true;
$submit_btn->setFieldTagAttribute('class', 'button button--fit button--fill button--primary no--margin-bottom');
$submit_btn->developerTags['col'] = 2;
$submit_btn->addWrapperAttribute('class', 'span span--2');
?>
<main id="MAIN" class="site-main--light">
    <?php #echo Helper::fat_shortcode("[fat_contactinfo]"); ?>
    <?php #echo Helper::fat_shortcode("[fat_offices]"); ?>
    <?php #echo Helper::fat_shortcode("[fat_contactform]"); ?>
    <?php if (!empty($banners)) { ?>
        <header>
            <div class="container-fluid hhwt-featured-fluid">
                <div class="container hhwt-featured-container">
                    <div class="hhwt-featured-title">
                        <h1>Discover experiences curated for Muslim travelers</h1>
                        <div class="hhwt-searchmenu">
                            <?php echo $frm->getFormTag(); ?>
                            <p class="dropdown-search input-group search-act">
                                <?php echo $frm->getFieldHTML('keyword'); ?>
                                <span class="search-span btn"><?php echo $frm->getFieldHTML('search'); ?></span>
                                <!-- <input class="form-control seachbox" placeholder="    Search for experiences" type="text"> <span class="search-span btn">Search</span> -->
                            </p>
                            </form><?php echo $frm->getExternalJS(); ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid hhwt-widget-fluid">
                <div class="container hhwt-widget-container">
                    <div class="col-md-12 col-sm-12 col-xs-12 hhwt-widget">
                        <div class="col-md-3 col-sm-3 col-xs-12 hhwt-small-widget">
                            <img src="images/friendly_grey.png">
                            <h3>Muslim friendly</h3>
                            <p>We've done our research so that you can focus on picking your perferred expriences!</p>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 hhwt-small-widget">
                            <img src="images/community_grey.png">
                            <h3>Community-driven</h3>
                            <p>View ratings and reviews by travellers just like you.</p>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 hhwt-small-widget">
                            <img src="images/safely_grey.png">
                            <h3>Transact Safely</h3>
                            <p>Book and make payments safely via our platform.</p>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 hhwt-small-widget">
                            <img src="images/global_grey.png">
                            <h3>Gobal Outreach</h3>
                            <p>You too can create experiences for travellers across the globe!</p>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <?php
    }
    ?>
    <div class="site-main__body">
        <!-- Desinations -->
        <?php if($destinations):?>            
                <div class="container-fluid hhwt-destination-fluid">
                    <div class="container hhwt-destination-container">
                        <h4 class="col-sm-10 col-xs-8 hhwt-destination-title">Destinations</h4>
                        <button class="btn hhwt-destination-btn col-sm-2">View All<span class="fa fa-chevron-right"></span></button>
                    </div>
                    <div id="destinations" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <?php 
                            $i = 1;
                            foreach ($destinations as $city) {
                                $item_active = "";
                                if ($i == 1)
                                    $item_active = "active"; 

                                if ($i % 6 == 1) { ?>
                                    <div class="item <?php echo $item_active; ?>">
                                <?php } ?>                    
                                    <div class="col-md-4 col-sm-4 col-xs-6 hhwt-modern-medium">
                                        <a href="<?php echo Route::getRoute('city', 'details', array($city['city_id'])); ?>" title="<?php echo $city['city_name'] ?>"><img class="lazy" data-original="<?php echo FatUtility::generateUrl('image', 'cityRandom', array($city['city_id'], 579, 434)); ?>" width="579" height="434" alt="<?php echo $city['city_name'] ?>" title="<?php echo $city['city_name'] ?>"></a>                                        
                                        <header>
                                            <span class="category"><a href="javascript:void(0)"><?php echo $city['country_name'] ?></a></span>
                                            <h3><a href="javascript:void(0)"><?php echo $city['city_name'] ?></a></h3> 
                                        </header>
                                    </div>
                                <?php if ($i % 6 == 0) { ?>
                                    </div>
                                <?php } 
                                $i++;
                                } 
                                //This is to ensure there is no open div if the number of elements in user_kicks is not a multiple of 6
                                if ($i % 6 != 1) echo "</div>";
                            ?>
                            
                        </div>
                        <a class="left carousel-control" role="button" data-slide="prev" data-parent="destinations">
                            <span class="fa fa-chevron-left" aria-hidden="true"></span>
                        </a>
                        <a class="right carousel-control"role="button" data-slide="next" data-parent="destinations">
                            <span class="fa fa-chevron-right" aria-hidden="true"></span>
                        </a>
                    </div>
                </div>        
        <?php endif;?>

        <!-- Featured Experiences -->
        <?php if($featured_experiences):?>
        <div class="container-fluid hhwt-destination-fluid">
            <div class="container hhwt-destination-container">
                <h4 class="col-sm-10 col-xs-8 hhwt-destination-title">Featured Experiences</h4>
                <button class="btn hhwt-destination-btn col-sm-2">View More<span class="fa fa-chevron-right"></span></button>
            </div>
            <div id="hhwt-post" class="carousel slide" data-ride="carousel">
                <div class="custom-container carousel-inner" role="listbox">
                    <?php 
                    $i = 1;
                    foreach ($featured_experiences as $experience) {

                        $item_active = "";
                        if ($i == 1)
                            $item_active = "active"; 

                        if ($i % 4 == 1) { ?>
                            <div class="item <?php echo $item_active; ?>">
                        <?php } ?>  
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <article class="halal-post simple-small">
                                    <header>
                                        <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                        <h3><a href="<?php echo Route::getRoute('activity','detail',array($experience['activity_id'])) ?>"><?php echo Info::subContent($experience['activity_name'], 100) ?></a></h3>
                                        <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                            <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                                <h3><?php echo Currency::displayPrice($experience['activity_display_price']); ?></h3>
                                                <p><?php echo Info::activityTypeByKey($experience['activity_price_type']); ?></p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                                <?php
                                                $rating = 0;
                                                if($experience['ratingcounter']){
                                                    $rating = $experience['rating']/$experience['ratingcounter'];
                                                    
                                                }     
                                                $rating = 3;                                           
                                                echo Info::rating($rating, false, 'rating--lightest rating-small') ?>
                                                <p><?php echo $experience['reviews'] ?> <?php echo Info::t_lang('REVIEWS ') ?></p>
                                            </div>
                                        </div>
                                    </header>
                                    <a href="<?php echo Route::getRoute('activity','detail',array($experience['activity_id'])); ?>" title="<?php echo Info::subContent($experience['activity_name'], 100) ?>">
                                        <img src="<?php echo FatUtility::generateUrl('image', 'activity', array($experience['activity_image_id'], 600, 450)) ?>" alt="<?php echo Info::subContent($experience['activity_name'], 100) ?>" class="img-responsive thumb">
                                    </a>
                                </article>
                            </div>
                    <?php if ($i % 4 == 0) { ?>
                            </div>
                        <?php } 
                        $i++;
                        } 
                        //This is to ensure there is no open div if the number of elements in user_kicks is not a multiple of 6
                        if ($i % 4 != 1) echo "</div>";?>
                </div>
                <a class="left carousel-control" role="button" data-slide="prev" data-parent="hhwt-post">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                </a>
                <a class="right carousel-control" role="button" data-slide="next" data-parent="hhwt-post">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                </a>
            </div>            
        </div>
        <?php endif;?> 

        <!-- Popular Experiences -->
        <?php if($popular_experiences):?>
        <!-- Popular Experiences -->
        <div class="container-fluid hhwt-destination-fluid">
            <div class="container hhwt-destination-container">
                <h4 class="col-sm-10 col-xs-8 hhwt-destination-title">Popular Experiences</h4>
            </div>
            <div id="popular-experiences" class="carousel slide" data-ride="carousel">
                <div class="custom-container carousel-inner" role="listbox">
                    <?php 
                    $i = 1;
                    foreach ($featured_experiences as $experience) {

                        $item_active = "";
                        if ($i == 1)
                            $item_active = "active"; 

                        if ($i % 4 == 1) { ?>
                            <div class="item <?php echo $item_active; ?>">
                        <?php } ?>  
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <article class="halal-post simple-small">
                                    <header>
                                        <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                        <h3><a href="<?php echo Route::getRoute('activity','detail',array($experience['activity_id'])) ?>"><?php echo Info::subContent($experience['activity_name'], 100) ?></a></h3>
                                        <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                            <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                                <h3><?php echo Currency::displayPrice($experience['activity_display_price']); ?></h3>
                                                <p><?php echo Info::activityTypeByKey($experience['activity_price_type']); ?></p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                                <?php
                                                $rating = 0;
                                                if($experience['ratingcounter']){
                                                    $rating = $experience['rating']/$experience['ratingcounter'];
                                                    
                                                }     
                                                $rating = 3;                                           
                                                echo Info::rating($rating, false, 'rating--lightest rating-small') ?>
                                                <p><?php echo $experience['reviews'] ?> <?php echo Info::t_lang('REVIEWS ') ?></p>
                                            </div>
                                        </div>
                                    </header>
                                    <a href="<?php echo Route::getRoute('activity','detail',array($experience['activity_id'])); ?>" title="<?php echo Info::subContent($experience['activity_name'], 100) ?>">
                                        <img src="<?php echo FatUtility::generateUrl('image', 'activity', array($experience['activity_image_id'], 600, 450)) ?>" alt="<?php echo Info::subContent($experience['activity_name'], 100) ?>" class="img-responsive thumb">
                                    </a>
                                </article>
                            </div>
                    <?php if ($i % 4 == 0) { ?>
                            </div>
                        <?php } 
                        $i++;
                        } 
                        //This is to ensure there is no open div if the number of elements in user_kicks is not a multiple of 6
                        if ($i % 4 != 1) echo "</div>";?>
                </div>
                <a class="left carousel-control"  role="button" data-slide="prev" data-parent="popular-experiences">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                </a>
                <a class="right carousel-control" role="button" data-slide="next" data-parent="popular-experiences">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                </a>
            </div>
        </div> 
        <?php endif;?>       

    </div>
</main>
