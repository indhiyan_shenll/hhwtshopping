<?php
defined('SYSTEM_INIT') or die('Invalid Usage.');

// echo FatCache::getCachedUrl(FatUtility::generateUrl('image','banner',array(20,1200,900)), 10000, '.jpg');

$frm->setFormTagAttribute('onsubmit','searchFilter(this); return false;');
$frm->setFormTagAttribute('action',FatUtility::generateUrl('activity'));
$frm->setFormTagAttribute('class','form form--vertical form--theme');
$frm->setFormTagAttribute('method','get');
$island = $frm->getField('island');
$island->developerTags['noCaptionTag'] = true;
$island->developerTags['col'] = 2;
$island->addWrapperAttribute('class','span span--2');

$theme = $frm->getField('theme');
$theme->developerTags['noCaptionTag'] = true;
$theme->developerTags['col'] = 2;
$theme->addWrapperAttribute('class','span span--3');

$category = $frm->getField('category');
$category->developerTags['noCaptionTag'] = true;
$category->developerTags['col'] = 2;
$category->addWrapperAttribute('class','span span--3');
 
$submit_btn = $frm->getField('submit_btn');
$submit_btn->developerTags['noCaptionTag'] = true;
$submit_btn->setFieldTagAttribute('class','button button--fit button--fill button--primary no--margin-bottom');
$submit_btn->developerTags['col'] = 2;
$submit_btn->addWrapperAttribute('class','span span--2');
?>
<main id="MAIN" class="site-main site-main--light">
	<?php #echo Helper::fat_shortcode("[fat_contactinfo]"); ?>
	<?php #echo Helper::fat_shortcode("[fat_offices]"); ?>
	<?php #echo Helper::fat_shortcode("[fat_contactform]"); ?>
	<?php if(!empty($banners)){?>
		<header class="site-main__header site-main__header--dark main-carousel__list js-main-carousel">
			<?php 
			$size = count($banners);
			$i = 1;
			foreach($banners as $banner){
				if(!empty($banner['banner_link']))
				{
			?>
					<a href="<?php echo $banner['banner_link']?>">
			<?php 
				}
				$obj = new loadTime();
				$obj->startTimer();
			?>
					<div class="main-carousel__item">
						<div class="site-main__header__image">
							<div class="img"><img src="<?php echo FatCache::getCachedUrl(FatUtility::generateUrl('image','banner',array($banner['banner_id'],1200,900)), 10000, '.jpg'); ?>" alt="<?php echo $banner['banner_title']?>"></div>
						</div>
						<div class="site-main__header__content">
							<div class="section section--vcenter">
								<div class="section__body">
									<div class="container container--static">
										<div class="span__row">
											<div class="span span--10 span--center text--center">
												<hgroup style="margin-bottom:2em;">
													<h5 class="special-heading special-heading-text"><?php echo $banner['banner_title']?> </h5>
												</hgroup>
												<p class="main-carousel__regular"><?php echo $banner['banner_text']?></p>
											</div>
										</div>
									</div>
								</div>
								<div class="section__footer">
									<div class="container container--static">
										<div class="span__row">
											<div class="span span--12">
												
												<div class="fl--right text--right hidden-on--mobile main-carousel__counter">
													<label>
														<small><?php echo Info::t_lang('FEATURED_LIST')?></small>
														<span><?php echo $i++;?></span>/<span><?php echo $size;?></span>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php
				if(!empty($banner['banner_link']))
				{
				?>
					</a>
				<?php 
				}
			} ?>  
		</header>
	<?php } ?>
	<?php
		$obj->stopTimer();
	?>
	<div class="site-main__body">
		<section class="section search__section" id="search">
			<?php echo $frm->getFormTag(); ?>
			<div class="container container--static">
				 <div class="span__row">
					 <div class="span span--3">
						 <div class="form-element">
							 <div class="form-element__control">
								 <label class="select" for="tag">
									 <?php echo $frm->getFieldHTML('island'); ?>
								 </label>
							 </div>
						 </div>
					 </div>
				
					 <div class="span span--3">
						 <div class="form-element">
							 <div class="form-element__control">
								 <label class="select" for="theme">
									 <?php echo $frm->getFieldHTML('theme'); ?>
								 </label>
							 </div>
						 </div>
					 </div>
				 
					 <div class="span span--3">
						 <div class="form-element">
							 <div class="form-element__control">
								 <label class="select" for="subcat-list">
									 <?php echo $frm->getFieldHTML('category'); ?>
								 </label>
							 </div>
						 </div>
					 </div>   
				 
					 <div class="span span--3 span-offset--1">
						 <div class="form-element text--center">
							<?php echo $frm->getFieldHTML('submit_btn'); ?>
						 </div>
					 </div>
			  </div>
			</form><?php echo $frm->getExternalJS(); ?>
		</section>
		<section class="section  island__section" id="islands">
			 <div class="section__header">
				 <div class="container container--static">
					 <div class="span__row">
						 <div class="span span--12" style="position:relative;">
							 <hgroup>
								 <h5 class="heading-text text--center"><?php echo Info::t_lang('PICK_AN_ISLAND');?></h5>
								 <h6 class="sub-heading-text text--center text--green"><?php echo Info::t_lang('ADVENTURE_AWAITS');?></h6>
							 </hgroup>
							 
						 </div>
					 </div>
				</div> 
			 </div>
			 <div class="section__body">
				 <div class="container container--static">
					<div class="span__row">
						 <div class="span span--12">
					 <div class="island-card__list list--4">
						 <?php
						 if(!empty($isLands)){
							 
							foreach($isLands as $isLand){
								
								?>
								<div class="island-card">
									 <div class="island-card__image">
										<img src="<?php echo FatUtility::generateUrl('image','islandRandom',array($isLand['island_id'],579,434))?>" alt="<?php echo $isLand['island_name']?>">
									 </div>
									 <a href="<?php echo FatUtility::generateUrl('island',Info::getSlugFromName($isLand['island_name']),array($isLand['island_id']))?>">
									 <div class="island-card__content">
										 <h6 class="island-card__heading"><?php echo $isLand['island_name']?></h6>
										 <p class="island-card__text"><?php echo Helper::truncateString(html_entity_decode($isLand['island_sub_content']),75);?></p>  
									 </div>
									 </a>
								 </div>
								<?php
							}
						 }
						 
						 ?>
						 
						 
					 </div>
					 <?php if(!(count($isLands) < 8)) { ?>  
					 <div class="text--center" style="margin-top:1.25em;"><a href="<?php echo FatUtility::generateUrl('island','lists')?>" class="button button--fill button--primary"><?php echo Info::t_lang('SHOW_MORE')?></a>
					</div>
					 <?php } ?>  
						 </div>
					 </div>
				 </div>
			 </div>
			</section>
		<section class="section section--light activity__section" id="activities">
			<div class="section__header">
			   <div class="container container--static">
					 <div class="span__row">
						 <div class="span span--12">
							 <hgroup>
								 <h5 class="heading-text text--center"><?php echo Info::t_lang('TOP_ESCAPADES')?></h5>
								 <h6 class="sub-heading-text text--center text--red"><?php echo Info::t_lang('TAVELERS_FAVORITE')?></h6>
							 </hgroup>
						 </div>
					 </div>
				</div> 
			 </div>
			 <div class="section__body">
				 <div class="container container--static">
					<div class="span__row">
						 <div class="span span--12">
							<div class="activity-card__list grid--style" id="feature-list"></div>
							<div class="text--center" style="margin-top:1.25em;"><a href="<?php echo FatUtility::generateUrl('activity') ?>" class="button button--fill button--primary"><?php echo Info::t_lang('SHOW_MORE')?></a></div>
						</div>
					 </div>
				 </div>
			 </div>
		</section>
		<section class="section category__section" id="categories">
				<div class="section__header">
					<div class="container container--static">
						<div class="span__row">
							<div class="span span--12">
								<hgroup>
									<h5 class="heading-text text--center"><?php echo Info::t_lang('THEMES');?></h5>
									<h6 class="sub-heading-text text--center text--orange"><?php echo Info::t_lang("WHAT'S_YOUR_FLAVOR?");?></h6>
								</hgroup>
							</div>
						</div>
					</div>
				</div>
				<div class="section__body">
					<div class="container container--static">
						<div class="span__row">
							<div class="span span--12">
								<div class="category-card__list list--carousel js-carousel" data-slides="5" data-arrow="1">
									<?php
									foreach($services as $service)
									{
									?>
											<div class="category-card">
												<a href="<?php echo FatUtility::generateUrl('services',Info::getSlugFromName($service['service_name']),array($service['service_id']))?>" class="category-card__wrap">
													<figure class="category-card__image">
														<img src="<?php echo FatUtility::generateUrl('image','service',array($service['service_id'], 620, 620))?>" alt="<?php echo $service['service_name']; ?>">
													</figure>  
													<div class="category-card__content">
														<span><?php echo $service['service_name']; ?></span>
													</div>
												</a>
											</div>
									<?php
									}
									?>
								</div>
								<?php if(!(count($services) < 8)) { ?>
									<div class="text--center" style="margin-top:1.25em;">
										<a href="<?php echo FatUtility::generateUrl('services')?>" class="button button--fill button--primary">
											<?php echo Info::t_lang('SHOW_MORE')?>
										</a>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div> 
		</section>
		<?php if(!empty($wuc['block_content'])){ ?>
		<section class="section why-choose__section no--padding-top" id="whyChooseUs">
			<div class="section__header">
				 <div class="container container--static">
					 <div class="span__row">
						 <div class="span span--10 span--center">
							 <hgroup>
								 <h5 class="heading-text text--center"><?php echo Info::t_lang('WHY_CHOOSE_US');?></h5>
								  <h6 class="sub-heading-text text--center text--primary"><?php echo Info::t_lang("FOR_ALL _THE_TROPICAL_REASONS");?></h6>
							 </hgroup>
						 </div>
					 </div>
				</div>   
			</div>
			<div class="section__body">
				<div class="container container--static">
					 <div class="span__row">
						 <div class="span span--10 span--center">
							 <article class="island-tip">
								 <div class="tip__list island-tip__list">
								 <?php echo html_entity_decode($wuc['block_content'])?>
								 </div>
							 </article>
						 </div>
					 </div>
				 </div>
			 </div>
		 </section>
		 <?php } ?>
		<!--News letter section-->
		<!--<section id="facts" class="section fact__section" style="background-image:url(images/surf3.jpg);">-->
		<section id="facts" class="section fact__section" style="background-image:url(<?php echo FatUtility::generateUrl('Image','fatImages',array('surf3.jpg', 'fullwidthbanner', 2000, 600)); ?>);">
			<div class="section__header">
				<div class="container container--static">
					<div class="span__row">
						<div class="span span--10 span--center">
							<hgroup>
								<h5 class="heading-text text--center"><?php echo Info::t_lang('Digitizing_island_services_for_2020');?></h5>
							</hgroup>
						</div>
					</div>
				</div>
			</div>
			<div class="section__body">
			  <div id="mc_embed_signup_scroll" class="container container--static">
				<div class="span__row">
				  <div class="span span--10 span--center">
				<div class="fact-card__list list--3">
				  <div class="fact-card">
					<div class="fact-card__content">
					  <div class="fact-card__count"><?php echo ($userCounts['total_host'] > 0 ? $userCounts['total_host'] : 0); ?></div>
					  <div class="fact-card__text"><?php echo Info::t_lang('Hosts');?></div>
					</div>
				  </div>
				  <div class="fact-card">
					<div class="fact-card__content">
					  <div class="fact-card__count"><?php echo $activity; ?></div>
					  <div class="fact-card__text"><?php echo Info::t_lang('Activitie_s');?></div>
					</div>
				  </div>
				  <div class="fact-card">
					<div class="fact-card__content">
					  <div class="fact-card__count"><?php echo ($userCounts['total_traveler'] > 0 ? $userCounts['total_traveler'] : 0); ?></div>
					  <div class="fact-card__text"><?php echo Info::t_lang('Travellers');?></div>
					</div>
				  </div>
				</div>
				  </div>
				</div>
				<?php echo Helper::fat_shortcode('[fat_mailchimpnewsletter]'); ?>
				<div class="span__row">
					<div class="span span--12">
						<div style="position: absolute; left: -5000px;" aria-hidden="true">
							<input type="text" name="b_f4195ce0c6ec8ec0047f524af_3c606e2d6f" tabindex="-1" value="">
						</div>
						<div id="mce-responses" class="clear message__el">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none"></div>
						</div>    
					</div>    
				</div>
			  </div>
			</div>
		</section> 
		 <!--End News letter-->
		<?php 
		if(!empty($testimonials)){ ?>
		<section id="asSeenOn" class="section testimonial__section">
			<div class="section__header">
				 <div class="container container--static">
					 <div class="span__row">
						 <div class="span span--10 span--center">
							 <hgroup>
								 <h5 class="heading-text text--center"><?php echo Info::t_lang('AS_SEEN_ON');?></h5>
							 </hgroup>
						 </div>
					 </div>
				</div>   
			</div>
			<div class="section__body">
				<div class="container container--static">
					 <div class="span__row js-carousel" data-slides="2">
						 <?php foreach($testimonials as $testimonial){ ?>
						 <div class="span span--6">
							 <div class="media testimonial__item">
								<div class="media__figure media--left">
									<div class="testimonial__image"><img alt="<?php echo $testimonial[Testimonial::DB_TBL_PREFIX.'name']?>" title="<?php echo $testimonial[Testimonial::DB_TBL_PREFIX.'name']?>" src="<?php echo FatUtility::generateUrl('image','testimonial',array($testimonial[Testimonial::DB_TBL_PREFIX.'id'],100,100))?>"></div>
								</div>
								<div class="media__body testimonial__content">
									<h6 class="testimonial__heading"><?php echo $testimonial[Testimonial::DB_TBL_PREFIX.'name']?></h6>
									<div class="more-less">
										<div class="more-block">
											<p class="testimonial__text"><?php echo $testimonial[Testimonial::DB_TBL_PREFIX.'content']?></p>
										</div>
									</div>
								</div>     
							</div>
						 </div>
						 <?php } ?>
					</div>
				</div>
			 </div>
		</section>
		<?php } ?>				
		 <!--<section class="section section--vcenter promotion__section" id="islandAdventure" style="background-image:url(images/promotion-bg.jpg)">-->
		<section class="section section--vcenter promotion__section" id="islandAdventure" style="background-image:url(<?php echo FatUtility::generateUrl('Image','fatImages',array('promotion-bg.jpg', 'fullwidthbanner', 2000, 500)); ?>)">
			<div class="section__body">
				<div class="container container--static">
					<div class="span__row">
						<div class="span span--10 span--center text--center">
							<div class="promotion__content">
								<h5 class="heading-text"><?php echo Info::t_lang('#ISLANDADVENTURES');?></h5>
								<h6 class="sub-heading-text"><?php echo Info::t_lang('DISCOVER_HOW_WE_CREATES_AUTHENTIC_EXPERIENCES_ON_ISLANDS.');?></h6>
							</div> 
							<div class="promotion__action">
								<a href="mailto:<?php echo FatApp::getConfig('ADMIN_CAREER_EMAIL_ID')?>" class="button button--fill button--primary"> <?php echo Info::t_lang('VOLUNTEER');?></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
	</div>
</main>