
$(document).ready(function () {
    var is_logged_user = localStorage.getItem('currentUser');

    if (is_logged_user && typeof is_logged_user != 'undefined') {
        var current_user = JSON.parse(is_logged_user);        
        if ( typeof current_user != 'undefined' && current_user.status == '1' && current_user.email != '') {
            var params = $.extend({}, doAjax_params_default);
            params['url'] = 'home/is-shared-user-logged';
            params['data'] = current_user;
            params['successCallbackFunction'] = 'enableTravelerMenu';
            doAjax(params);
        }
    } 
});

function enableTravelerMenu(response) {
    
    if (response != "")  {
        var json = $.parseJSON(response);
        if (response != "" && json.status == '1') {
            var params = $.extend({}, doAjax_params_default);
            params['url'] = 'home/enable-traveler-menu';
            params['data'] = {email: json.email, name: json.name, country: json.country, image: json.image};
            params['successCallbackFunction'] = 'loginTravelerMenuSuccessCallBack';
            doAjax(params);
        }        
    }    
}

function loginTravelerMenuSuccessCallBack(response) {
    if (response != "")
        $(".h__navigation.fl--right").empty().append(response);    
}

String.prototype.endsWith = function(suffix) {
   return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

var doAjax_params_default = {
    'url': null,
    'requestType': "POST",
    // 'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
    // 'dataType': 'json',
    'data': {},
    'beforeSendCallbackFunction': null,
    'successCallbackFunction': null,
    'completeCallbackFunction': null,
    'errorCallBackFunction': null,
};


function doAjax(doAjax_params) {

    var url = doAjax_params['url'];
    var requestType = doAjax_params['requestType'];
    // var contentType = doAjax_params['contentType'];
    // var dataType = doAjax_params['dataType'];
    var data = doAjax_params['data'];
    var beforeSendCallbackFunction = doAjax_params['beforeSendCallbackFunction'];
    var successCallbackFunction = doAjax_params['successCallbackFunction'];
    var completeCallbackFunction = doAjax_params['completeCallbackFunction'];
    var errorCallBackFunction = doAjax_params['errorCallBackFunction'];

    $.ajax({
        url: url,
        // crossDomain: true,
        type: requestType,
        // contentType: contentType,
        // dataType: dataType,
        data: data,
        beforeSend: function(jqXHR, settings) {
            if (typeof beforeSendCallbackFunction === "function") {
            }
        },
        success: function(data, textStatus, jqXHR) {  

            switch (successCallbackFunction) {
                case "enableTravelerMenu":
                    enableTravelerMenu(data);
                    break;
                case "loginTravelerMenuSuccessCallBack":
                    loginTravelerMenuSuccessCallBack(data);
                    break;
            }

        },
        error: function(jqXHR, textStatus, errorThrown) {
            if (typeof errorCallBackFunction === "function") {
            }

        },
        complete: function(jqXHR, textStatus) {
            if (typeof completeCallbackFunction === "function") {
            }
        }
    });
}



$(document).ready(function () {
   
    readlessmore('', 110, 200);
  $("img.lazy").lazyload({
        event : "sporty"
    });
});


$(window).bind("load", function() {
    var timeout = setTimeout(function() {
        $("img.lazy").trigger("sporty")
         getFeaturedList();
    }, 300);
});
$(function () {
    var $window = $(window),
            $body = $('body');
    //Scroll top
    var _isTop = function () {
        if ($window.scrollTop() > 0)
            $body.removeClass('is--top').addClass('is--bottom');
        else
            $body.removeClass('is--bottom').addClass('is--top');
    };
    _isTop();
    $window.on('scroll', function () {
        _isTop();
    });

});


searchFilter = function (frm) {
    var url = fcom.makeUrl('activity');
    var query_string = '';
    $(frm).find('select').each(function () {
        var field_name = $(this).attr('name');
        var field_value = $(this).val();
        if (field_value != '') {
            if (query_string == '') {
                query_string += field_name + '=' + field_value;
            }
            else {
                query_string += '&' + field_name + '=' + field_value;
            }
        }

    });
    if (query_string == '') {
        return false;
    }

    url += '?' + query_string;
    window.location = url;

}

getSubService = function (obj) {
    $.ajax({
        url: fcom.makeUrl('services', 'sub-service'),
        type: 'post',
        data: {'service_id': $(obj).val()},
        success: function (json) {
            json = $.parseJSON(json);
            $('#subcat-list').html(json.msg);
        }
    });
}

function getFeaturedList() {
    jsonNotifyMessage('Loading...');
    fcom.ajax(fcom.makeUrl('home', 'ajaxLoad'), {}, function (json) {
        json = $.parseJSON(json);
        jsonRemoveMessage();
        if (json.status == 1) {
            if (typeof json.featureList != 'undefined' && json.noResult != '1') {
                $('#feature-list').html(json.featureList);
                $('#feature-list-wrapper').show();
                $('#activities').show();
                nicescrollbar();
            }
        }
    });
}

showFeatureActivities = function (el, tab_id) {

    $('.js-feature-tab').slideUp();
    $('#js-feature-tab-' + tab_id).slideDown();
    $('.js-featured-island li a').removeClass('button--red');
    $('.js-featured-island li a').removeClass('button--dark');
    $('.js-featured-island li a').addClass('button--dark');
    $(el).removeClass('button--dark');
    $(el).addClass('button--red');
    return;
}

