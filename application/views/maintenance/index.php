<?php defined('SYSTEM_INIT') or die('Invalid Usage'); ?>
<div>
    <div class="body clearfix">
		<div class=" fixed-container">
			<div class="cmsContainer">
				<?php echo html_entity_decode($maintenance_text); ?>
			</div>
		</div>
    </div>
</div>
  