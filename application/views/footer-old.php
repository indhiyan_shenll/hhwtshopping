<?php
defined('SYSTEM_INIT') or die('Invalid Usage.');
?>
 <!-- Footer -->
		<?php if(isset($controller) && $controller == "guest"){?>
         <footer id="FOOTER" class="site-footer site-footer--bg" style="background-image:url(<?php echo CONF_WEBROOT_URL; ?>images/footer-bg.jpg);">
		<?php }elseif(isset($class) && $class == "is--dashboard"){
		$usertype = User::getLoggedUserAttribute("user_type");
		?>
		<?php if($usertype == 1){ ?>
		<aside class="site-fixed-sidebar">
            <div class="container container--fluid">
                <nav role="navigation" class="menu">
                    <ul class="list list--vertical text--center">
                        <li><a class="button button--fit button--fill button--red" href="<?php echo FatUtility::generateUrl('hostactivity','update',array(0));?>"><?php echo Info::t_lang('ADD_AN_ACTIVITY');?></a></li>
                        <li><a class="button button--fit button--fill button--blue" href="<?php echo FatUtility::generateUrl('message');?>"><?php echo Info::t_lang('SEND_MESSAGES');?></a></li>
                        <li class="dropdown dropdown--floated">
                            <a class="link dropdown__trigger" href="javascript:;"><?php echo Info::t_lang('MORE')?></a>
                            <ul class="list list--vertical text--center dropdown__target">
                                <li><a href="<?php echo FatUtility::generateUrl('notification');?>"><?php echo Info::t_lang('NOTIFICATIONS')?></a></li>
                                <li><a href="<?php echo FatUtility::generateUrl('wishlist');?>"><?php echo Info::t_lang('Wishlist');?></a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!--div class="sidebar__stats">
                    <div class="stat">
                        <span class="stat__big">252</span>
                        <span class="stat__small">Sales</span>
                    </div>
                    <div class="stat">
                        <span class="stat__big">S$12k+</span>
                        <span class="stat__small">Earnings</span>
                    </div>
                </div-->
            </div>
        </aside>
		<?php } else { ?>
		<aside class="site-fixed-sidebar">
            <div class="container container--fluid">
                <nav role="navigation" class="menu">
                    <ul class="list list--vertical text--center">
                        <li><a class="button button--fit button--fill button--red" href="<?php echo FatUtility::generateUrl('notification');?>"><?php echo Info::t_lang('NOTIFICATIONS')?></a>
						
                        <li><a class="button button--fit button--fill button--blue" href="<?php echo FatUtility::generateUrl('message');?>"><?php echo Info::t_lang('SEND_MESSAGES');?></a></li>
						
                        <li class="dropdown dropdown--floated">
                            <a class="link dropdown__trigger" href="javascript:;"><?php echo Info::t_lang('MORE')?></a>
                            <ul class="list list--vertical text--center dropdown__target">
                                <li><a href="<?php echo FatUtility::generateUrl('review');?>"><?php echo Info::t_lang('REVIEWS')?></a></li>
                                <li><a href="<?php echo FatUtility::generateUrl('wishlist');?>"><?php echo Info::t_lang('Wishlist');?></a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!--div class="sidebar__stats">
                    <div class="stat">
                        <span class="stat__big">252</span>
                        <span class="stat__small">Sales</span>
                    </div>
                    <div class="stat">
                        <span class="stat__big">S$12k+</span>
                        <span class="stat__small">Earnings</span>
                    </div>
                </div-->
            </div>
        </aside>
		<?php } ?>
		 <footer id="FOOTER" class="site-footer site-footer--light with--sidebar">
		<?php }else{ ?>
		<footer id="FOOTER" class="site-footer site-footer--dark">
		<?php } ?>
            <div class="container container--static">
                <div class="span__row">
                    <div class="span span--10 span--center">
            <section class="site-footer__upper">
                <div class="container container--fluid">
                    <div class="span__row">
                    <div class="span span--6">
                        <div class="f__block">
                            <h6 class="f__block__heading"><?php if(!empty($our_mission)) echo $our_mission['block_title'];?></h6>
                            <div class="regular-text innova-editor"><?php if(!empty($our_mission)) echo html_entity_decode($our_mission['block_content']);?></div>
                        </div>
						<?php /*
						<div class="f__block">
                            <h6 class="f__block__heading"><?php if(!empty($our_vision)) echo $our_vision['block_title'];?></h6>
                            <p class="regular-text"><?php if(!empty($our_vision)) echo html_entity_decode($our_vision['block_content']);?></p>
						</div>
						*/ ?>
						<div class="f__block">
                            <?php /*
							<h6 class="f__block__heading"><?php echo Info::t_lang('DOWNLOAD_APP') ?></h6>
                            <p class="regular-text">Coming Soon</p>
							*/?>
							<img src="<?php echo CONF_WEBROOT_URL; ?>images/app.png" width="360" alt="">
						</div>
						
                        
                    </div>
                    <div class="span span--2">
                        <div class="f__block">
                            <?php
							$browse_cms = Cms::getCmsLinks(Cms::CMS_BROWSE_POSITION_TYPE);
							?>
							<h6 class="f__block__heading"><?php echo Info::t_lang('BROWSE')?></h6>
                            <ul class="list list--vertical">
								 <li><a href="<?php echo FatUtility::generateUrl('hosts')?>"><?php echo Info::t_lang('BECOME_A_HOST')?></a></li>
								 <?php if(!empty($cms_links[7])){ ?>
								<li><a href="<?php echo FatUtility::generateUrl('terms',$cms_links[7]['cms_slug'])?>"><?php echo Info::t_lang('TERMS_OF_SERVICE')?></a></li>
								<?php } ?>
								<li><a href="<?php echo FatUtility::generateUrl('cancellation-policy')?>"><?php echo Info::t_lang('CANCELLATION_POLICY')?></a></li>
								 
                                <?php if(!empty($browse_cms)){ ?>
									<?php foreach($browse_cms as $cms){ 
									$cms_url = $cms['cms_type'] == 1? FatUtility::generateUrl('terms',$cms['cms_slug']):$cms['cms_slug'];
									?>
									
									<li><a href="<?php echo $cms_url; ?>"><?php echo $cms['cms_name']?></a></li>
									<?php } ?>
								<?php } ?>
								<li><a href="<?php echo FatUtility::generateUrl('Faq')?>"><?php echo Info::t_lang('FAQ')?></a></li>
							 </ul>
                        </div>
                    </div>
                    <div class="span span--2">
                        <div class="f__block">
							<?php
							$about_cms = Cms::getCmsLinks(Cms::CMS_ABOUT_POSITION_TYPE);
							?>
						   <h6 class="f__block__heading"><?php echo Info::t_lang('ABOUT')?></h6>
                            <ul class="list list--vertical">
                               <li><a href="<?php echo FatUtility::generateUrl('aboutUs')?>"><?php echo Info::t_lang('ABOUT_US')?></a></li>
                                <?php if(!empty($about_cms)){ ?>
									<?php foreach($about_cms as $cms){ 
										$cms_url = $cms['cms_type'] == 1? FatUtility::generateUrl('terms',$cms['cms_slug']):$cms['cms_slug'];
									?>
									<li><a href="<?php echo $cms_url; ?>"><?php echo $cms['cms_name']?></a></li>
									<?php } ?>
								<?php } ?>
								<li><a href="<?php echo FatUtility::generateUrl('contacts')?>"><?php echo Info::t_lang('CONTACT_US')?></a></li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="span span--2">
						<div class="f__block f__currency">
							<h6 class="f__block__heading"><?php echo Info::t_lang('CURRENCY');?></h6>
							<form class="form form--theme form--vertical">
								<div class="form-element no--margin">
									<div class="form-element__control">
										<select class='js-currency-class'>
											<?php foreach($currencyopt as $k=>$v){ ?>
												<option value="<?php echo $k?>" <?php if($k == Info::getCurrentCurrency()) {?> selected="selected" <?php } ?>><?php echo $v?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</form>
						</div>
                        <a href="<?php echo FatUtility::generateUrl();?>" class="f__logo">
                            <img src="<?php echo FatUtility::generateFullUrl('image','companyLogo');?>" alt="">
                        </a>
                    </div>
                    </div>
                </div>
            </section>
			<section class="site-footer__upper">
				<div class="container container--fluid">
                    <div class="span__row">
						<div class="span span--2 f__certified">
							<span class="image">
							<span id="cdSiteSeal1"><script type="text/javascript" src="//tracedseals.starfieldtech.com/siteseal/get?scriptId=cdSiteSeal1&amp;cdSealType=Seal1&amp;sealId=55e4ye7y7mb73cc2aea7f3ac40669eapevy7mb7355e4ye7a50a1ecc36bdb9217"></script></span>
							</span>
						</div>
						<div class="span span--5 span--last f__payment">
							<ul class="list list--horizontal">
								<li><img src="<?php echo CONF_WEBROOT_URL; ?>images/payment/visa.png" alt="visa"></li>
								<li><img src="<?php echo CONF_WEBROOT_URL; ?>images/payment/master.png" alt="master card"></li>
								<li><img src="<?php echo CONF_WEBROOT_URL; ?>images/payment/jcb.png" alt="jcb"></li>
							</ul>
						</div>
					</div>
				</div>
			</section>
            <section class="site-footer__lower">
                <div class="container container--fluid">
                    <div class="span__row">
                    <div class="span span--12 text--center">
                        <div class="f__block">
                            <h6 class="f__block__heading"><?php echo Info::t_lang('HANG_OUT_WITH_US')?></h6>
                            <nav class="menu f__social-menu">
                                <ul class="list list--fit">
                                    <li><a target="_blank" href="<?php echo FatApp::getConfig('conf_facebook_url')?>">
                                        <span><svg class="icon"><use xlink:href="#icon-facebook" /></svg></span>
                                        <span class="hidden-on--mobile"><?php echo Info::t_lang('FACEBOOK')?></span></a></li>
                                    <li><a  target="_blank" href="<?php echo FatApp::getConfig('conf_twitter_url')?>" >
                                        <span><svg class="icon"><use xlink:href="#icon-twitter" /></svg></span>
                                        <span class="hidden-on--mobile"><?php echo Info::t_lang('TWITTER')?></span></a></li>
                                    <li><a  target="_blank" href="<?php echo FatApp::getConfig('conf_instagram_url')?>" >
                                        <span><svg class="icon"><use xlink:href="#icon-instagram" /></svg></span>
                                        <span class="hidden-on--mobile"><?php echo Info::t_lang('INSTAGRAM')?></span></a></li>
                                    <li><a target="_blank" href="<?php echo FatApp::getConfig('conf_pinterest_url')?>">
                                        <span><svg class="icon"><use xlink:href="#icon-pinterest" /></svg></span>
                                        <span class="hidden-on--mobile"><?php echo Info::t_lang('PINTEREST')?></span></a></li>
                                    <li><a target="_blank" href="<?php echo FatApp::getConfig('conf_snapchat_url')?>">
                                        <span><svg class="icon"><use xlink:href="#icon-snapchat" /></svg></span>
                                        <span class="hidden-on--mobile"><?php echo Info::t_lang('SNAPCHAT')?></span></a></li>
                                </ul>
                            </nav>
                            <p class="regular-text"><?php echo FatApp::getConfig('conf_copyright_text')?></p>
                        </div>
                    </div>
                    </div>
                </div>
            </section>

                    </div>
                </div>
            </div>
        </footer>
<?php 

if(!empty($_SESSION[User::SESSION_ELEMENT_NAME]['email_verify_msg'])){
?>
<aside  class="alert alert_info fixed"> 
	<div> 		
		<div class="content"><?php echo $_SESSION[User::SESSION_ELEMENT_NAME]['email_verify_msg']; ?></div> 		
		
	</div>
</aside>
<?php
}
//Message::addMessage('sdfsdfsd');
//Message::addError('sdfsdfsd');
if((Message::getErrorCount()+Message::getMessageCount()) > 0 ){ ?>
<aside class="system-message js-system-message">
	<div>
		<a class="fl--right" href="javascript:clearSystemMessage();">
			<svg class="icon icon--cross">
				<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-cross"></use>
			</svg>
		</a>
		<?php
			defined('SYSTEM_INIT') or die('Invalid Usage');
			echo Message::getHtml();
		?>
	</div>
</aside> 	
<?php } ?>
<div  style="display:none;" id="main-search">
	<div class="search-card"  >
        <div class="search-card__action">
            <div class="container container--static">
                <span class="search-card__action__label">
                    <svg class="icon icon--search"><use xlink:href="#icon-search" /></svg>
                </span>
                <label class="search-card__action__input">
                    <input type="text" id="main-search-input" value="" placeholder="<?php echo Info::t_lang('SEARCH_FOR_ISLAND_ESCAPADES')?>">
                </label>
                <a href="javascript:;" class="search-card__action__close" onclick="closeMainSearch()">
                    <svg class="icon icon--cross"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-cross"></use></svg>
                </a>
            </div>
        </div>
        <div class="search-card__result" id="search-card-result-wrapper" style="display:none;" >
             <section class="section">
				<div class="section__body">
					<div class="container container--static">
					<div class="span__row">
						 <div class="span span--12">
							 
							 <div class="activity-media__list" id="search-card__result">
							 </div>
							 <nav class="text--center" style="margin-top:1.2em;display:none" id="more-result" >
								 <a href="javascript:;" onclick="loadMoreMainSearch()" class="button button--fill button--dark"><?php echo Info::t_lang('LOAD_MORE')?></a>
							 </nav>
							
						</div>
					</div>
					</div>
				</div>
			</section>
        </div>
    </div>  
</div> 
<!-- mobile menu-->
<nav class="menu mobile-menu js-mobile-menu">
	<button class="block-heading-text block-heading-text--small js-menu-close has--opened">
		<svg class="icon icon--cross"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-cross"></use></svg> <span><?php echo Info::t_lang('CLOSE')?></span>
	</button>
</nav> 
<div class="overlay js-overlay"></div>
<!-- mobile menu end -->
    </body>

</html>
