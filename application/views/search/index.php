
<main id="MAIN" class="site-main">

    <div class="container hhwt-single-category">
        <div class="col-md-6 col-sm-6 col-xs-12 hhwt-destinations-category">
            <p><a href="javascript:void(0)">Home</a><span class="fa fa-angle-right icon-right"></span><a href="javascript:void(0)">Experiences</a></p>
            <h3><a href="javascript:void(0)">Experiences</a></h3>
        </div>
    </div>
    <div class="container-fluid hhwt-filter-fluid">
        <div class="container hhwt-filter-container">
            <div class="col-sm-6 hhwt-hide-filter">
                <select class="form-control hhwt-filter hhwt-filter-status" name="hhwt-filter-status">
                    <option value="">Hide filter</option>
                </select>
                <p class="filter-reset"><a href="javascript:void(0)">RESET</a></p>
            </div>
            <div class="col-sm-6 hhwt-spec-filters">
                <ul class="filter-list">
                    <li><select class="form-control hhwt-filter hhwt-sort-filter" name="hhwt-sort-filter">
                        <option value="">Sort</option>
                    </select></li>
                    <li><select class="form-control hhwt-filter hhwt-duration-filter" name="hhwt-duration-filter">
                        <option value="">Select Duration</option>
                    </select></li>
                    <li><select class="form-control hhwt-filter hhwt-price-filter" name="hhwt-price-filter">
                        <option value="">Select Price</option>
                    </select></li>
                </ul>
            </div>
            <div class="col-sm-12 hhwt-city-filters">
                <select class="form-control hhwt-filter hhwt-city-filter" name="hhwt-city-filter">
                    <option value="">Select City</option>
                </select>
                <select class="form-control hhwt-filter hhwt-category-filter" name="hhwt-category-filter">
                    <option value="">Select category</option>
                </select>
                <div class="search-keyword input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" class="form-control" placeholder="Search by Keyword...">
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid hhwt-search-fluid">
        <div class="container hhwt-destination-container">
            <h4 class="col-sm-10 col-xs-8 hhwt-destination-title">Search Results</h4>
        </div>
        <div id="hhwt-post">
            <div class="custom-container">
                <div class="search-grid">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">11 Days Tokyo-Kyoto with Trekking in Jap</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$300</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>80 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/01.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">Beyond Tokyo:Roof of Japan Experience</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$90</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>91 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/02.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Tokyo Minakami Onsan F&E</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>70 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/03.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Basic Tokyo Mt Fuji & Disney Fun</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>102 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/04.png" class="img-responsive thumb">
                        </article>
                    </div>
                </div>
                <div class="search-grid">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">11 Days Tokyo-Kyoto with Trekking in Jap</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$300</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>80 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/01.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">Beyond Tokyo:Roof of Japan Experience</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$90</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>91 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/02.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Tokyo Minakami Onsan F&E</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>70 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/03.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Basic Tokyo Mt Fuji & Disney Fun</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>102 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/04.png" class="img-responsive thumb">
                        </article>
                    </div>
                </div>
                <div class="search-grid">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">11 Days Tokyo-Kyoto with Trekking in Jap</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$300</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>80 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/01.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">Beyond Tokyo:Roof of Japan Experience</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$90</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>91 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/02.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Tokyo Minakami Onsan F&E</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>70 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/03.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Basic Tokyo Mt Fuji & Disney Fun</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>102 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/04.png" class="img-responsive thumb">
                        </article>
                    </div>
                </div>
                <div class="search-grid">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">11 Days Tokyo-Kyoto with Trekking in Jap</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$300</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>80 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/01.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">Beyond Tokyo:Roof of Japan Experience</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$90</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>91 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/02.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Tokyo Minakami Onsan F&E</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>70 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/03.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Basic Tokyo Mt Fuji & Disney Fun</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>102 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/04.png" class="img-responsive thumb">
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>

<?php echo TrackingCode::getTrackingCode(2); ?>
    function facebookWishListTrack() {
<?php echo TrackingCode::getTrackingCode(3); ?>
    }

</script>