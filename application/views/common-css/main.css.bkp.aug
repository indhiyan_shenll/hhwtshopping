/* 

By FATbit
*/
* {
  -ms-box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  outline: none;
}

span svg {
  display: inline;
}

img {
  display: inline-block;
  vertical-align: middle;
  max-width: 100%;
}

/*! normalize.css v4.0.0 | MIT License | github.com/necolas/normalize.css */
progress, sub, sup {
  vertical-align: baseline;
}

button, hr, input, select {
  overflow: visible;
}

[type=checkbox], [type=radio], legend {
  box-sizing: border-box;
  padding: 0;
}

html {
  font-family: sans-serif;
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
}

body {
  margin: 0;
}

article, aside, details, figcaption, figure, footer, header, main, menu, nav, section, summary {
  display: block;
}

audio, canvas, progress, video {
  display: inline-block;
}

audio:not([controls]) {
  display: none;
  height: 0;
}

[hidden], template {
  display: none;
}

a {
  background-color: transparent;
}

a:active, a:hover {
  outline-width: 0;
}

abbr[title] {
  border-bottom: none;
  text-decoration: underline;
  text-decoration: underline dotted;
}

b, strong {
  font-weight: bolder;
}

dfn {
  font-style: italic;
}

h1 {
  font-size: 2em;
  margin: .67em 0;
}

mark {
  background-color: #ff0;
  color: #000;
}

small {
  font-size: 80%;
}

sub, sup {
  font-size: 75%;
  line-height: 0;
  position: relative;
}

sub {
  bottom: -.25em;
}

sup {
  top: -.5em;
}

img {
  border-style: none;
}

svg:not(:root) {
  overflow: hidden;
}

code, kbd, pre, samp {
  font-family: monospace,monospace;
  font-size: 1em;
}

figure {
  margin: 1em 40px;
}

hr {
  box-sizing: content-box;
  height: 0;
}

button, input, select, textarea {
  font: inherit;
  margin: 0;
}

optgroup {
  font-weight: 700;
}

button, select {
  text-transform: none;
}

[type=button], [type=reset], [type=submit], button {
  cursor: pointer;
}

[disabled] {
  cursor: default;
}

[type=reset], [type=submit], button, html [type=button] {
  -webkit-appearance: button;
}

button::-moz-focus-inner, input::-moz-focus-inner {
  border: 0;
  padding: 0;
}

button:-moz-focusring, input:-moz-focusring {
  outline: ButtonText dotted 1px;
}

fieldset {
  border: 1px solid silver;
  margin: 0 2px;
  padding: .35em .625em .75em;
}

legend {
  color: inherit;
  display: table;
  max-width: 100%;
  white-space: normal;
}

textarea {
  overflow: auto;
}

[type=number]::-webkit-inner-spin-button, [type=number]::-webkit-outer-spin-button {
  height: auto;
}

[type=search] {
  -webkit-appearance: textfield;
}

[type=search]::-webkit-search-cancel-button, [type=search]::-webkit-search-decoration {
  -webkit-appearance: none;
}

@-ms-viewport {
  width: device-width;
}
body {
  -ms-overflow-style: scrollbar;
}

@font-face {
  font-family: 'brusher_regular';
  src: url("../fonts/brusher-regular-webfont.woff2") format("woff2"), url("../fonts/brusher-regular-webfont.woff") format("woff");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_cond_thin';
  src: url("../fonts/solitas-conthi-webfont.eot");
  src: url("../fonts/solitas-conthi-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-conthi-webfont.woff2") format("woff2"), url("../fonts/solitas-conthi-webfont.woff") format("woff"), url("../fonts/solitas-conthi-webfont.ttf") format("truetype"), url("../fonts/solitas-conthi-webfont.svg#solitas_cond_thincond_thin") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_cond_light';
  src: url("../fonts/solitas-conlig-webfont.eot");
  src: url("../fonts/solitas-conlig-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-conlig-webfont.woff2") format("woff2"), url("../fonts/solitas-conlig-webfont.woff") format("woff"), url("../fonts/solitas-conlig-webfont.ttf") format("truetype"), url("../fonts/solitas-conlig-webfont.svg#solitas_cond_lightcond_light") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_cond_regular';
  src: url("../fonts/solitas-conreg-webfont.eot");
  src: url("../fonts/solitas-conreg-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-conreg-webfont.woff2") format("woff2"), url("../fonts/solitas-conreg-webfont.woff") format("woff"), url("../fonts/solitas-conreg-webfont.ttf") format("truetype"), url("../fonts/solitas-conreg-webfont.svg#solitas_cond_regularcondRg") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_norm_thin';
  src: url("../fonts/solitas-northi-webfont.eot");
  src: url("../fonts/solitas-northi-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-northi-webfont.woff2") format("woff2"), url("../fonts/solitas-northi-webfont.woff") format("woff"), url("../fonts/solitas-northi-webfont.ttf") format("truetype"), url("../fonts/solitas-northi-webfont.svg#solitas_norm_thinnorm_thin") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_norm_light';
  src: url("../fonts/solitas-norlig-webfont.eot");
  src: url("../fonts/solitas-norlig-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-norlig-webfont.woff2") format("woff2"), url("../fonts/solitas-norlig-webfont.woff") format("woff"), url("../fonts/solitas-norlig-webfont.ttf") format("truetype"), url("../fonts/solitas-norlig-webfont.svg#solitas_norm_lightnorm_light") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_norm_light_i';
  src: url("../fonts/solitas-norligit-webfont.eot");
  src: url("../fonts/solitas-norligit-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-norligit-webfont.woff2") format("woff2"), url("../fonts/solitas-norligit-webfont.woff") format("woff"), url("../fonts/solitas-norligit-webfont.ttf") format("truetype"), url("../fonts/solitas-norligit-webfont.svg#solitas_norm_light_itnormLtIt") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_norm_book';
  src: url("../fonts/solitas-norboo-webfont.eot");
  src: url("../fonts/solitas-norboo-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-norboo-webfont.woff2") format("woff2"), url("../fonts/solitas-norboo-webfont.woff") format("woff"), url("../fonts/solitas-norboo-webfont.ttf") format("truetype"), url("../fonts/solitas-norboo-webfont.svg#solitas_norm_booknorm_book") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_norm_book_i';
  src: url("../fonts/solitas-norbooit-webfont.eot");
  src: url("../fonts/solitas-norbooit-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-norbooit-webfont.woff2") format("woff2"), url("../fonts/solitas-norbooit-webfont.woff") format("woff"), url("../fonts/solitas-norbooit-webfont.ttf") format("truetype"), url("../fonts/solitas-norbooit-webfont.svg#solitas_norm_book_itanormBkIt") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_norm_regular';
  src: url("../fonts/solitas-norreg-webfont.eot");
  src: url("../fonts/solitas-norreg-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-norreg-webfont.woff2") format("woff2"), url("../fonts/solitas-norreg-webfont.woff") format("woff"), url("../fonts/solitas-norreg-webfont.ttf") format("truetype"), url("../fonts/solitas-norreg-webfont.svg#solitas_norm_regularnormRg") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_norm_regular_i';
  src: url("../fonts/solitas-norregit-webfont.eot");
  src: url("../fonts/solitas-norregit-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-norregit-webfont.woff2") format("woff2"), url("../fonts/solitas-norregit-webfont.woff") format("woff"), url("../fonts/solitas-norregit-webfont.ttf") format("truetype"), url("../fonts/solitas-norregit-webfont.svg#solitas_norm_regular_normRgIt") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_norm_medium';
  src: url("../fonts/solitas-normed-webfont.eot");
  src: url("../fonts/solitas-normed-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-normed-webfont.woff2") format("woff2"), url("../fonts/solitas-normed-webfont.woff") format("woff"), url("../fonts/solitas-normed-webfont.ttf") format("truetype"), url("../fonts/solitas-normed-webfont.svg#solitas_norm_mediumnormMd") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_norm_demi';
  src: url("../fonts/solitas-nordem-webfont.eot");
  src: url("../fonts/solitas-nordem-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-nordem-webfont.woff2") format("woff2"), url("../fonts/solitas-nordem-webfont.woff") format("woff"), url("../fonts/solitas-nordem-webfont.ttf") format("truetype"), url("../fonts/solitas-nordem-webfont.svg#solitas_norm_deminorm_demi") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_norm_demi_i';
  src: url("../fonts/solitas-nordemit-webfont.eot");
  src: url("../fonts/solitas-nordemit-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-nordemit-webfont.woff2") format("woff2"), url("../fonts/solitas-nordemit-webfont.woff") format("woff"), url("../fonts/solitas-nordemit-webfont.ttf") format("truetype"), url("../fonts/solitas-nordemit-webfont.svg#solitas_norm_demi_inormdemiIt") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_norm_bold';
  src: url("../fonts/solitas-norbol-webfont.eot");
  src: url("../fonts/solitas-norbol-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-norbol-webfont.woff2") format("woff2"), url("../fonts/solitas-norbol-webfont.woff") format("woff"), url("../fonts/solitas-norbol-webfont.ttf") format("truetype"), url("../fonts/solitas-norbol-webfont.svg#solitas_norm_boldnorm_bold") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_ext_thin';
  src: url("../fonts/solitas-extthi-webfont.eot");
  src: url("../fonts/solitas-extthi-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-extthi-webfont.woff2") format("woff2"), url("../fonts/solitas-extthi-webfont.woff") format("woff"), url("../fonts/solitas-extthi-webfont.ttf") format("truetype"), url("../fonts/solitas-extthi-webfont.svg#solitas_ext_thinext_thin") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_ext_light';
  src: url("../fonts/solitas-extlig-webfont.eot");
  src: url("../fonts/solitas-extlig-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-extlig-webfont.woff2") format("woff2"), url("../fonts/solitas-extlig-webfont.woff") format("woff"), url("../fonts/solitas-extlig-webfont.ttf") format("truetype"), url("../fonts/solitas-extlig-webfont.svg#solitas_ext_lightext_light") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_ext_book';
  src: url("../fonts/solitas-extboo-webfont.eot");
  src: url("../fonts/solitas-extboo-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-extboo-webfont.woff2") format("woff2"), url("../fonts/solitas-extboo-webfont.woff") format("woff"), url("../fonts/solitas-extboo-webfont.ttf") format("truetype"), url("../fonts/solitas-extboo-webfont.svg#solitas_ext_bookext_book") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_ext_medium';
  src: url("../fonts/solitas-extmed-webfont.eot");
  src: url("../fonts/solitas-extmed-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-extmed-webfont.woff2") format("woff2"), url("../fonts/solitas-extmed-webfont.woff") format("woff"), url("../fonts/solitas-extmed-webfont.ttf") format("truetype"), url("../fonts/solitas-extmed-webfont.svg#solitas_ext_mediumext_medium") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_ext_demi';
  src: url("../fonts/solitas-extdem-webfont.eot");
  src: url("../fonts/solitas-extdem-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-extdem-webfont.woff2") format("woff2"), url("../fonts/solitas-extdem-webfont.woff") format("woff"), url("../fonts/solitas-extdem-webfont.ttf") format("truetype"), url("../fonts/solitas-extdem-webfont.svg#solitas_ext_demiext_demi") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'solitas_ext_bold';
  src: url("../fonts/solitas-extbol-webfont.eot");
  src: url("../fonts/solitas-extbol-webfont.eot?#iefix") format("embedded-opentype"), url("../fonts/solitas-extbol-webfont.woff2") format("woff2"), url("../fonts/solitas-extbol-webfont.woff") format("woff"), url("../fonts/solitas-extbol-webfont.ttf") format("truetype"), url("../fonts/solitas-extbol-webfont.svg#solitas_ext_boldext_bold") format("svg");
  font-weight: normal;
  font-style: normal;
}
/* Type */
html, body {
  color: #000000;
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 16px;
  font-weight: normal;
  line-height: 28px;
  /*@include breakpoint(xlarge-min) {
      font-size: 16px;
  }*/
}

a {
  text-decoration: none;
  color: inherit;
}
a:hover {
  color: inherit;
}

strong, b {
  font-family: "solitas_norm_medium", sans-serif;
  font-weight: normal;
}

em, i {
  font-style: italic;
}

p {
  margin: 0;
  font-size: inherit;
  line-height: 2;
}
p:not(:last-child) {
  margin: 0 0 1.25em 0;
}

h1, h2, h3, h4, h5, h6 {
  font-family: "solitas_norm_medium", sans-serif;
  font-weight: normal;
  margin: 0 0 1.25em 0;
}
h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {
  color: inherit;
  text-decoration: none;
}
@media screen and (max-width: 63.9375em) {
  h1, h2, h3, h4, h5, h6 {
    margin: 0 0 0.625em 0;
  }
}

h1 {
  font-size: 2.25em;
}

h2 {
  font-size: 2em;
}

h3 {
  font-size: 1.75em;
}

h4 {
  font-size: 1.5em;
}

h5 {
  font-size: 1.25em;
}

h6 {
  font-size: 1em;
}

@media screen and (max-width: 47.9375em) {
  h2 {
    font-size: 1em;
  }

  h3 {
    font-size: 0.8em;
  }
}
sub {
  font-size: 0.8em;
  position: relative;
  top: 0.5em;
}

sup {
  font-size: 0.8em;
  position: relative;
  top: -0.5em;
}

blockquote {
  font-family: "solitas_norm_bold", sans-serif;
  font-style: italic;
  margin: 0 0 1.25em 0;
}

code {
  background: #f2f2f2;
  font-family: monospace;
  font-size: 0.9em;
  margin: 0 0.25em;
  padding: 0.25em 0.65em;
}

pre {
  -webkit-overflow-scrolling: touch;
  font-family: monospace;
  font-size: 0.9em;
  margin: 0 0 1.25em 0;
}
pre code {
  display: block;
  line-height: 1.75em;
  padding: 1em 1.5em;
  overflow-x: auto;
}

hr {
  border: 0;
  border-bottom: solid 1px currentcolor;
  opacity: 0.1;
  margin: 2.49375em 0;
}
@media screen and (max-width: 47.9375em) {
  hr {
    margin: 1.3125em 0;
  }
}

button, input, optgroup, select, textarea {
  color: inherit;
  font: inherit;
  margin: 0;
}

table {
  border-collapse: collapse;
  border-spacing: 0;
}
table td, table th {
  padding: 0;
  font-weight: inherit;
}
@media screen and (min-width: 48em) {
  table td, table th {
    text-align: left;
  }
}

fieldset {
  border-width: 0;
  margin: 0;
  padding: 0;
}

.text--left {
  text-align: left;
}

.text--right {
  text-align: right;
}

.text--center {
  text-align: center;
}

.text--justify {
  text-align: justify;
}

.text--underline {
  text-decoration: underline;
}

.text--overline {
  text-decoration: overline;
}

.border.border--top {
  border-top: 1px solid rgba(0, 0, 0, 0.1);
}
.border.border--left {
  border-left: 1px solid rgba(0, 0, 0, 0.1);
}
.border.border--right {
  border-right: 1px solid rgba(0, 0, 0, 0.1);
}
.border.border--bottom {
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
}

.clearfix:after {
  display: table;
  clear: both;
  content: "";
}

@media screen and (min-width: 48em) {
  .fl--left {
    float: left;
  }

  .fl--right {
    float: right;
  }
}
/* List */
ol {
  list-style: decimal;
  margin: 0 0 1.25em 0;
  padding: 0;
}

ul {
  list-style: disc;
  margin: 0;
  padding: 0;
}
ul:not(:last-child) {
  margin-bottom: 1.25em;
}

.list.list--horizontal {
  cursor: default;
  list-style: none;
}
.list.list--horizontal > * {
  display: inline-block;
  vertical-align: middle;
}
@media screen and (min-width: 48em) {
  .list.list--horizontal.list--2 > *, .list.list--horizontal.list--3 > *, .list.list--horizontal.list--4 > *, .list.list--horizontal.list--5 > * {
    width: 49.5%;
  }
}
@media screen and (min-width: 64em) {
  .list.list--horizontal.list--3 > *, .list.list--horizontal.list--4 > *, .list.list--horizontal.list--5 > * {
    width: 32.83333%;
  }
}
@media screen and (min-width: 80em) {
  .list.list--horizontal.list--4 > * {
    width: 24.5%;
  }
  .list.list--horizontal.list--5 > * {
    width: 19.5%;
  }
}
.list.list--vertical {
  cursor: default;
  list-style: none;
  padding-left: 0;
}
.list.list--vertical > * {
  display: block;
}
.list.list--fit {
  display: table;
  table-layout: fixed;
  cursor: default;
  list-style: none;
  width: 100%;
}
.list.list--fit > * {
  display: table-cell;
  width: 50%;
}
.list.list--bullet > * {
  position: relative;
  padding-left: 2em;
  line-height: 2em;
}
.list.list--bullet > *:before {
  content: " ";
  position: absolute;
  top: 0.7em;
  left: 0;
  display: inline-block;
  vertical-align: middle;
  width: 0.6em;
  height: 0.6em;
  margin-right: 0.625em;
  -webkit-border-radius: 1em;
  border-radius: 1em;
  background-color: #ffffff;
  border: 2px solid;
}

dl dt {
  display: block;
  font-family: "solitas_norm_bold", sans-serif;
  font-weight: normal;
  margin: 0 0 0.625em 0;
}
dl dd {
  margin-left: 1.25em;
}

.no--margin {
  margin: 0 !important;
}

.no--margin-left {
  margin-left: 0 !important;
}

.no--margin-right {
  margin-right: 0 !important;
}

.no--margin-top {
  margin-top: 0 !important;
}

.no--margin-bottom {
  margin-bottom: 0 !important;
}

.no--padding {
  padding: 0 !important;
}

.no--padding-left {
  padding-left: 0 !important;
}

.no--padding-right {
  padding-right: 0 !important;
}

.no--padding-top {
  padding-top: 0 !important;
}

.no--padding-bottom {
  padding-bottom: 0 !important;
}

.scrollable--y {
  -webkit-overflow-scrolling: touch;
  max-height: 100%;
  overflow: hidden;
  overflow-y: auto;
}

.scrollable--x {
  -webkit-overflow-scrolling: touch;
  max-width: 100%;
  overflow: hidden;
  overflow-x: auto;
}

.scrollable--hidden {
  -webkit-overflow-scrolling: touch;
  max-width: 100%;
  overflow: hidden;
}

/* Type Colors */
.text--primary {
  color: #67c9d3 !important;
}

.text--secondary {
  color: #00153b !important;
}

.text--blue {
  color: #0599b2 !important;
}

.text--green {
  color: #76b043 !important;
}

.text--brown {
  color: #886e5c !important;
}

.text--red {
  color: #d03e2a !important;
}

.text--orange {
  color: #f99d34 !important;
}

.text--facebook {
  color: #3a5795 !important;
}

.text--twitter {
  color: #55acee !important;
}

.text--linkedin {
  color: #006fa6 !important;
}

.text--base {
  color: #f3f6fa !important;
}

/* Type */
.heading-text {
  display: block;
  margin: 0;
  padding: 0;
  font-family: "solitas_ext_book", sans-serif;
  font-size: 2em;
  font-weight: normal;
  line-height: 1;
  letter-spacing: 0.02em;
}
.heading-text:not(:last-child) {
  margin-bottom: 0.46875em;
}
.heading-text.heading-text--small {
  font-size: 1.4em;
}
.heading-text.heading-text--small:not(:last-child) {
  margin-bottom: 0.44643em;
}
@media screen and (max-width: 79.9375em) {
  .heading-text.heading-text--large {
    font-size: 4em;
    letter-spacing: 0.00781em;
  }
  .heading-text.heading-text--large:not(:last-child) {
    margin-bottom: 0.3125em;
  }
}

.special-heading-text {
  display: block;
  margin: 0;
  padding: 0;
  font-family: "brusher_regular", sans-serif;
  font-size: 4em;
  font-weight: normal;
  line-height: 1;
  letter-spacing: 0.02em;
}
.special-heading-text:not(:last-child) {
  margin-bottom: 0.3125em;
}
@media screen and (max-width: 79.9375em) {
  .special-heading-text {
    font-size: 3em;
  }
  .special-heading-text:not(:last-child) {
    margin-bottom: 0.41667em;
  }
}
@media screen and (max-width: 47.9375em) {
  .special-heading-text {
    font-size: 2.25em;
  }
  .special-heading-text:not(:last-child) {
    margin-bottom: 0.55556em;
  }
}

.block-heading-text {
  display: block;
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.5em;
  font-weight: normal;
  line-height: 1;
  letter-spacing: 0.02em;
  text-transform: uppercase;
}
.block-heading-text:not(:last-child) {
  margin-bottom: 0.9375em;
}
.block-heading-text.block-heading-text--small {
  font-size: 1.25em;
}
.block-heading-text.block-heading-text--large {
  font-size: 1.75em;
}

.sub-heading-text {
  display: block;
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 1em;
  line-height: 1;
  letter-spacing: 0.3125em;
  text-transform: uppercase;
}
.sub-heading-text:not(:last-child) {
  margin-bottom: 0.9375em;
}
.sub-heading-text.sub-heading-text--small {
  font-size: 0.75em;
}
.sub-heading-text.sub-heading-text--large {
  font-size: 1.25em;
}

.regular-text {
  font-size: 1rem;
  line-height: 1.75rem;
}
.regular-text.regular-text--small {
  font-family: "solitas_norm_book", sans-serif;
  font-size: 0.7rem;
}
.regular-text.regular-text--large {
  font-family: "solitas_norm_light", sans-serif;
  font-size: 1.3rem;
}

@media screen and (max-width: 79.9375em) {
  .heading-text {
    font-size: 1.8em;
  }
  .heading-text.heading-text--large {
    font-size: 3em;
  }
}
@media screen and (max-width: 47.9375em) {
  .heading-text, .heading-text.heading-text--large {
    font-size: 2em;
  }
}
.assistive__text {
  position: absolute !important;
  margin: -1px !important;
  border: 0 !important;
  padding: 0 !important;
  width: 1px !important;
  height: 1px !important;
  overflow: hidden !important;
  clip: rect(0 0 0 0) !important;
}

@media screen and (max-width: 47.9375em) {
  .hidden-on--mobile {
    display: none !important;
  }
}
@media screen and (min-width: 48em) {
  .hidden-on--tablet {
    display: none !important;
  }
}
@media screen and (min-width: 80em) {
  .hidden-on--tablet {
    display: block !important;
  }

  .hidden-on--desktop {
    display: none !important;
  }
}
.visible-on--mobile,
.visible-on--tablet,
.visible-on--desktop {
  display: none !important;
}

@media screen and (max-width: 47.9375em) {
  .visible-on--mobile {
    display: block !important;
  }
}
@media screen and (min-width: 48em) {
  .visible-on--tablet {
    display: block !important;
  }
}
@media screen and (min-width: 80em) {
  .visible-on--desktop {
    display: block !important;
  }

  .visible-on--tablet {
    display: none !important;
  }
}
.system-message {
  position: fixed;
  display: block;
  width: 100%;
  left: 0;
  right: 0;
  bottom: -1px;
  z-index: 6000;
}
.system-message ul {
  margin: 0;
  padding: 0;
  list-style: none;
}
.system-message .div_msg,
.system-message .div_error {
  padding: 0.3125em 1.25em;
  display: block;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 0.875em;
  letter-spacing: 0.02em;
  text-align: center;
  white-space: nowrap;
}
.system-message .div_msg {
  background-color: #d2e4c1;
  color: #76b043;
}
.system-message .div_error {
  background-color: #efc3bd;
  color: #d03e2a;
}

.alert {
  position: fixed;
  width: 100%;
  left: 0;
  right: 0;
  bottom: -1px;
  z-index: 6000;
  /*-webkit-box-shadow: 0 -1px 7px 0 rgba(_palette(sec), 0.1);
          box-shadow: 0 -1px 7px 0 rgba(_palette(sec), 0.1);*/
  -webkit-transition: bottom 0.3s linear;
  transition: bottom 0.3s linear;
}
.alert > div {
  display: block;
  padding: 0.3125em 1.25em;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 0.875em;
  letter-spacing: 0.02em;
  text-align: center;
  white-space: nowrap;
}
.alert:nth-of-type(2):not(.site-fixed-sidebar) {
  bottom: 0px;
}
.alert:nth-of-type(3):not(.site-fixed-sidebar) {
  bottom: 36px;
}
.alert:nth-of-type(4):not(.site-fixed-sidebar) {
  bottom: 72px;
}
.alert:nth-of-type(5):not(.site-fixed-sidebar) {
  bottom: 108px;
}
.alert:nth-of-type(6):not(.site-fixed-sidebar) {
  bottom: 144px;
}
.alert:nth-of-type(7):not(.site-fixed-sidebar) {
  bottom: 180px;
}
.alert:nth-of-type(8):not(.site-fixed-sidebar) {
  bottom: 216px;
}
.alert:nth-of-type(9):not(.site-fixed-sidebar) {
  bottom: 252px;
}
.alert:nth-of-type(10):not(.site-fixed-sidebar) {
  bottom: 288px;
}
.alert .close {
  cursor: pointer;
}
.alert .content,
.alert .close {
  display: inline-block;
  vertical-align: middle;
}
.alert .content:not(:first-child),
.alert .close:not(:first-child) {
  margin-left: 1.25em;
}
.alert.alert_danger > div {
  background-color: #efc3bd;
  color: #d03e2a;
}
.alert.alert_success > div {
  background-color: #d2e4c1;
  color: #76b043;
}
.alert.alert_warning > div {
  background-color: #fdf1e2;
  color: #f99d34;
}
.alert.alert_info > div {
  background-color: #d3cac4;
  color: #886e5c;
}
.alert.has--click {
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 9010;
  background-color: rgba(243, 246, 250, 0.7);
}
.alert.has--click > div {
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
}
.alert.has--click:nth-of-type(3) > div {
  bottom: 36px;
}
.alert.has--click:nth-of-type(4) > div {
  bottom: 72px;
}
.alert.has--click:nth-of-type(5) > div {
  bottom: 108px;
}
.alert.has--click:nth-of-type(6) > div {
  bottom: 144px;
}
.alert.has--click:nth-of-type(7) > div {
  bottom: 180px;
}
.alert.has--click:nth-of-type(8) > div {
  bottom: 216px;
}
.alert.has--click:nth-of-type(9) > div {
  bottom: 252px;
}
.alert.has--click:nth-of-type(10) > div {
  bottom: 288px;
}

.breadcrumb {
  list-style: none;
  margin: 0;
  padding: 0;
}
.breadcrumb a:not(.button) {
  position: relative;
  display: block;
  padding: 0 0.46875rem 0 0.9375rem;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.125em;
}
.breadcrumb a:not(.button):hover {
  text-decoration: none;
}
.breadcrumb a:not(.button):before {
  content: '/';
  position: absolute;
  left: 0;
}
.breadcrumb > *:last-child a {
  font-family: "solitas_norm_book", sans-serif;
}
.breadcrumb > *:first-child a {
  padding-left: 0;
}
.breadcrumb > *:first-child a:before {
  content: '';
}
@media screen and (min-width: 48em) {
  .breadcrumb a:not(.button) {
    font-size: 1.125em;
  }
}

/* Button */
.buttons__group {
  margin-top: -0.625em;
}
.buttons__group .button, .buttons__group .img-uploader .upload-label, .img-uploader .buttons__group .upload-label {
  margin-top: 0.625em;
}
@media screen and (min-width: 48em) {
  .buttons__group {
    white-space: nowrap;
  }
}

.s-button {
  display: inline-block;
  width: 100%;
  max-width: 31.25em;
  padding: 0 0.71429em;
  line-height: 3.57143em;
  min-height: calc(3.57143em + 2px);
  min-width: 3.57143em;
  border-radius: 0;
  font-size: 0.875em;
  text-align: center;
  color: inherit;
  background-color: #fff;
  border: 1px solid #e3e9f0;
  transition: border 0.3s linear, background 0.3s linear;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  text-transform: uppercase;
}
.s-button::-ms-expand {
  display: none;
}

.button, .img-uploader .upload-label {
  -moz-appearance: none;
  -webkit-appearance: none;
  appearance: none;
  cursor: pointer;
  display: inline-block;
  vertical-align: middle;
  height: calc(2.8125em);
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 1em;
  font-weight: normal;
  letter-spacing: 0.3125em;
  line-height: 2.8125em;
  padding: 0 1.21875em 0 1.40625em;
  text-align: center;
  text-decoration: none;
  text-transform: uppercase;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  -webkit-border-radius: 2.8125em;
  border-radius: 2.8125em;
  border: 1px solid transparent;
  background-color: transparent;
  color: #00153b;
  min-width: 7.5em;
  -webkit-transition: background-color 0.2s ease-in-out;
  transition: background-color 0.2s ease-in-out;
}
.button.button--icon, .img-uploader .button--icon.upload-label {
  white-space: nowrap;
}
.button.button--icon:before:last-child, .img-uploader .button--icon.upload-label:before:last-child, .button.button--icon span:last-child, .img-uploader .button--icon.upload-label span:last-child {
  margin-left: 0.5em;
}
.button.button--icon:before:first-child, .img-uploader .button--icon.upload-label:before:first-child, .button.button--icon span:first-child, .img-uploader .button--icon.upload-label span:first-child {
  margin-right: 0.5em;
}
.button.button--circle, .img-uploader .button--circle.upload-label {
  padding: 0;
  text-align: center;
  width: 2.8125em;
  min-width: 0;
}
.button.button--fit, .img-uploader .button--fit.upload-label {
  display: block;
  margin: 0 auto 1.25em auto;
  width: 100%;
  max-width: 31.25em;
}
.button.button--small, .img-uploader .upload-label {
  font-size: 0.8em;
}
.button.button--large, .img-uploader .button--large.upload-label {
  font-size: 1.2em;
}
.button.button--disabled, .img-uploader .button--disabled.upload-label, .button:disabled, .img-uploader .upload-label:disabled {
  opacity: 0.25;
  cursor: not-allowed;
  pointer-events: none;
}
.button.button--focus, .img-uploader .button--focus.upload-label, .button:focus, .img-uploader .upload-label:focus {
  box-shadow: 0 0 0 5px rgba(171, 192, 220, 0.1);
}
.button.button--fill, .img-uploader .button--fill.upload-label {
  color: #ffffff;
}
.button.button--fill:hover, .img-uploader .button--fill.upload-label:hover, .button.button--fill:active, .img-uploader .button--fill.upload-label:active {
  border-color: rgba(255, 255, 255, 0.2);
}
.button.button--fill.button--primary, .img-uploader .button--fill.button--primary.upload-label {
  background-color: #67c9d3;
}
.button.button--fill.button--primary:hover, .img-uploader .button--fill.button--primary.upload-label:hover, .button.button--fill.button--primary:active, .img-uploader .button--fill.button--primary.upload-label:active, .button.button--fill.button--primary.has--active, .img-uploader .button--fill.button--primary.has--active.upload-label {
  background-color: #00153b;
}
.button.button--fill.button--secondary, .img-uploader .button--fill.button--secondary.upload-label {
  background-color: #00153b;
}
.button.button--fill.button--secondary:hover, .img-uploader .button--fill.button--secondary.upload-label:hover, .button.button--fill.button--secondary:active, .img-uploader .button--fill.button--secondary.upload-label:active, .button.button--fill.button--secondary.has--active, .img-uploader .button--fill.button--secondary.has--active.upload-label {
  background-color: #67c9d3;
}
.button.button--fill.button--blue, .img-uploader .button--fill.button--blue.upload-label {
  background-color: #0599b2;
}
.button.button--fill.button--blue:hover, .img-uploader .button--fill.button--blue.upload-label:hover, .button.button--fill.button--blue:active, .img-uploader .button--fill.button--blue.upload-label:active, .button.button--fill.button--blue.has--active, .img-uploader .button--fill.button--blue.has--active.upload-label {
  background-color: #00153b;
}
.button.button--fill.button--red, .img-uploader .button--fill.button--red.upload-label {
  background-color: #d03e2a;
}
.button.button--fill.button--red:hover, .img-uploader .button--fill.button--red.upload-label:hover, .button.button--fill.button--red:active, .img-uploader .button--fill.button--red.upload-label:active, .button.button--fill.button--red.has--active, .img-uploader .button--fill.button--red.has--active.upload-label {
  background-color: #00153b;
}
.button.button--fill.button--orange, .img-uploader .button--fill.button--orange.upload-label {
  background-color: #f99d34;
}
.button.button--fill.button--orange:hover, .img-uploader .button--fill.button--orange.upload-label:hover, .button.button--fill.button--orange:active, .img-uploader .button--fill.button--orange.upload-label:active, .button.button--fill.button--orange.has--active, .img-uploader .button--fill.button--orange.has--active.upload-label {
  background-color: #00153b;
}
.button.button--fill.button--green, .img-uploader .button--fill.button--green.upload-label {
  background-color: #76b043;
}
.button.button--fill.button--green:hover, .img-uploader .button--fill.button--green.upload-label:hover, .button.button--fill.button--green:active, .img-uploader .button--fill.button--green.upload-label:active, .button.button--fill.button--green.has--active, .img-uploader .button--fill.button--green.has--active.upload-label {
  background-color: #00153b;
}
.button.button--fill.button--brown, .img-uploader .button--fill.button--brown.upload-label {
  background-color: #886e5c;
}
.button.button--fill.button--brown:hover, .img-uploader .button--fill.button--brown.upload-label:hover, .button.button--fill.button--brown:active, .img-uploader .button--fill.button--brown.upload-label:active, .button.button--fill.button--brown.has--active, .img-uploader .button--fill.button--brown.has--active.upload-label {
  background-color: #00153b;
}
.button.button--fill.button--facebook, .img-uploader .button--fill.button--facebook.upload-label {
  background-color: #3a5795;
}
.button.button--fill.button--facebook:hover, .img-uploader .button--fill.button--facebook.upload-label:hover, .button.button--fill.button--facebook:active, .img-uploader .button--fill.button--facebook.upload-label:active, .button.button--fill.button--facebook.has--active, .img-uploader .button--fill.button--facebook.has--active.upload-label {
  background-color: #00153b;
}
.button.button--fill.button--twitter, .img-uploader .button--fill.button--twitter.upload-label {
  background-color: #55acee;
}
.button.button--fill.button--twitter:hover, .img-uploader .button--fill.button--twitter.upload-label:hover, .button.button--fill.button--twitter:active, .img-uploader .button--fill.button--twitter.upload-label:active, .button.button--fill.button--twitter.has--active, .img-uploader .button--fill.button--twitter.has--active.upload-label {
  background-color: #00153b;
}
.button.button--fill.button--linkedin, .img-uploader .button--fill.button--linkedin.upload-label {
  background-color: #006fa6;
}
.button.button--fill.button--linkedin:hover, .img-uploader .button--fill.button--linkedin.upload-label:hover, .button.button--fill.button--linkedin:active, .img-uploader .button--fill.button--linkedin.upload-label:active, .button.button--fill.button--linkedin.has--active, .img-uploader .button--fill.button--linkedin.has--active.upload-label {
  background-color: #00153b;
}
.button.button--fill.button--pinterest, .img-uploader .button--fill.button--pinterest.upload-label {
  background-color: #cb2027;
}
.button.button--fill.button--pinterest:hover, .img-uploader .button--fill.button--pinterest.upload-label:hover, .button.button--fill.button--pinterest:active, .img-uploader .button--fill.button--pinterest.upload-label:active, .button.button--fill.button--pinterest.has--active, .img-uploader .button--fill.button--pinterest.has--active.upload-label {
  background-color: #00153b;
}
.button.button--fill.button--dark, .img-uploader .button--fill.button--dark.upload-label {
  background-color: #e1e9f3;
  color: #00153b;
}
.button.button--fill.button--dark:hover, .img-uploader .button--fill.button--dark.upload-label:hover, .button.button--fill.button--dark:active, .img-uploader .button--fill.button--dark.upload-label:active, .button.button--fill.button--dark.has--active, .img-uploader .button--fill.button--dark.has--active.upload-label {
  background-color: #00153b;
  color: #ffffff;
}
.button.button--non-fill:hover, .img-uploader .button--non-fill.upload-label:hover, .button.button--non-fill:active, .img-uploader .button--non-fill.upload-label:active {
  color: #ffffff;
}
.button.button--non-fill.button--primary, .img-uploader .button--non-fill.button--primary.upload-label {
  border-color: #67c9d3;
  color: #67c9d3;
}
.button.button--non-fill.button--primary:hover, .img-uploader .button--non-fill.button--primary.upload-label:hover, .button.button--non-fill.button--primary:active, .img-uploader .button--non-fill.button--primary.upload-label:active, .button.button--non-fill.button--primary.has--active, .img-uploader .button--non-fill.button--primary.has--active.upload-label {
  background-color: #67c9d3;
  color: #ffffff;
}
.button.button--non-fill.button--secondary, .img-uploader .button--non-fill.button--secondary.upload-label {
  border-color: #00153b;
  color: #00153b;
}
.button.button--non-fill.button--secondary:hover, .img-uploader .button--non-fill.button--secondary.upload-label:hover, .button.button--non-fill.button--secondary:active, .img-uploader .button--non-fill.button--secondary.upload-label:active, .button.button--non-fill.button--secondary.has--active, .img-uploader .button--non-fill.button--secondary.has--active.upload-label {
  background-color: #00153b;
  color: #ffffff;
}
.button.button--non-fill.button--blue, .img-uploader .button--non-fill.button--blue.upload-label {
  border-color: #0599b2;
  color: #0599b2;
}
.button.button--non-fill.button--blue:hover, .img-uploader .button--non-fill.button--blue.upload-label:hover, .button.button--non-fill.button--blue:active, .img-uploader .button--non-fill.button--blue.upload-label:active, .button.button--non-fill.button--blue.has--active, .img-uploader .button--non-fill.button--blue.has--active.upload-label {
  background-color: #0599b2;
  color: #ffffff;
}
.button.button--non-fill.button--red, .img-uploader .button--non-fill.button--red.upload-label {
  border-color: #d03e2a;
  color: #d03e2a;
}
.button.button--non-fill.button--red:hover, .img-uploader .button--non-fill.button--red.upload-label:hover, .button.button--non-fill.button--red:active, .img-uploader .button--non-fill.button--red.upload-label:active, .button.button--non-fill.button--red.has--active, .img-uploader .button--non-fill.button--red.has--active.upload-label {
  background-color: #d03e2a;
  color: #ffffff;
}
.button.button--non-fill.button--orange, .img-uploader .button--non-fill.button--orange.upload-label {
  border-color: #f99d34;
  color: #f99d34;
}
.button.button--non-fill.button--orange:hover, .img-uploader .button--non-fill.button--orange.upload-label:hover, .button.button--non-fill.button--orange:active, .img-uploader .button--non-fill.button--orange.upload-label:active, .button.button--non-fill.button--orange.has--active, .img-uploader .button--non-fill.button--orange.has--active.upload-label {
  background-color: #f99d34;
  color: #ffffff;
}
.button.button--non-fill.button--green, .img-uploader .button--non-fill.button--green.upload-label {
  border-color: #76b043;
  color: #76b043;
}
.button.button--non-fill.button--green:hover, .img-uploader .button--non-fill.button--green.upload-label:hover, .button.button--non-fill.button--green:active, .img-uploader .button--non-fill.button--green.upload-label:active, .button.button--non-fill.button--green.has--active, .img-uploader .button--non-fill.button--green.has--active.upload-label {
  background-color: #76b043;
  color: #ffffff;
}
.button.button--non-fill.button--brown, .img-uploader .button--non-fill.button--brown.upload-label {
  border-color: #886e5c;
  color: #886e5c;
}
.button.button--non-fill.button--brown:hover, .img-uploader .button--non-fill.button--brown.upload-label:hover, .button.button--non-fill.button--brown:active, .img-uploader .button--non-fill.button--brown.upload-label:active, .button.button--non-fill.button--brown.has--active, .img-uploader .button--non-fill.button--brown.has--active.upload-label {
  background-color: #886e5c;
  color: #ffffff;
}
.button.button--non-fill.button--dark, .img-uploader .button--non-fill.button--dark.upload-label {
  border-color: #e1e9f3;
  color: #00153b;
}
.button.button--non-fill.button--dark:hover, .img-uploader .button--non-fill.button--dark.upload-label:hover, .button.button--non-fill.button--dark:active, .img-uploader .button--non-fill.button--dark.upload-label:active, .button.button--non-fill.button--dark.has--active, .img-uploader .button--non-fill.button--dark.has--active.upload-label {
  background-color: #e1e9f3;
}
.button.button--label, .img-uploader .button--label.upload-label {
  padding: 0;
  margin-right: -3px;
  pointer-events: none;
}
.button.button--label.button--primary, .img-uploader .button--label.button--primary.upload-label {
  color: #67c9d3;
}
.button.button--label.button--secondary, .img-uploader .button--label.button--secondary.upload-label {
  color: #00153b;
}
.button.button--label.button--blue, .img-uploader .button--label.button--blue.upload-label {
  color: #0599b2;
}
.button.button--label.button--red, .img-uploader .button--label.button--red.upload-label {
  color: #d03e2a;
}
.button.button--label.button--orange, .img-uploader .button--label.button--orange.upload-label {
  color: #f99d34;
}
.button.button--label.button--green, .img-uploader .button--label.button--green.upload-label {
  color: #76b043;
}
.button.button--label.button--brown, .img-uploader .button--label.button--brown.upload-label {
  color: #886e5c;
}
.button.button--label.button--dark, .img-uploader .button--label.button--dark.upload-label {
  color: #cfdbeb;
}
.button.button--label:hover, .img-uploader .button--label.upload-label:hover, .button.button--label:active, .img-uploader .button--label.upload-label:active {
  background-color: #f3f6fa;
}
@media screen and (min-width: 30em) {
  .button:not(:first-of-type), .img-uploader .upload-label:not(:first-of-type) {
    margin-left: 0.625em;
  }
}
@media screen and (max-width: 63.9375em) {
  .button, .img-uploader .upload-label {
    padding: 0 1.21875em 0 1.40625em;
    min-width: 2.8125em;
  }
}
@media screen and (max-width: 29.9375em) {
  .button, .img-uploader .upload-label {
    width: 100%;
  }
  .button:not(:first-of-type), .img-uploader .upload-label:not(:first-of-type) {
    margin-top: 0.625em;
  }
}

.carousel {
  position: relative;
  display: block;
}

.dropdown {
  position: relative;
}
.dropdown .dropdown__target {
  position: absolute;
  z-index: 2000;
  top: 100%;
  left: 50%;
  display: none;
  min-width: 10em;
  padding: 0.5em 0;
  line-height: 2;
  background: #fff;
  color: #00153b;
  border: 1px solid #e1e9f3;
  box-shadow: 0 5px 5px 0 rgba(0, 0, 0, 0.08);
  -webkit-transform: translateX(-50%);
  transform: translateX(-50%);
}
.dropdown .dropdown__target a:hover, .dropdown .dropdown__target a:focus {
  background-color: #e1e9f3;
}
.dropdown .dropdown__trigger {
  position: relative;
  display: block;
}
.dropdown .dropdown__trigger:after {
  display: inline-block;
  vertical-align: middle;
  content: " ";
  height: 0;
  width: 0;
  margin-left: 0.3125em;
  border: solid transparent;
  border-width: 5px;
  border-top-color: currentColor;
  border-bottom-width: 0;
  pointer-events: none;
}
.dropdown.dropdown--floated {
  padding-bottom: 0.625em;
}
.dropdown.dropdown--floated .dropdown__target {
  top: 100%;
  -webkit-border-radius: 0.625em;
  border-radius: 0.625em;
}
.dropdown.dropdown--floated .dropdown__target:before {
  position: absolute;
  top: -10px;
  left: 50%;
  -webkit-transform: translate(-50%, 0);
  transform: translate(-50%, 0);
  display: inline-block;
  content: " ";
  height: 0;
  width: 0;
  border: solid transparent;
  border-width: 10px;
  border-top-width: 0;
  border-bottom-color: #e1e9f3;
}
.dropdown.dropdown--floated .dropdown__target:after {
  position: absolute;
  top: -8px;
  left: 50%;
  -webkit-transform: translate(-50%, 0);
  transform: translate(-50%, 0);
  display: inline-block;
  content: " ";
  height: 0;
  width: 0;
  border: solid transparent;
  border-width: 10px;
  border-top-width: 0;
  border-bottom-color: #fff;
}
.dropdown:hover .dropdown__target {
  display: block;
}

input:-webkit-autofill, input:-webkit-autofill:hover, input:-webkit-autofill:focus, input:-webkit-autofill:active, textarea:-webkit-autofill, textarea:-webkit-autofill:hover, textarea:-webkit-autofill:focus, textarea:-webkit-autofill:active, select:-webkit-autofill, select:-webkit-autofill:hover, select:-webkit-autofill:focus, select:-webkit-autofill:active {
  background-color: inherit !important;
  color: currentColor !important;
  -webkit-text-fill-color: currentColor !important;
  -webkit-transition-delay: 9999s;
  transition-delay: 9999s;
  -webkit-transition-property: background-color, color;
  transition-property: background-color, color;
}

.input, input[type="text"], input[type="email"], input[type="password"], input[type="search"], input[type="tel"], select, textarea {
  display: inline-block;
  width: 100%;
  padding: 0 1.42857em;
  line-height: 3.57143em;
  min-height: calc(3.57143em + 2px);
  min-width: 9.28571em;
  border-radius: 0;
  font-size: 0.875em;
  color: inherit;
  background-color: #fff;
  border: 1px solid #e1e9f3;
  -webkit-transition: all 0.15s linear;
  transition: all 0.15s linear;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}
.input:not(select):disabled, input[type="text"]:not(select):disabled, input[type="email"]:not(select):disabled, input[type="password"]:not(select):disabled, input[type="search"]:not(select):disabled, input[type="tel"]:not(select):disabled, select:not(select):disabled, textarea:not(select):disabled {
  opacity: 0.5;
  pointer-events: none;
}

select {
  padding-top: 1.07143em;
  padding-bottom: 1.07143em;
  padding-right: 2.14286em;
  line-height: 1.42857em;
  text-transform: uppercase;
  background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI0OCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB3aWR0aD0iNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTE0IDIwbDEwIDEwIDEwLTEweiIvPjxwYXRoIGQ9Ik0wIDBoNDh2NDhoLTQ4eiIgZmlsbD0ibm9uZSIvPjwvc3ZnPg==);
  background-repeat: no-repeat;
  background-position: 97% 50%;
  background-position: calc(100% - 10px) 50%;
  background-size: 24px;
}
select option {
  color: #00153b;
  background-color: #ffffff;
}
select::-ms-expand {
  display: none;
}

.textarea,
textarea {
  padding: 0.71429em 1.42857em;
  min-height: 100px;
  resize: vertical;
  line-height: 2;
}

.checkbox {
  display: inline-block;
  margin-right: 1.875em;
}
.checkbox:not(:last-child) {
  margin-bottom: 0.625em;
}
.checkbox .checkbox__icon {
  width: 1rem;
  height: 1rem;
  display: inline-block;
  position: relative;
  vertical-align: middle;
  margin-right: 0.5em;
  border: 2px solid #d2dbe8;
  border-radius: .125rem;
  background: #fff;
  transition: border 0.15s linear, background 0.15s linear;
}
.checkbox .checkbox__label {
  display: inline;
  vertical-align: middle;
}
.checkbox input[type="checkbox"] {
  width: 1px;
  height: 1px;
  border: 0;
  clip: rect(0 0 0 0);
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
}
.checkbox input[type="checkbox"]:checked + .checkbox__icon,
.checkbox input[type="checkbox"]:checked ~ .checkbox__icon {
  border: 4px solid #00153b;
}

.radio {
  display: inline-block;
}
.radio .radio__icon {
  width: 1rem;
  height: 1rem;
  display: inline-block;
  position: relative;
  vertical-align: middle;
  margin-right: 0.5em;
  border: 2px solid #d2dbe8;
  border-radius: 1rem;
  background: #fff;
  transition: border 0.15s linear, background-color 0.15s linear;
}
.radio .radio__label {
  display: inline;
  vertical-align: middle;
}
.radio input[type="radio"] {
  width: 1px;
  height: 1px;
  border: 0;
  clip: rect(0 0 0 0);
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
}
.radio input[type="radio"]:checked + .radio__icon,
.radio input[type="radio"]:checked ~ .radio__icon {
  border: 4px solid #00153b;
}

.form-element {
  display: block;
}
.form-element .form-element__label,
.form-element .form-element__info {
  margin-right: .75rem;
}
.form-element .form-element__label:not(:last-child),
.form-element .form-element__info:not(:last-child) {
  margin-bottom: .25rem;
}
.form-element .form-element__info {
  display: block;
  font-family: "solitas_norm_book_i", sans-serif;
  font-size: 0.875em;
  line-height: 1.71429em;
  color: #d03e2a;
}
.form-element .form-element__label {
  display: inline-block;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1.25em;
  line-height: 1.5em;
}
.form-element .form-element__label .form-element__info {
  font-size: 0.8em;
}
.form-element .form-element__control {
  position: relative;
}
.form-element .form-element__add-on {
  position: absolute;
  top: 1px;
  bottom: 1px;
  padding: 0 0.71429em;
  height: 3.28571em;
  font-size: 0.875em;
  line-height: 3.28571em;
  text-align: center;
  background-color: transparent;
}
.form-element .form-element__add-on.add-on--left {
  left: 1px;
}
.form-element .form-element__add-on.add-on--right {
  right: 1px;
}
.form-element .form-element__add-on.add-on--left + input {
  padding-left: 4.375em;
}
.form-element .form-element__add-on + input {
  padding-right: 9.375em;
}
.form-element .form-element__add-on input, .form-element .form-element__add-on select {
  background-color: transparent;
  border-bottom-width: 0 !important;
  width: 100%;
  min-width: 11.42857em;
  margin: 0 -0.71429em;
}
.form-element .form-element__add-on select {
  padding-top: 1.28571em;
  padding-bottom: 1.28571em;
  line-height: 1.42857em;
}

.form:after {
  display: table;
  clear: both;
  content: " ";
}
.form.form--vertical .form-element {
  display: block;
  margin-bottom: 1.25em;
  margin-top: 1.25em;
}
.form.form--vertical .form-element .form-element__label,
.form.form--vertical .form-element .form-element__control {
  display: block;
}
.form.form--horizontal .form-element {
  display: table;
  table-layout: fixed;
  width: 100%;
  margin-bottom: 1.25em;
}
.form.form--horizontal .form-element .form-element__label,
.form.form--horizontal .form-element .form-element__control {
  display: table-cell;
  vertical-align: top;
  text-align: left;
  line-height: 2.5em;
}
.form.form--horizontal .form-element .form-element__control {
  width: 65.51724%;
}
.form.form--default .input, .form.form--default input[type="text"], .form.form--default input[type="email"], .form.form--default input[type="password"], .form.form--default input[type="search"], .form.form--default input[type="tel"], .form.form--default select, .form.form--default textarea {
  display: block;
  background-color: #fff;
  border: 1px solid #e3e9f0;
}
.form.form--default .input:not(select):focus, .form.form--default input[type="text"]:not(select):focus, .form.form--default input[type="email"]:not(select):focus, .form.form--default input[type="password"]:not(select):focus, .form.form--default input[type="search"]:not(select):focus, .form.form--default input[type="tel"]:not(select):focus, .form.form--default select:not(select):focus, .form.form--default textarea:not(select):focus {
  -webkit-box-shadow: 0 0 10px rgba(0, 21, 59, 0.1);
  box-shadow: 0 0 10px rgba(0, 21, 59, 0.1);
}
.form.form--theme .input, .form.form--theme input[type="text"], .form.form--theme input[type="email"], .form.form--theme input[type="password"], .form.form--theme input[type="search"], .form.form--theme input[type="tel"], .form.form--theme select, .form.form--theme textarea {
  display: block;
  padding: 0 1.05263em;
  line-height: 2.63158em;
  min-height: calc(2.63158em + 2px);
  min-width: 6.84211em;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1.1875em;
  background-color: transparent;
  border-width: 0;
  border-bottom: 4px solid rgba(0, 21, 59, 0.1);
}
.form.form--theme .input.error, .form.form--theme .input.mce_inline_error, .form.form--theme input[type="text"].error, .form.form--theme input[type="text"].mce_inline_error, .form.form--theme input[type="email"].error, .form.form--theme input[type="email"].mce_inline_error, .form.form--theme input[type="password"].error, .form.form--theme input[type="password"].mce_inline_error, .form.form--theme input[type="search"].error, .form.form--theme input[type="search"].mce_inline_error, .form.form--theme input[type="tel"].error, .form.form--theme input[type="tel"].mce_inline_error, .form.form--theme select.error, .form.form--theme select.mce_inline_error, .form.form--theme textarea.error, .form.form--theme textarea.mce_inline_error {
  border-bottom: 4px solid #d03e2a;
}
.form.form--theme select {
  padding-top: 0.52632em;
  padding-bottom: 0.52632em;
  line-height: 1.57895em;
  min-height: 2.63158em;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1.1875em;
  text-transform: none;
}
.form.form--theme textarea {
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1.25rem;
  text-transform: none;
}
.form.form--theme .button, .form.form--theme .img-uploader .upload-label, .img-uploader .form.form--theme .upload-label {
  height: 3.375em;
  line-height: 3.375em;
  -webkit-border-radius: 3.375em;
  border-radius: 3.375em;
}
.form.form--theme.form--inverse .input, .form.form--theme.form--inverse input[type="text"], .form.form--theme.form--inverse input[type="email"], .form.form--theme.form--inverse input[type="password"], .form.form--theme.form--inverse input[type="search"], .form.form--theme.form--inverse input[type="tel"], .form.form--theme.form--inverse select, .form.form--theme.form--inverse textarea {
  border-bottom: 4px solid rgba(255, 255, 255, 0.1);
}
.form.form--theme.form--inverse .input.error, .form.form--theme.form--inverse input[type="text"].error, .form.form--theme.form--inverse input[type="email"].error, .form.form--theme.form--inverse input[type="password"].error, .form.form--theme.form--inverse input[type="search"].error, .form.form--theme.form--inverse input[type="tel"].error, .form.form--theme.form--inverse select.error, .form.form--theme.form--inverse textarea.error {
  border-bottom: 4px solid #d03e2a;
}
.form.form--theme.form--inverse select {
  background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI0OCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB3aWR0aD0iNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTE0IDIwbDEwIDEwIDEwLTEweiIgZmlsbD0iI2ZmZmZmZiIgLz48cGF0aCBkPSJNMCAwaDQ4djQ4aC00OHoiIGZpbGw9Im5vbmUiLz48L3N2Zz4=);
}
.form.form--theme.form--vertical .form-element__label {
  position: absolute;
  top: 14px;
  left: 0;
  pointer-events: none;
  opacity: 1;
  -webkit-transition: all 0.15s linear;
  transition: all 0.15s linear;
}
.form.form--theme.form--vertical .input, .form.form--theme.form--vertical input[type="text"], .form.form--theme.form--vertical input[type="email"], .form.form--theme.form--vertical input[type="password"], .form.form--theme.form--vertical input[type="search"], .form.form--theme.form--vertical input[type="tel"], .form.form--theme.form--vertical select, .form.form--theme.form--vertical textarea {
  padding-left: 0;
  padding-right: 0;
}
.form.form--theme.form--vertical .input:focus ~ label, .form.form--theme.form--vertical .input:valid ~ label, .form.form--theme.form--vertical .input:invalid ~ label, .form.form--theme.form--vertical .input:focus + label, .form.form--theme.form--vertical .input:valid + label, .form.form--theme.form--vertical .input:invalid + label, .form.form--theme.form--vertical input[type="text"]:focus ~ label, .form.form--theme.form--vertical input[type="text"]:valid ~ label, .form.form--theme.form--vertical input[type="text"]:invalid ~ label, .form.form--theme.form--vertical input[type="text"]:focus + label, .form.form--theme.form--vertical input[type="text"]:valid + label, .form.form--theme.form--vertical input[type="text"]:invalid + label, .form.form--theme.form--vertical input[type="email"]:focus ~ label, .form.form--theme.form--vertical input[type="email"]:valid ~ label, .form.form--theme.form--vertical input[type="email"]:invalid ~ label, .form.form--theme.form--vertical input[type="email"]:focus + label, .form.form--theme.form--vertical input[type="email"]:valid + label, .form.form--theme.form--vertical input[type="email"]:invalid + label, .form.form--theme.form--vertical input[type="password"]:focus ~ label, .form.form--theme.form--vertical input[type="password"]:valid ~ label, .form.form--theme.form--vertical input[type="password"]:invalid ~ label, .form.form--theme.form--vertical input[type="password"]:focus + label, .form.form--theme.form--vertical input[type="password"]:valid + label, .form.form--theme.form--vertical input[type="password"]:invalid + label, .form.form--theme.form--vertical input[type="search"]:focus ~ label, .form.form--theme.form--vertical input[type="search"]:valid ~ label, .form.form--theme.form--vertical input[type="search"]:invalid ~ label, .form.form--theme.form--vertical input[type="search"]:focus + label, .form.form--theme.form--vertical input[type="search"]:valid + label, .form.form--theme.form--vertical input[type="search"]:invalid + label, .form.form--theme.form--vertical input[type="tel"]:focus ~ label, .form.form--theme.form--vertical input[type="tel"]:valid ~ label, .form.form--theme.form--vertical input[type="tel"]:invalid ~ label, .form.form--theme.form--vertical input[type="tel"]:focus + label, .form.form--theme.form--vertical input[type="tel"]:valid + label, .form.form--theme.form--vertical input[type="tel"]:invalid + label, .form.form--theme.form--vertical select:focus ~ label, .form.form--theme.form--vertical select:valid ~ label, .form.form--theme.form--vertical select:invalid ~ label, .form.form--theme.form--vertical select:focus + label, .form.form--theme.form--vertical select:valid + label, .form.form--theme.form--vertical select:invalid + label, .form.form--theme.form--vertical textarea:focus ~ label, .form.form--theme.form--vertical textarea:valid ~ label, .form.form--theme.form--vertical textarea:invalid ~ label, .form.form--theme.form--vertical textarea:focus + label, .form.form--theme.form--vertical textarea:valid + label, .form.form--theme.form--vertical textarea:invalid + label {
  top: -1.42857em;
  opacity: 0.3;
  font-size: 0.875em;
}
.form.form--theme.form--vertical .input.empty + label, .form.form--theme.form--vertical input[type="text"].empty + label, .form.form--theme.form--vertical input[type="email"].empty + label, .form.form--theme.form--vertical input[type="password"].empty + label, .form.form--theme.form--vertical input[type="search"].empty + label, .form.form--theme.form--vertical input[type="tel"].empty + label, .form.form--theme.form--vertical select.empty + label, .form.form--theme.form--vertical textarea.empty + label {
  top: 14px;
  opacity: 1;
  font-size: 1.25em;
}
.form.form--theme.form--vertical .input.empty:focus ~ label, .form.form--theme.form--vertical .input.empty:focus + label, .form.form--theme.form--vertical input[type="text"].empty:focus ~ label, .form.form--theme.form--vertical input[type="text"].empty:focus + label, .form.form--theme.form--vertical input[type="email"].empty:focus ~ label, .form.form--theme.form--vertical input[type="email"].empty:focus + label, .form.form--theme.form--vertical input[type="password"].empty:focus ~ label, .form.form--theme.form--vertical input[type="password"].empty:focus + label, .form.form--theme.form--vertical input[type="search"].empty:focus ~ label, .form.form--theme.form--vertical input[type="search"].empty:focus + label, .form.form--theme.form--vertical input[type="tel"].empty:focus ~ label, .form.form--theme.form--vertical input[type="tel"].empty:focus + label, .form.form--theme.form--vertical select.empty:focus ~ label, .form.form--theme.form--vertical select.empty:focus + label, .form.form--theme.form--vertical textarea.empty:focus ~ label, .form.form--theme.form--vertical textarea.empty:focus + label {
  top: -1.42857em;
  opacity: 0.3;
  font-size: 0.875em;
}

@media screen and (max-width: 47.9375em) {
  .form.form--horizontal .form-element {
    display: block;
  }
  .form.form--horizontal .form-element .form-element__label,
  .form.form--horizontal .form-element .form-element__control {
    display: block;
    width: 100%;
  }
}
.gallery {
  position: relative;
  display: block;
  margin-top: -1.69492%;
}
.gallery:after {
  display: table;
  clear: both;
  content: " ";
}
.gallery .gallery__item {
  display: inline-block;
  width: 100%;
  margin-top: 1.69492%;
  -webkit-box-shadow: 0 0 0 0 #e1e9f3;
  box-shadow: 0 0 0 0 #e1e9f3;
  transition: -webkit-box-shadow 0.3s linear;
  -webkit-transition: box-shadow 0.3s linear;
  transition: box-shadow 0.3s linear;
}
.gallery .gallery__item:hover, .gallery .gallery__item:focus {
  -webkit-box-shadow: 0 0 0 3px #e1e9f3;
  box-shadow: 0 0 0 3px #e1e9f3;
}
.gallery .gallery__item img {
  display: inline-block;
  vertical-align: middle;
  max-width: 100%;
}
@media screen and (min-width: 48em) {
  .gallery.gallery--1 {
    margin-top: -1.69492%;
  }
  .gallery.gallery--1 .gallery__item {
    display: block;
    width: 100%;
    margin-top: 1.69492%;
  }
  .gallery.gallery--2 {
    margin-top: -1.69492%;
  }
  .gallery.gallery--2 .gallery__item {
    width: 48.27586%;
    float: left;
    margin-right: 3.44828%;
    margin-top: 1.69492%;
  }
  .gallery.gallery--2 .gallery__item:nth-child(2n) {
    margin-right: 0;
  }
  .gallery.gallery--2 .gallery__item:nth-child(2n+1) {
    clear: left;
  }
  .gallery.gallery--3 {
    margin-top: -1.69492%;
  }
  .gallery.gallery--3 .gallery__item {
    width: 31.81818%;
    float: left;
    margin-right: 2.27273%;
    margin-top: 1.69492%;
  }
  .gallery.gallery--3 .gallery__item:nth-child(3n) {
    margin-right: 0;
  }
  .gallery.gallery--3 .gallery__item:nth-child(3n+1) {
    clear: left;
  }
  .gallery.gallery--4 {
    margin-top: -1.69492%;
  }
  .gallery.gallery--4 .gallery__item {
    width: 49.15254%;
    float: left;
    margin-right: 1.69492%;
    margin-top: 1.69492%;
  }
  .gallery.gallery--4 .gallery__item:nth-child(2n) {
    margin-right: 0;
  }
  .gallery.gallery--4 .gallery__item:nth-child(2n+1) {
    clear: left;
  }
}
@media screen and (min-width: 64em) {
  .gallery.gallery--4 {
    margin-top: -1.69492%;
  }
  .gallery.gallery--4 .gallery__item {
    width: 23.72881%;
    float: left;
    margin-right: 1.69492%;
    margin-top: 1.69492%;
  }
  .gallery.gallery--4 .gallery__item:nth-child(2n) {
    margin-right: 1.69492%;
  }
  .gallery.gallery--4 .gallery__item:nth-child(2n+1) {
    clear: none;
  }
  .gallery.gallery--4 .gallery__item:nth-child(4n) {
    margin-right: 0;
  }
  .gallery.gallery--4 .gallery__item:nth-child(4n+1) {
    clear: left;
  }
}

.container {
  width: 100%;
  padding-left: 0.625rem;
  padding-right: 0.625rem;
}
.container:after {
  display: table;
  clear: both;
  content: " ";
}
.container.container--static {
  max-width: 75rem;
  margin-left: auto;
  margin-right: auto;
}
.container.container--static:after {
  content: " ";
  display: block;
  clear: both;
}
.container.container--fluid {
  padding-left: 0;
  padding-right: 0;
}
@media screen and (min-width: 48em) {
  .container.container--static {
    max-width: 45rem;
  }
}
@media screen and (min-width: 64em) {
  .container.container--static {
    max-width: 60rem;
  }
}
@media screen and (min-width: 80em) {
  .container.container--static {
    max-width: 75rem;
  }
}

.span__row {
  margin-left: -0.625rem;
  margin-right: -0.625rem;
}
.span__row:after {
  display: table;
  clear: both;
  content: " ";
}

.span {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  width: 100%;
  float: left;
  padding-left: 0.625rem;
  padding-right: 0.625rem;
}

@media screen and (min-width: 48em) {
  .span--1 {
    width: 8.33333%;
  }

  .span--2 {
    width: 16.66667%;
  }

  .span--3 {
    width: 25%;
  }

  .span--4 {
    width: 33.33333%;
  }

  .span--5 {
    width: 41.66667%;
  }

  .span--6 {
    width: 50%;
  }

  .span--7 {
    width: 58.33333%;
  }

  .span--8 {
    width: 66.66667%;
  }

  .span--9 {
    width: 75%;
  }

  .span--10 {
    width: 83.33333%;
  }

  .span--11 {
    width: 91.66667%;
  }

  .span--12 {
    width: 100%;
  }
}
@media screen and (min-width: 80em) {
  .span-offset--1 {
    padding-left: 8.33333%;
  }

  .span-offset--2 {
    padding-left: 16.66667%;
  }

  .span-offset--3 {
    padding-left: 25%;
  }

  .span-offset--4 {
    padding-left: 33.33333%;
  }

  .span-offset--5 {
    padding-left: 41.66667%;
  }

  .span-offset--6 {
    padding-left: 50%;
  }

  .span-offset--7 {
    padding-left: 58.33333%;
  }

  .span-offset--8 {
    padding-left: 66.66667%;
  }

  .span-offset--9 {
    padding-left: 75%;
  }

  .span-offset--10 {
    padding-left: 83.33333%;
  }

  .span-offset--11 {
    padding-left: 91.66667%;
  }

  .span-offset--12 {
    padding-left: 100%;
  }
}
@media screen and (max-width: 47.9375em) {
  .span__row {
    margin-top: -0.625rem;
  }

  .span {
    margin-top: 0.625rem;
  }
}
.span--center {
  float: none !important;
  margin-left: auto !important;
  margin-right: auto !important;
}

.span--last {
  float: right;
}

.icon {
  display: inline-block;
  vertical-align: middle;
  height: 1em;
  width: 1em;
  fill: currentColor;
}
.icon.icon--logo {
  width: 100%;
  height: 100%;
}
.icon.icon--list, .icon.icon--grid {
  width: 1.75em;
  height: 1.75em;
}
.icon.icon--no-record {
  width: 6.25em;
  height: 7.5em;
}

/* Image */
.image {
  position: relative;
  display: inline-block;
  border: none;
  overflow: hidden;
}
.image img {
  display: block;
}
.image.image--left, .image.image--right {
  max-width: 100%;
}
.image.image--left img, .image.image--right img {
  max-width: 100%;
}
.image.image--left {
  float: left;
  padding: 0 1.5em 1em 0;
  top: 0.25em;
}
.image.image--right {
  float: right;
  padding: 0 0 1em 1.5em;
  top: 0.25em;
}
.image.image--fit {
  display: block;
  width: 100%;
}
.image.image--fit img {
  width: 100%;
}

.link {
  -moz-appearance: none;
  -webkit-appearance: none;
  appearance: none;
  cursor: pointer;
  display: inline-block;
  vertical-align: baseline;
  text-decoration: underline;
}
.link.link--fit {
  display: block;
}
.link .link__arrow {
  position: relative;
  display: inline-block;
  vertical-align: middle;
  width: 20px;
  height: 8px;
}
.link .link__arrow:before {
  position: absolute;
  top: 50%;
  right: 0;
  margin-top: -1px;
  display: inline-block;
  width: 20px;
  height: 2px;
  content: " ";
  background-color: currentColor;
}
.link .link__arrow:after {
  position: absolute;
  top: 50%;
  right: 0;
  margin-top: -4px;
  display: inline-block;
  width: 0;
  height: 0;
  content: " ";
  border: 4px solid transparent;
  border-left-color: currentColor;
  border-right-width: 0;
}
.link.link--right {
  padding-right: 30px;
}
.link.link--right .link__arrow {
  -webkit-transform: rotate(0deg);
  transform: rotate(0deg);
}
.link.link--top {
  padding-right: 15px;
}
.link.link--top .link__arrow {
  -webkit-transform: rotate(-90deg);
  transform: rotate(-90deg);
  width: 12px;
}
.link.link--top .link__arrow:before {
  width: 12px;
}
.link.link--bottom {
  padding-right: 15px;
}
.link.link--bottom .link__arrow {
  -webkit-transform: rotate(90deg);
  transform: rotate(90deg);
  width: 12px;
}
.link.link--bottom .link__arrow:before {
  width: 12px;
}

.media, .media .media__body {
  overflow: hidden;
  zoom: 1;
}
.media .media__figure,
.media .media__body {
  display: table-cell;
  vertical-align: top;
}
.media .media__figure img,
.media .media__body img {
  width: 100%;
}
.media .media__figure.media--middle {
  vertical-align: middle;
}
.media .media__figure.media--bottom {
  vertical-align: bottom;
}
@media screen and (min-width: 48em) {
  .media .media__figure.media--left {
    float: left;
    padding-right: 1.25em;
  }
  .media .media__figure.media--right {
    float: right;
    padding-left: 1.25em;
  }
}
.media .media__body {
  width: 75em;
  padding-left: 1.25em;
}
.media .media__body.media--middle {
  vertical-align: middle;
}
.media .media__body.media--bottom {
  vertical-align: bottom;
}
.media .media--left + .media__body,
.media .media--right + .media__body {
  padding-left: 0;
}
@media screen and (max-width: 47.9375em) {
  .media,
  .media .media__figure,
  .media .media__body {
    display: block;
    width: auto;
    text-align: center;
  }
  .media .media__body {
    padding-left: 0;
    padding-right: 0;
    padding-top: 1.25em;
  }
}

.menu {
  position: relative;
  display: block;
}
.menu a:not(.button) {
  position: relative;
  display: block;
  padding: 0.3125em 1.25em;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1em;
  line-height: 1.25em;
  white-space: nowrap;
}
.menu a:not(.button).active {
  font-family: "solitas_norm_medium", sans-serif;
}
.menu a.button, .menu .img-uploader a.upload-label, .img-uploader .menu a.upload-label {
  margin-left: 5px;
  margin-right: 5px;
}
.menu ul {
  list-style: none;
  padding: 0;
  margin: 0;
}
.menu.menu--no a:not(.button) {
  padding-top: 0;
  padding-bottom: 0;
}
.menu.menu--large a:not(.button) {
  padding-top: 0.78125em;
  padding-bottom: 0.78125em;
}
.menu.menu--bordered {
  background-color: #ffffff;
  border: 1px solid #e1e9f3;
  border-top-width: 0;
}
.menu.menu--bordered a:not(.button) {
  border-top: 1px solid #e1e9f3;
}
.menu.menu--bordered a:not(.button).active {
  font-family: "solitas_norm_medium", sans-serif;
  background-color: #e1e9f3;
}
@media screen and (max-width: 79.9375em) {
  .menu a:not(.button) {
    padding: 0.3125em 0.625em;
  }
}
@media screen and (max-width: 47.9375em) {
  .menu a:not(.button) {
    padding: 0.3125em;
  }
}

.menu-bar {
  position: relative;
  display: block;
  width: 100%;
  z-index: 2000;
  padding: 0 1.25rem;
  font-size: 0.875em;
  background-color: #e8eef6;
  -webkit-transition: all 0.15s linear;
  transition: all 0.15s linear;
}
.menu-bar:after {
  display: table;
  clear: both;
  content: " ";
}
.menu-bar nav {
  display: inline-block;
  vertical-align: middle;
}
.menu-bar .menu > ul > li > a:not(.button) {
  height: 4.375rem;
  line-height: 4.375rem;
  padding-top: 0;
  padding-bottom: 0;
  -webkit-transition: all 0.15s linear;
  transition: all 0.15s linear;
}
@media screen and (min-width: 48em) {
  .menu-bar {
    padding-top: 1.875rem;
  }
  .menu-bar > * {
    height: 4.375rem;
    line-height: 4.375rem;
  }
  .menu-bar .menu > ul > li > a:not(.button) {
    height: 6.25rem;
    line-height: 4.375rem;
    padding-top: 1.875rem;
    margin-top: -1.875rem;
  }
  .menu-bar .menu > ul > li > a:not(.button):active, .menu-bar .menu > ul > li > a:not(.button).active {
    background-color: rgba(0, 0, 0, 0.05);
    mix-blend-mode: multiply;
  }
  .menu-bar.is--stuck {
    padding-top: 0;
  }
  .menu-bar.is--stuck .menu > ul > li > a:not(.button) {
    height: 4.375rem;
    padding-top: 0;
    margin-top: 0;
  }
}
@media screen and (max-width: 63.9375em) {
  .menu-bar {
    white-space: nowrap;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
  }
}
@media screen and (max-width: 47.9375em) {
  .menu-bar nav + nav:last-child {
    margin-left: 1.25em;
    padding-left: 1.25em;
    background-color: #dde6f1;
  }
}

.modal .modal__header, .modal .modal__content, .modal .modal__footer {
  position: relative;
  padding: 1.25em;
}
.modal .modal__header {
  border-bottom: 1px solid #e1e9f3;
  text-align: center;
}
.modal .modal__footer {
  border-top: 1px solid #e1e9f3;
}
.modal .modal__heading {
  font-size: 1.4em;
  line-height: 1.71429em;
  letter-spacing: 0.02em;
  margin: 0;
  padding: 0;
}

.pagination {
  padding: 1.25em;
}
.pagination:after {
  display: table;
  clear: both;
  content: " ";
}
.pagination a:not(.button) {
  cursor: pointer;
  display: block;
  width: 2.8125em;
  height: 2.8125em;
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 1em;
  font-weight: normal;
  line-height: 2.8125em;
  -webkit-border-radius: 2.8125em;
  border-radius: 2.8125em;
  text-align: center;
}
.pagination a:not(.button):hover, .pagination a:not(.button).active {
  background-color: #e1e9f3;
}
.pagination .selected a:not(.button) {
  background-color: #e1e9f3;
}

@media screen and (max-width: 47.9375em) {
  .pagination .fl--left,
  .pagination .fl--right {
    float: none;
  }
}
.section {
  position: relative;
  display: block;
  padding: 2.5em 0;
}
.section hr {
  margin: 1.6625em 0;
}
.section .section__header {
  position: relative;
  display: block;
  margin-bottom: 2.5em;
}
.section .section__header:after {
  display: table;
  clear: both;
  content: " ";
}
.section .section__header.section-header--bordered {
  margin-bottom: 1.875em;
  border-bottom: 1px solid #d6e0ee;
}
.section .header__heading-text,
.section .section__heading-text {
  height: 2.32143em;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.4em;
  line-height: 2.32143em;
  letter-spacing: 0.02em;
  margin: 0;
  padding: 0;
}
.section.section--lightest {
  background-color: #ffffff;
  color: #00153b;
}
.section.section--light {
  background-color: #f3f6fa;
  color: #00153b;
}
.section.section--dark {
  background-color: #00153b;
  color: #ffffff;
}
.section.section--top-border:before {
  position: relative;
  top: -2.5em;
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 100%;
  height: 1px;
  max-width: 73.75rem;
  background-color: #e1e9f3;
  content: " ";
}
.section.section--vcenter {
  display: block;
  padding: 2.5em 0;
}
@media screen and (min-width: 64em) {
  .section {
    padding: 5em 0;
  }
  .section.section--top-border:before {
    top: -5em;
  }
}
@media screen and (min-width: 48em) {
  .section.section--vcenter {
    display: table;
    width: 100%;
    height: 100%;
  }
  .section.section--vcenter .section__body {
    display: table-cell;
    vertical-align: middle;
    height: 100%;
    width: 100%;
  }
  .section.section--vcenter .section__footer {
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
  }
}

.block hr {
  margin: 0.83125em 0;
}
.block .block__heading-text {
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.5em;
  line-height: 1.6em;
  letter-spacing: 0.02em;
  text-transform: uppercase;
  margin: 0;
  padding: 0;
}

.table {
  width: 100%;
  background-color: #fff;
}
.table thead, .table tbody, .table tfoot {
  width: 100%;
}
.table td, .table th {
  position: relative;
}
.table thead td, .table thead th, .table tfoot td, .table tfoot th {
  padding: 0.625em 1.25em;
  font-size: 1.1em;
  font-weight: normal;
  text-transform: uppercase;
}
.table tbody td, .table tbody th {
  padding: 1.25em;
}
.table .table {
  font-size: 0.9em;
}
.table .table thead td, .table .table thead th, .table .table tbody td, .table .table tbody th, .table .table tfoot td, .table .table tfoot th {
  padding: 0.3125em 1.25em;
}
.table.table--no-padding td, .table.table--no-padding th {
  padding: 0;
}
.table.table--fixed {
  table-layout: fixed;
}
.table.table--bordered {
  border: 1px solid #e1e9f3;
}
.table.table--bordered tr:not(:first-child) td, .table.table--bordered tr:not(:first-child) th {
  border-top: 1px solid #e1e9f3;
}
.table.table--striped tr:not(:nth-child(2n+1)) td, .table.table--striped tr:not(:nth-child(2n+1)) th {
  background-color: #e1e9f3;
}
@media screen and (max-width: 47.9375em) {
  .table.table--responsive {
    display: block;
  }
  .table.table--responsive thead, .table.table--responsive tbody, .table.table--responsive tfoot {
    display: block;
  }
  .table.table--responsive thead {
    display: none;
  }
  .table.table--responsive tbody, .table.table--responsive tr, .table.table--responsive td, .table.table--responsive th {
    display: block;
  }
  .table.table--responsive td:before, .table.table--responsive th:before {
    display: block;
    content: attr(data-label);
    text-transform: uppercase;
  }
  .table.table--responsive td:not(:last-child), .table.table--responsive th:not(:last-child) {
    padding-bottom: 0;
  }
  .table.table--responsive.table--bordered tr:not(:first-child) td, .table.table--responsive.table--bordered tr:not(:first-child) th {
    border-width: 0;
  }
  .table.table--responsive.table--bordered tr:not(:first-child) td:first-child, .table.table--responsive.table--bordered tr:not(:first-child) th:first-child {
    border-top: 1px solid #f3f6fa;
  }
}

.tag {
  display: inline-block;
  vertical-align: middle;
  padding: 3px 5px;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  font-size: 0.875em;
  line-height: 1;
  background-color: #f3f6fa;
  border: 1px solid #e8eef6;
}
.tag .tag__label {
  max-width: 18.75em;
  display: inline-block;
  vertical-align: middle;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.tag .tag__remove {
  margin-left: 0.625em;
}

.site-header {
  position: relative;
  z-index: 3000;
  display: block;
  padding: 0.625rem;
  background-color: #ffffff;
  color: #00153b;
  -webkit-box-shadow: 0 0 0 0 rgba(0, 21, 59, 0.1);
  box-shadow: 0 0 0 0 rgba(0, 21, 59, 0.1);
  -webkit-transition: box-shadow 0.21s linear, height 0.21s linear;
  transition: box-shadow 0.21s linear, height 0.21s linear;
}
.is--bottom .site-header {
  -webkit-box-shadow: 0 1px 7px 0 rgba(0, 21, 59, 0.1);
  box-shadow: 0 1px 7px 0 rgba(0, 21, 59, 0.1);
}
.site-header.site-header--inverse {
  background-color: #00153b;
  color: #ffffff;
}
.site-header.site-header--inverse .h__brand .h__trigger {
  background-color: #001e55;
  color: #f3f6fa;
}
.site-header .h__logo {
  display: inline-block;
  margin-right: 1.25em;
}
.site-header .h__logo img {
  max-height: 100%;
}
.site-header .h__search {
  display: inline-block;
  vertical-align: middle;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1.125em;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  white-space: nowrap;
  margin-right: 0.5em;
}
.site-header .h__search .search__icon,
.site-header .h__search .search__text {
  display: inline-block;
  vertical-align: middle;
}
.site-header .h__search .search__icon:not(:last-child),
.site-header .h__search .search__text:not(:last-child) {
  margin-right: 0.625em;
}
.site-header .h__brand .h__logo {
  height: 5.3125em;
  -webkit-transition: height 0.21s linear, line-height 0.21s linear, margin 0.21s linear;
  transition: height 0.21s linear, line-height 0.21s linear, margin 0.21s linear;
}
.site-header .h__navigation {
  font-family: "solitas_norm_medium", sans-serif;
  text-transform: uppercase;
  -webkit-transition: height 0.21s linear, line-height 0.21s linear;
  transition: height 0.21s linear, line-height 0.21s linear;
}
.site-header .h__navigation .menu {
  display: inline-block;
}
.site-header .h__brand-main {
  -webkit-transition: transform 0.21s linear;
  transition: -webkit-transform 0.21s linear;
  transition: transform 0.21s linear;
}
.site-header .h__brand-main .h__logo {
  height: 10.625em;
  margin: 1.25em;
}
.site-header .h__cart {
  display: inline-block;
  padding-left: 1.25em;
  padding-right: 1.25em;
  margin-top: -0.625em;
  margin-right: -0.625em;
  margin-left: 0.625em;
  height: 6.5625em;
  line-height: 6.5625em;
  background-color: #f3f6fa;
  color: #00153b;
}
@media screen and (min-width: 48em) {
  .site-header.site-header--fixed {
    position: fixed;
    top: 2em;
    left: 0;
    right: 0;
  }
  .site-header.site-header--float {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
  }
  .site-header .h__brand,
  .site-header .h__navigation {
    position: relative;
    z-index: 2000;
    height: 5.3125em;
    line-height: 5.3125em;
  }
}
@media screen and (min-width: 80em) {
  .site-header .h__brand-main {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    z-index: 1000;
  }
  .site-header.site-header--transparent {
    background-color: transparent;
    color: #ffffff;
    -webkit-transition: all 0.21s linear;
    transition: all 0.21s linear;
  }
}
@media screen and (max-width: 63.9375em) {
  .site-header .h__search .search__text span {
    display: none;
  }
}
@media screen and (max-width: 47.9375em) {
  .site-header .h__brand .h__logo {
    height: 3.125em;
    margin-right: 0.625em;
  }
  .site-header .h__brand .h__trigger {
    float: right;
  }
}

.site-header-nofication {
  position: relative;
  z-index: 3001;
  padding: 0 1.25em;
  background-color: #efc3bd;
  color: #d03e2a;
  -webkit-transition: opacity 0.3s linear;
  transition: opacity 0.3s linear;
  -webkit-transition-delay: 0.3s;
  transition-delay: 0.3s;
}
@media screen and (min-width: 48em) {
  .site-header-nofication {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
  }
}

.site-main {
  position: relative;
  display: block;
  color: #00153b;
}
.site-main.site-main--dark {
  background-color: #f3f6fa;
}
.site-main.site-main--darkest {
  background-color: #00153b;
  color: #ffffff;
}
.site-main.site-main--primary {
  background-color: #67c9d3;
  color: #ffffff;
}
.site-main__header {
  position: relative;
  display: block;
}
.site-main__header.site-main__header--light {
  background-color: #ffffff;
  color: #00153b;
}
.site-main__header.site-main__header--dark {
  background-color: #00153b;
  color: #ffffff;
}
@media screen and (min-width: 48em) {
  .site-main__header__image {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;
  }
  .site-main__header__image:after {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    content: " ";
    background-color: rgba(0, 21, 59, 0.15);
  }
  .site-main__header__image .img {
    position: relative;
    display: block;
    width: 100%;
    max-height: 768px;
    max-height: 100vh;
    overflow: hidden;
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
  }
  .site-main__header__image .img:before {
    content: " ";
    display: block;
    width: 100%;
    padding-bottom: 200%;
  }
  .site-main__header__image .img img {
    position: absolute;
    top: -9999px;
    left: -9999px;
    right: -9999px;
    bottom: -9999px;
    margin: auto;
    min-width: 100%;
    min-height: 100%;
    max-width: 1000%;
    max-height: 1000%;
  }
}
@media screen and (min-width: 48em) {
  .site-main {
    padding-top: 8.5625em;
  }
}
@media screen and (min-width: 85em) {
  .site-main.with--sidebar {
    margin-right: 18.75em;
  }
}
@media screen and (min-width: 101.25em) {
  .site-main.with--sidebar {
    margin-right: 25em;
  }
}

.site-fixed-sidebar {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  z-index: 2000;
  display: none;
  width: 100%;
  background-color: #00153b;
  color: #ffffff;
}
.site-fixed-sidebar .sidebar__stats {
  display: table;
  table-layout: fixed;
  height: 7.5em;
  margin-top: 5em;
  border-bottom: 1px solid rgba(255, 255, 255, 0.1);
}
.site-fixed-sidebar .sidebar__stats .stat {
  display: table-cell;
  width: 50%;
  height: 100%;
  text-align: center;
}
.site-fixed-sidebar .sidebar__stats .stat .stat__big {
  display: inline-block;
  width: 100%;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 2em;
}
.site-fixed-sidebar .sidebar__stats .stat .stat__small {
  display: inline-block;
  width: 100%;
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 1em;
  letter-spacing: 0.3125em;
  text-transform: uppercase;
}
@media screen and (min-width: 85em) {
  .site-fixed-sidebar {
    display: block;
    max-width: 18.75em;
    padding: 11.0625em 1.25em 2.5em;
  }
}
@media screen and (min-width: 101.25em) {
  .site-fixed-sidebar {
    max-width: 25em;
    padding: 11.0625em 2.5em 2.5em;
  }
}

.site-footer {
  position: relative;
  display: block;
  padding: 5em 0;
  background-color: #ffffff;
}
.site-footer .site-footer__upper {
  padding-bottom: 2.5em;
  margin-bottom: 2.5em;
}
.site-footer .f__logo {
  display: inline-block;
  height: 8.125em;
}
.site-footer .f__logo img {
  max-height: 100%;
}
.site-footer .f__block:not(:last-child) {
  margin-bottom: 1.25em;
}
.site-footer .f__block .f__block__heading {
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.125em;
  color: #67c9d3;
  text-transform: uppercase;
}
.site-footer .f__block a {
  display: inline-block;
  line-height: 1.2;
}
.site-footer .f__block ul li {
  line-height: 1.8;
}
.site-footer .f__certified {
  text-align: left;
}
.site-footer .f__payment {
  text-align: right;
}
.site-footer .f__social-menu {
  padding: 1.25em 0;
}
.site-footer .f__social-menu a {
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 0.9em;
  line-height: 1;
  letter-spacing: 0.3125em;
  text-transform: uppercase;
  white-space: nowrap;
  padding: 0;
  margin: 0;
}
.site-footer .f__social-menu .icon {
  margin-right: 0.625em;
}
.site-footer.site-footer--dark {
  background-color: #00153b;
  color: #ffffff;
}
.site-footer.site-footer--dark .site-footer__upper {
  border-bottom: 1px solid rgba(255, 255, 255, 0.1);
}
.site-footer.site-footer--light {
  background-color: #ffffff;
  color: #00153b;
}
.site-footer.site-footer--light .site-footer__upper {
  border-bottom: 1px solid rgba(0, 21, 59, 0.05);
}
.site-footer.site-footer--bg {
  background-color: #00153b;
  background-position: center center;
  background-size: cover;
  color: #ffffff;
}
.site-footer.site-footer--bg:after {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1000;
  content: " ";
  display: block;
  background-color: rgba(0, 21, 59, 0.6);
}
.site-footer.site-footer--bg > * {
  position: relative;
  z-index: 2000;
}
.site-footer.site-footer--bg .site-footer__upper {
  border-bottom: 1px solid rgba(255, 255, 255, 0.1);
}
.site-footer.site-footer--dark .input, .site-footer.site-footer--dark input[type="text"], .site-footer.site-footer--dark input[type="email"], .site-footer.site-footer--dark input[type="password"], .site-footer.site-footer--dark input[type="search"], .site-footer.site-footer--dark input[type="tel"], .site-footer.site-footer--dark select, .site-footer.site-footer--dark textarea, .site-footer.site-footer--bg .input, .site-footer.site-footer--bg input[type="text"], .site-footer.site-footer--bg input[type="email"], .site-footer.site-footer--bg input[type="password"], .site-footer.site-footer--bg input[type="search"], .site-footer.site-footer--bg input[type="tel"], .site-footer.site-footer--bg select, .site-footer.site-footer--bg textarea {
  border-bottom: 4px solid rgba(255, 255, 255, 0.1);
}
.site-footer.site-footer--dark .input.error, .site-footer.site-footer--dark input[type="text"].error, .site-footer.site-footer--dark input[type="email"].error, .site-footer.site-footer--dark input[type="password"].error, .site-footer.site-footer--dark input[type="search"].error, .site-footer.site-footer--dark input[type="tel"].error, .site-footer.site-footer--dark select.error, .site-footer.site-footer--dark textarea.error, .site-footer.site-footer--bg .input.error, .site-footer.site-footer--bg input[type="text"].error, .site-footer.site-footer--bg input[type="email"].error, .site-footer.site-footer--bg input[type="password"].error, .site-footer.site-footer--bg input[type="search"].error, .site-footer.site-footer--bg input[type="tel"].error, .site-footer.site-footer--bg select.error, .site-footer.site-footer--bg textarea.error {
  border-bottom: 4px solid #d03e2a;
}
.site-footer.site-footer--dark select, .site-footer.site-footer--bg select {
  background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI0OCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB3aWR0aD0iNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTE0IDIwbDEwIDEwIDEwLTEweiIgZmlsbD0iI2ZmZmZmZiIgLz48cGF0aCBkPSJNMCAwaDQ4djQ4aC00OHoiIGZpbGw9Im5vbmUiLz48L3N2Zz4=);
}
@media screen and (min-width: 85em) {
  .site-footer.with--sidebar {
    margin-right: 18.75em;
  }
}
@media screen and (min-width: 101.25em) {
  .site-footer.with--sidebar {
    margin-right: 25em;
  }
}
@media screen and (max-width: 63.9375em) {
  .site-footer .span.span--10 {
    width: 100%;
  }
  .site-footer .f__social-menu ul.list.list--fit {
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
  }
  .site-footer .f__social-menu .icon {
    margin: 0 0.3125em;
  }
  .site-footer .f__social-menu .hidden-on--mobile {
    display: none;
  }
}
@media screen and (max-width: 47.9375em) {
  .site-footer {
    text-align: center;
    padding: 2.5em 0;
  }
  .site-footer .f__certified,
  .site-footer .f__payment {
    text-align: center;
  }
  .site-footer .site-footer__upper {
    padding-bottom: 1.25em;
  }
}

.main-menu a:not(.button) {
  font-family: "solitas_norm_medium", sans-serif;
}
.main-menu a:not(.button).avatar {
  position: static;
  display: block;
  overflow: hidden;
  padding: 0;
  margin: 0;
  width: 4.72222em;
  height: 4.72222em;
  -webkit-border-radius: 4.72222em;
  border-radius: 4.72222em;
  border: 2px solid rgba(128, 128, 128, 0.2);
}
.main-menu a:not(.button).notify {
  position: relative;
}
.main-menu a:not(.button).notify:before {
  display: inline-block;
  vertical-align: middle;
  padding-left: 5px;
  padding-right: 5px;
  margin-right: 5px;
  content: attr(data-counter);
  -webkit-border-radius: 20px;
  border-radius: 20px;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 11px;
  line-height: 15px;
  text-align: center;
  background-color: #d03e2a;
  color: #ffffff;
}
.main-menu .sub-menu {
  position: relative;
}
.main-menu .sub-menu > a:after {
  display: inline-block;
  vertical-align: middle;
  content: " ";
  height: 0;
  width: 0;
  margin-left: 0.625em;
  border: solid transparent;
  border-width: 5px;
  border-top-color: currentColor;
  pointer-events: none;
}
.main-menu .sub-menu ul {
  position: absolute;
  z-index: 2000;
  top: -0.5em;
  display: none;
  width: auto;
  min-width: 10em;
  max-width: 20em;
  padding: 2.9375em 0 0.5em;
  line-height: 2;
  background: #ffffff;
  border: 1px solid #e1e9f3;
  -webkit-border-radius: 0.625em;
  border-radius: 0.625em;
  box-shadow: 0 5px 5px 0 rgba(0, 0, 0, 0.08);
}
.main-menu .sub-menu ul a {
  font-size: 0.9375em;
  border-top: 1px solid #e1e9f3;
}
.main-menu .sub-menu ul a:hover, .main-menu .sub-menu ul a:focus {
  background-color: #e1e9f3;
}
.main-menu .sub-menu:hover {
  color: #00153b;
}
.main-menu .sub-menu.sub-menu--left ul {
  left: 0;
  text-align: left;
}
.main-menu .sub-menu.sub-menu--right ul {
  right: 0;
  text-align: right;
}
@media screen and (min-width: 64em) {
  .main-menu .sub-menu:hover > a {
    z-index: 3000;
  }
  .main-menu .sub-menu:hover ul {
    display: block;
  }
}
@media screen and (max-width: 79.9375em) {
  .main-menu a:not(.button).avatar {
    width: 2.5em;
    height: 2.5em;
    -webkit-border-radius: 2.5em;
    border-radius: 2.5em;
  }
}
@media screen and (max-width: 47.9375em) {
  .main-menu .list.list--horizontal {
    display: block;
  }
  .main-menu .list.list--horizontal > * {
    display: block;
    padding: 0.625em 0;
  }
  .main-menu .list.list--horizontal > *:not(:first-child) {
    border-top: 1px solid #e1e9f3;
  }
  .main-menu.small--menu ul {
    position: absolute;
    top: 1.25em;
    left: 0;
    right: 0;
    max-width: 100%;
    padding: 20px;
    transition: all 0.3s linear;
    text-align: center;
    background-color: #f3f6fa;
    -webkit-box-shadow: 0 0 0 10px rgba(0, 21, 59, 0.5);
    box-shadow: 0 0 0 10px rgba(0, 21, 59, 0.5);
  }
  .main-menu.small--menu .menu__toggle {
    position: absolute;
    top: -50px;
    right: 0px;
  }
  .main-menu.large--menu {
    position: fixed;
    top: 0;
    bottom: 0;
    right: 0;
    max-width: 80%;
    transform: translate(100%, 0);
    padding: 20px;
    transition: all 0.3s linear;
    width: 280px;
    background-color: #f3f6fa;
  }
  .main-menu.large--menu .menu__toggle {
    position: absolute;
    top: 10px;
    left: -70px;
  }
}

.menu__toggle {
  display: inline-block;
  width: 3em;
  height: 3em;
  line-height: 3em;
  text-align: center;
  background-color: #f3f6fa;
  border: none;
  color: #00153b;
}
.menu__toggle input[type="checkbox"] {
  display: none;
}
@media screen and (min-width: 48em) {
  .menu__toggle {
    display: none;
  }
}

.mobile-menu {
  position: fixed;
  right: 0;
  top: 0;
  bottom: 0;
  z-index: 5000;
  width: 25em;
  height: 100vh;
  max-width: 85%;
  overflow: hidden;
  -webkit-overflow-scrolling: touch;
  overflow-y: auto;
  background-color: #fff;
  -webkit-box-shadow: 0 0 1.25em 0 rgba(0, 0, 0, 0.15);
  box-shadow: 0 0 1.25em 0 rgba(0, 0, 0, 0.15);
  opacity: 1;
  -webkit-transform: translate(100%, 0);
  transform: translate(100%, 0);
  -webkit-transform: translate3d(100%, 0, 0);
  transform: translate3d(100%, 0, 0);
  -webkit-transition: transform 0.15s linear, opacity 0.3s linear;
  transition: -webkit-transform 0.15s linear, opacity 0.3s linear;
  transition: transform 0.15s linear, opacity 0.3s linear;
}
.mobile-menu .list.list--horizontal {
  margin-bottom: 3.125em;
}
.mobile-menu .list.list--horizontal, .mobile-menu .list.list--horizontal > * {
  display: block;
}
.mobile-menu button:not(.button) {
  display: block;
  width: 100%;
  padding: 0.625em 1.25em;
  line-height: 2.25em;
  text-align: left;
  background-color: #f3f6fa;
  border: none;
  border-bottom: 1px solid #e1e9f3;
}
.mobile-menu a:not(.button) {
  padding: 0.625em 1.25em;
  border-bottom: 1px solid #e1e9f3;
}
.mobile-menu a.button, .mobile-menu .img-uploader a.upload-label, .img-uploader .mobile-menu a.upload-label {
  margin: 0.625em 1.25em;
  width: calc(100% - 2.5em);
}
.mobile-menu .sub-menu {
  position: relative;
}
.mobile-menu .sub-menu > a:after {
  position: absolute;
  top: 1.25em;
  right: 1.25em;
  display: inline-block;
  vertical-align: middle;
  content: " ";
  height: 0;
  width: 0;
  margin-left: 0.625em;
  border: solid transparent;
  border-width: 5px;
  border-top-color: currentColor;
  pointer-events: none;
}
.mobile-menu .sub-menu ul a {
  font-size: 0.9375em;
  background-color: #f3f6fa;
}
.mobile-menu .icon {
  width: 0.8em;
  height: 0.8em;
  vertical-align: baseline;
}
.is--menu-visible .mobile-menu {
  opacity: 1;
  -webkit-transform: translate(0, 0);
  transform: translate(0, 0);
  -webkit-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0);
}

.overlay {
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  top: 0;
  z-index: 4000;
  display: none;
  background-color: rgba(0, 21, 59, 0.4);
  opacity: 0;
  -webkit-transition: opacity 400ms;
  transition: opacity 400ms;
}
.overlay.has--visible {
  display: block;
  opacity: 1;
}

.no-record {
  position: relative;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -webkit-align-items: center;
  -ms-flex-align: center;
  align-items: center;
  -webkit-box-pack: center;
  -webkit-justify-content: center;
  -ms-flex-pack: center;
  justify-content: center;
  text-align: center;
  height: 18.75em;
  color: #d6e0ee;
}
.no-record label {
  display: block;
  margin-top: 1.25em;
  font-family: "solitas_ext_light", sans-serif;
  font-size: 1.125em;
  line-height: 1;
  text-transform: uppercase;
  color: #b9cbe2;
}

.innova-editor ul {
  padding-left: 1.25em;
  list-style: circle;
}
.innova-editor ol {
  padding-left: 1.25em;
  list-style: decimal-leading-zero;
}
.innova-editor a:not(.button) {
  text-decoration: underline;
  color: inherit;
}
.innova-editor a:not(.button):hover {
  color: inherit;
}
.innova-editor strong, .innova-editor b {
  font-family: "solitas_norm_medium", sans-serif;
  font-weight: normal;
}
.innova-editor em, .innova-editor i {
  font-family: "solitas_norm_regular_i", sans-serif;
  font-style: italic;
}
.innova-editor blockquote {
  font-family: "solitas_norm_bold", sans-serif;
  font-style: italic;
  margin: 0 0 1.25em 0;
}
.innova-editor hr {
  border: 0;
  border-bottom: solid 1px currentcolor;
  opacity: 0.1;
  margin: 2.49375em 0;
}
@media screen and (max-width: 47.9375em) {
  .innova-editor hr {
    margin: 1.3125em 0;
  }
}

.h__serach-wrapper {
  height: auto !important;
  max-height: 100% !important;
  overflow-y: auto !important;
}

.search-card {
  position: relative;
  display: block;
  background-color: #ffffff;
}
.search-card__action {
  position: relative;
  z-index: 2000;
  overflow: hidden;
  height: 4.375em;
  line-height: 4.375em;
  -webkit-transition: height 0.3s linear;
  transition: height 0.3s linear;
}
.search-card__action__label, .search-card__action__input, .search-card__action__close {
  display: inline-block;
  vertical-align: middle;
}
.search-card__action__input {
  width: 90%;
  width: calc(100% - 4em);
}
.search-card__action__input input[type="text"] {
  font-size: 1.125em;
  border: none;
}
.search-card__result {
  border-top: 1px solid #e1e9f3;
}
@media screen and (min-width: 48em) {
  .search-card__action {
    height: 8.5625em;
    line-height: 8.5625em;
  }
}
@media screen and (min-width: 80em) {
  .is--top.is--home .search-card__action {
    height: 13.875em;
    line-height: 13.875em;
  }
}

.img-uploader {
  display: block;
  padding: 0.625em;
  background-color: #ffffff;
  border: 1px solid #e1e9f3;
  text-align: center;
}
.img-uploader .upload-label {
  display: block;
  margin-top: 0.625em;
  background-color: #0599b2;
  color: #ffffff;
}
@media screen and (min-width: 48em) {
  .img-uploader {
    display: inline-block;
  }
}

.row:not(:last-child) {
  margin-bottom: 1.25em;
}

.field-set {
  display: block;
}
.field-set .field_label,
.field-set .field_info {
  margin-right: .75rem;
}
.field-set .field_label:not(:last-child),
.field-set .field_info:not(:last-child) {
  margin-bottom: .25rem;
}
.field-set .field_info {
  display: block;
  font-family: "solitas_norm_book_i", sans-serif;
  font-size: 0.875em;
  line-height: 1.71429em;
  color: #d03e2a;
}
.field-set .field_label {
  display: inline-block;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1.25em;
  line-height: 1.5em;
}
.field-set .field_label .field_info {
  font-size: 0.8em;
}
.field-set .field_cover {
  position: relative;
}
.field-set .field_cover .field_add-on {
  position: absolute;
  top: 1px;
  bottom: 1px;
  padding: 0 0.71429em;
  height: 3.57143em;
  font-size: 0.875em;
  line-height: 3.57143em;
  text-align: center;
  background-color: #f3f6fa;
}
.field-set .field_cover .field_add-on.add-on--left {
  left: 1px;
  border-right: 1px solid #e1e9f3;
}
.field-set .field_cover .field_add-on.add-on--right {
  right: 1px;
  border-left: 1px solid #e1e9f3;
}
.field-set .field_cover .field_add-on.add-on--left + input {
  padding-left: 4.375em;
}
.field-set .field_cover .field_add-on + input {
  padding-right: 9.375em;
}
.field-set .field_cover .field_add-on input, .field-set .field_cover .field_add-on select {
  background-color: transparent;
  border-width: 0;
  width: 100%;
  min-width: 11.42857em;
  margin: 0 -0.71429em;
}
.field-set .field_cover .field_add-on select {
  padding-top: 1.28571em;
  padding-bottom: 1.28571em;
  line-height: 1.42857em;
}
.field-set .field_cover .captcha-wrapper {
  position: absolute;
  right: 0px;
  bottom: 5px;
  height: 100%;
}
.field-set .field_cover .captcha-wrapper img {
  max-width: none;
  max-height: 3.375em;
}
.field-set .field_cover .captcha-wrapper .reload {
  display: inline-block;
  vertical-align: middle;
  padding-left: 0.625em;
  padding-right: 0.625em;
  height: 3.375em;
  line-height: 3.375em;
  background-color: #f3f6fa;
}
.field-set .field_cover .checkbox,
.field-set .field_cover .radio {
  display: inline;
}
.field-set .field_cover .checkbox {
  margin-right: 0;
}
.field-set .field_cover .checkbox .input-helper {
  width: 1rem;
  height: 1rem;
  display: inline-block;
  position: relative;
  vertical-align: middle;
  margin-right: 0.625em;
  border: 2px solid #d2dbe8;
  border-radius: .125rem;
  background: #fff;
  transition: border 0.15s linear, background 0.15s linear;
}
.field-set .field_cover .checkbox input[type="checkbox"]:checked + .input-helper,
.field-set .field_cover .checkbox input[type="checkbox"]:checked ~ .input-helper {
  border: 4px solid #00153b;
}
.field-set .field_cover .radio {
  margin-right: 0;
}
.field-set .field_cover .radio .input-helper {
  width: 1rem;
  height: 1rem;
  display: inline-block;
  position: relative;
  vertical-align: middle;
  margin-right: 0.625em;
  border: 2px solid #d2dbe8;
  border-radius: 1rem;
  background: #fff;
  transition: border 0.15s linear, background-color 0.15s linear;
}
.field-set .field_cover .radio input[type="radio"]:checked + .input-helper,
.field-set .field_cover .radio input[type="radio"]:checked ~ .input-helper {
  border: 4px solid #00153b;
}
.field-set .field_cover .list.list--horizontal li {
  padding-right: 1.25em;
}

.form:after {
  display: table;
  clear: both;
  content: " ";
}
.form.form--vertical .field-set {
  display: block;
  margin-bottom: 1.25em;
}
.form.form--vertical .field-set .caption-wraper,
.form.form--vertical .field-set .field-wraper {
  display: block;
}
.form.form--horizontal .field-set {
  display: table;
  table-layout: fixed;
  width: 100%;
  margin-bottom: 1.25em;
}
.form.form--horizontal .field-set .caption-wraper,
.form.form--horizontal .field-set .field-wraper {
  display: table-cell;
  vertical-align: top;
  line-height: 2.5em;
}
.form.form--horizontal .field-set .field-wraper {
  width: 65.51724%;
}

@media screen and (max-width: 47.9375em) {
  .form.form--horizontal .field-set {
    display: block;
  }
  .form.form--horizontal .field-set .caption-wraper,
  .form.form--horizontal .field-set .field-wraper {
    display: block;
    width: 100%;
  }
}
.errorlist {
  list-style: none;
  margin: 0;
  padding: 0;
}
.errorlist li:not(:last-child) a {
  border-bottom: 1px solid rgba(255, 255, 255, 0.3);
}
.errorlist a {
  display: block;
  padding: 0 1.53846em;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 0.8125em;
  height: 2em;
  line-height: 2em;
  background-color: #efc3bd;
  color: #d03e2a;
}

.mce_inline_error {
  display: block;
  padding: 0 1.53846em;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 0.8125em;
  height: 2em;
  line-height: 2em;
  background-color: #efc3bd;
  color: #d03e2a;
}

.filters a:not(.button):not(.f-button):not(.listing__filter-button) {
  position: relative;
  display: block;
  padding: 0.3125em;
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 0.875em;
  line-height: 1.2em;
  text-transform: uppercase;
  white-space: nowrap;
}
.filters .f-button {
  display: block;
  width: 3.125em;
  height: 3.125em;
  line-height: 3.125em;
  height: calc(3.125em + 2px);
  text-align: center;
  color: #e3e9f0;
}
.filters .f-button.active {
  background-color: #ffffff;
  border: 1px solid #e3e9f0;
  color: #00153b;
}
@media screen and (min-width: 48em) {
  .filters .list.list--horizontal > *:not(:last-child) {
    margin-right: 0.625em;
  }
}
@media screen and (max-width: 47.9375em) {
  .filters .list.list--horizontal > * {
    display: block;
    margin: 0 0 0.625em 0;
    padding: 0;
  }
}

.box {
  position: relative;
  display: block;
  text-align: center;
  background-color: #ffffff;
  border: 1px solid #e9e9ea;
}
.box .box__header,
.box .box__content {
  display: block;
  padding: 1.875em 1.25em;
}
.box .box__header {
  padding-bottom: 0.625em;
  font-size: 1em;
  text-transform: uppercase;
  background-color: #e1e9f3;
}
.box .box__heading {
  display: block;
  margin-bottom: 0.83333em;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.5em;
}

.info .info__heading {
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.25em;
  line-height: 2em;
  min-width: 30%;
}
.info .info__wrap {
  white-space: normal;
  width: 40%;
}
.info .info__actions {
  text-align: right;
}

@media screen and (min-width: 48em) {
  .info-table td, .info-table th {
    white-space: nowrap;
  }
  .info-table td:first-child, .info-table th:first-child {
    width: 50%;
  }
}
@media screen and (max-width: 47.9375em) {
  .info-table {
    text-align: center;
  }
  .info-table td {
    text-align: right;
  }
  .info-table td:before {
    float: left;
  }
  .info-table .info .info__wrap {
    width: 100%;
  }
  .info-table .info .info__actions {
    text-align: inherit;
  }
}
.activities-table .activity__heading {
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.25em;
  line-height: 2em;
  min-width: 30%;
}
.activities-table .activity__gallery .gallery {
  display: inline-block;
  min-width: 15em;
  max-width: 20em;
}
.activities-table .activity__gallery .gallery .gallery__item {
  width: 31.81818%;
  float: left;
  margin-right: 2.27273%;
  margin-top: 1.69492%;
}
.activities-table .activity__gallery .gallery .gallery__item:nth-child(3n) {
  width: 31.81818%;
  float: right;
  margin-right: 0;
}
.activities-table .activity__gallery .gallery .gallery__item:nth-child(3n+1):after {
  display: table;
  clear: both;
  content: " ";
}
.activities-table .activity__actions {
  text-align: right;
}

@media screen and (max-width: 79.9375em) {
  .activities-table {
    display: block;
  }
  .activities-table tbody, .activities-table tr, .activities-table td, .activities-table th {
    display: block;
  }
  .activities-table tbody:not(:last-child):not(:nth-last-child(2)), .activities-table tr:not(:last-child):not(:nth-last-child(2)), .activities-table td:not(:last-child):not(:nth-last-child(2)), .activities-table th:not(:last-child):not(:nth-last-child(2)) {
    padding-bottom: 0;
  }
  .activities-table td:nth-last-child(2), .activities-table th:nth-last-child(2) {
    padding-bottom: 0.625em !important;
  }
  .activities-table.table--bordered tr:not(:first-child) td, .activities-table.table--bordered tr:not(:first-child) th {
    border-width: 0;
  }
  .activities-table.table--bordered tr:not(:first-child) td:first-child, .activities-table.table--bordered tr:not(:first-child) th:first-child {
    border-top: 1px solid #e8eef6;
  }
  .activities-table .activity {
    text-align: center;
  }
  .activities-table .activity__actions {
    text-align: left;
    background-color: #e8eef6;
  }
}
@media screen and (max-width: 47.9375em) {
  .activities-table .activity__actions {
    text-align: inherit;
  }
}
.message .message__heading {
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.25em;
  line-height: 2em;
  min-width: 30%;
}
.message .message__actions {
  text-align: right;
}

@media screen and (max-width: 47.9375em) {
  .message .message__actions {
    text-align: inherit;
  }
}
.reply th {
  background-color: #e1e9f3;
}
.reply__heading {
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.25em;
  line-height: 2em;
  min-width: 30%;
}
.reply .comment__list:after {
  display: table;
  clear: both;
  content: " ";
}
.reply .comment__list .comment {
  display: inline-block;
  max-width: 43.75em;
  padding: 0.625em 1.25em;
  -webkit-border-radius: 0.625em;
  border-radius: 0.625em;
  border: 1px solid #e8eef6;
}
.reply .comment__list .comment:not(:last-child) {
  margin-bottom: 0.625em;
}
.reply .person {
  display: block;
}
.reply .person:not(:last-child) {
  margin-bottom: 1.875em;
}
.reply .person .person__image {
  position: static;
  display: inline-block;
  width: 5em;
  height: 5em;
  -webkit-border-radius: 5em;
  border-radius: 5em;
  overflow: hidden;
}
.reply .person .person__image img {
  width: 5em;
  height: 5em;
  -webkit-border-radius: 5em;
  border-radius: 5em;
  overflow: hidden;
}
.reply .person .person__comment {
  text-align: left;
}
.reply .person.person--first .comment__list .comment {
  clear: left;
  float: left;
  background-color: #ffffff;
}
.reply .person.person--second .comment__list .comment {
  clear: right;
  float: right;
  background-color: #f3f6fa;
}
@media screen and (max-width: 47.9375em) {
  .reply .person:not(:last-child) {
    margin-bottom: 1.25em;
  }
  .reply .person .media .media__figure {
    display: none;
  }
  .reply .person .media .media__body {
    padding-top: 0;
  }
}

.d-calendar__heading {
  padding: 0;
  margin: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1em;
}
.d-calendar__toolbar {
  padding: 0.625em 1.25em;
  background-color: #00153b;
  color: #ffffff;
}
.d-calendar__toolbar a {
  font-size: 0.75em;
  text-transform: uppercase;
  letter-spacing: 5px;
}
.d-calendar__view {
  border-collapse: separate;
  width: 100%;
  min-width: 43.75em;
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 0.8125em;
  text-align: center;
}
.d-calendar__view td, .d-calendar__view th {
  text-align: inherit;
}
.d-calendar__date {
  position: relative;
  width: 14.28571%;
  padding-bottom: 28.57143%;
  background-color: #ffffff;
  border: 1px solid #e1e9f3;
}
.d-calendar__date:not(:last-child) {
  border-right-width: 0;
}
.d-calendar__date.today {
  background-color: #f1fafb;
}
.d-calendar__date.disable {
  background-color: #f3f6fa;
}
.d-calendar__date.disable > span, .d-calendar__date.disable > div {
  opacity: 0.2;
}
.d-calendar__date.required .d-calendar__action:before {
  position: absolute;
  bottom: 5px;
  right: 5px;
  display: inline-block;
  width: 5px;
  height: 5px;
  content: " ";
  background-color: #d03e2a;
}
.d-calendar__week:not(:last-child) .d-calendar__date {
  border-bottom-width: 0;
}
.d-calendar__digit {
  position: absolute;
  top: 0;
  left: 0;
  padding: 0.625em;
  font-size: 1em;
  line-height: 1em;
}
.d-calendar__action {
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  padding: 0.625em;
  padding-top: 2.5em;
}
.d-calendar__action .time {
  position: relative;
  display: inline-block;
  width: 100%;
  margin-bottom: 3px;
  padding: 0.66667em;
  font-size: 0.9375em;
  line-height: 1em;
  text-align: left;
  background-color: #f3f6fa;
}
.d-calendar__action .time__del {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  padding: 0.66667em;
  background-color: rgba(0, 0, 0, 0.05);
}
.d-calendar__action .time.required {
  background-color: #fae8e6;
  /*&:before{
      position: absolute;
      bottom:em(5px);
      right: em(28px);
      display: inline-block;
      width: 5px;
      height: 5px;
      content: " ";
      background-color: _palette(red);
  }*/
}
.d-calendar__action .add,
.d-calendar__action .view {
  display: inline-block;
  width: 100%;
  padding: 0 0.625em;
  font-family: "solitas_ext_medium", sans-serif;
  font-size: 0.875em;
  text-align: center;
  text-transform: uppercase;
  color: #ffffff;
}
.d-calendar__action .add {
  display: none;
  background-color: #0599b2;
}
.d-calendar__action .view {
  background-color: #76b043;
}
.d-calendar__date:not(.disable) .d-calendar__action:hover .add {
  display: inline-block;
}

.ui-datepicker {
  display: none;
  font-family: "solitas_norm_book", sans-serif;
  padding: 0.625em;
  width: 18.75em;
  background: #ffffff;
  color: #00153b;
  border: 1px solid #e1e9f3;
  box-shadow: 0 5px 5px 0 rgba(0, 0, 0, 0.08);
  z-index: 2000 !important;
}
.ui-datepicker table {
  border-collapse: separate;
  width: 100%;
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 0.8125em;
  text-align: center;
}
.ui-datepicker table td, .ui-datepicker table th {
  text-align: inherit;
}
.ui-datepicker .ui-corner-all {
  border-radius: 0;
}
.ui-datepicker .ui-datepicker-header {
  margin-bottom: 1.25em;
}
.ui-datepicker .ui-datepicker-header .ui-datepicker-prev,
.ui-datepicker .ui-datepicker-header .ui-datepicker-next {
  font-size: 0.875em;
  height: 3.71429em;
  line-height: 3.71429em;
  padding: 0 0.625em;
  background-color: #f3f6fa;
  text-align: center;
  cursor: pointer;
}
.ui-datepicker .ui-datepicker-header .ui-datepicker-prev:hover,
.ui-datepicker .ui-datepicker-header .ui-datepicker-next:hover {
  background-color: #e1e9f3;
}
.ui-datepicker .ui-datepicker-header .ui-datepicker-prev {
  float: left;
}
.ui-datepicker .ui-datepicker-header .ui-datepicker-next {
  float: right;
}
.ui-datepicker .ui-datepicker-header .ui-datepicker-title {
  padding: 0 2.5em;
  line-height: 1.8em;
  text-align: center;
}
.ui-datepicker .ui-datepicker-header .ui-datepicker-title select {
  width: calc(50% - 4px);
  padding-left: 0.625em;
  padding-right: 0.625em;
  min-width: inherit;
}
.ui-datepicker .ui-datepicker-header .ui-datepicker-title select:not(:last-child) {
  border-right-width: 0;
}
.ui-datepicker .ui-datepicker-calendar {
  border-spacing: 0 2px;
  border-collapse: separate;
}
.ui-datepicker .ui-datepicker-calendar td {
  background-color: #f3f6fa;
}
.ui-datepicker .ui-datepicker-calendar .ui-state-default {
  display: block;
  margin: 4px;
}
.ui-datepicker .ui-datepicker-calendar .ui-state-hover {
  background-color: #e1e9f3;
}
.ui-datepicker .ui-datepicker-calendar .ui-state-active {
  background-color: #d03e2a;
  color: #ffffff;
}
.ui-datepicker .ui-datepicker-calendar .ui-datepicker-today .ui-state-highlight, .ui-datepicker .ui-datepicker-calendar .ui-datepicker-today .ui-state-hover {
  background-color: #76b043;
  color: #ffffff;
}
.ui-datepicker .ui-datepicker-buttonpane {
  margin: 1.25em -0.625em -0.625em;
  padding: 0.625em 0.625em 1.25em;
  background-color: #f3f6fa;
}
.ui-datepicker .ui-datepicker-buttonpane:after {
  display: table;
  clear: both;
  content: " ";
}
.ui-datepicker .ui-datepicker-buttonpane button.ui-state-default {
  display: inline-block;
  width: auto;
  padding: 0 1.42857em;
  line-height: 2.85714em;
  min-height: calc(2.85714em - 2px);
  min-width: 7.14286em;
  -webkit-border-radius: 2.85714em;
  border-radius: 2.85714em;
  font-size: 0.875em;
  letter-spacing: 0.375em;
  text-align: center;
  text-transform: uppercase;
  color: #ffffff;
  border: 0 solid transparent;
}
.ui-datepicker .ui-datepicker-buttonpane button.ui-state-default.ui-priority-secondary {
  background-color: #76b043;
}
.ui-datepicker .ui-datepicker-buttonpane button.ui-state-default.ui-priority-primary {
  background-color: #d03e2a;
}
.ui-datepicker .ui-datepicker-buttonpane button.ui-state-default.ui-datepicker-current {
  float: left;
}
.ui-datepicker .ui-datepicker-buttonpane button.ui-state-default.ui-datepicker-close {
  float: right;
}

.thumb__item {
  padding: 0.625em;
  background-color: #ffffff;
  border: 1px solid #e1e9f3;
  text-align: center;
}
.thumb__item .thumb__iframe, .thumb__item .thumb__image, .thumb__item .thumb__status {
  display: block;
}
.thumb__item .thumb__iframe {
  position: relative;
  display: block;
  width: 100%;
  padding-bottom: 56%;
}
.thumb__item .thumb__iframe iframe {
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
}
.thumb__item .thumb__delete {
  padding: 0 1.875em;
}
.thumb__item .thumb__delete:hover {
  background-color: #d03e2a !important;
}

.main-carousel__list, .main-carousel__list .main-carousel__item {
  position: relative;
  display: block;
}
.main-carousel__item .section__footer {
  padding: 1.875em;
}
.main-carousel__item .main-carousel__heading {
  display: block;
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 3em;
  line-height: 1.16667em;
  letter-spacing: 0.02em;
}
.main-carousel__item .main-carousel__special-heading {
  display: block;
  margin: 0;
  padding: 0;
  font-family: "brusher_regular", sans-serif;
  font-size: 4em;
  font-weight: normal;
  line-height: 1;
  letter-spacing: 0.02em;
}
.main-carousel__item .main-carousel__sub-heading {
  display: block;
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 1.125em;
  line-height: 1;
  letter-spacing: 0.3125em;
  text-transform: uppercase;
}
.main-carousel__item .main-carousel__regular {
  font-family: "solitas_norm_light", sans-serif;
  font-size: 1.125em;
  line-height: 2em;
  letter-spacing: 0.02em;
}
.main-carousel__item .main-carousel__video__icon {
  position: relative;
  display: inline-block;
  width: 5.625em;
  height: 5.625em;
  -webkit-border-radius: 5.625em;
  border-radius: 5.625em;
  border: 4px solid currentColor;
  overflow: hidden;
}
.main-carousel__item .main-carousel__video__icon:before {
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-35%, -50%);
  transform: translate(-35%, -50%);
  width: 0;
  height: 0;
  border: 18px solid transparent;
  border-left: 34px solid currentColor;
  border-right: 0 solid currentColor;
  content: " ";
}
.main-carousel__item .main-carousel__video__text {
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1.5em;
  line-height: 1.2;
  text-transform: uppercase;
}
.main-carousel__item .main-carousel__video__text small {
  display: block;
  font-size: 0.75em;
  text-transform: none;
}
.main-carousel__item .main-carousel__counter {
  font-family: "solitas_norm_bold", sans-serif;
  font-size: 3em;
  line-height: 1.2;
  text-transform: uppercase;
}
.main-carousel__item .main-carousel__counter small {
  display: block;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 0.375em;
  text-transform: none;
}
@media screen and (min-width: 48em) {
  .main-carousel__item .main-carousel__heading {
    font-size: 3.5em;
    line-height: 1.14286em;
  }
  .main-carousel__item .main-carousel__sub-heading {
    font-size: 1.25em;
  }
}
@media screen and (min-width: 80em) {
  .main-carousel__item .main-carousel__video {
    max-width: 50%;
  }
  .main-carousel__item .main-carousel__heading {
    font-size: 4em;
    line-height: 1.125em;
  }
  .main-carousel__item .main-carousel__sub-heading {
    font-size: 1.5em;
  }
}

/*.banner{
    position: relative;
    display: block;
    background-color: _palette(sec);
    color: _palette(bg);
    
    .banner__image{
        position: absolute;
        top: 0; left: 0; right: 0; bottom: 0;
        width: 100%;
        height: 100%;
        overflow: hidden;
        &:after{
            position: absolute;
            top: 0; left: 0; right: 0; bottom: 0;
            width:100%; height: 100%;
            content: " ";
            background-color: rgba(_palette(sec), 0.25);
        }
        img{
            height: 100%;
            max-width: 1000%;
        }
    }
    .banner__content{
        .heading-text{
            font-family: _font(family, ext-medium);
        }
        .regular-text{
            font-family: _font(family, ext-light);
        }
    }
    
    @include breakpoint(medium-min){
        .banner__content{
            height: 100vh;
        }
    }
    @include breakpoint(small-min){
        .banner__content{
            padding-top: em(105px);
        }
    }
}*/
.host__avatar {
  display: inline-block;
  width: 11.875rem;
  height: 11.875rem;
  -webkit-border-radius: 11.875rem;
  border-radius: 11.875rem;
  overflow: hidden;
}
.host__label {
  font-family: "solitas_norm_regular_i", sans-serif;
  font-size: 1.125em;
}
.host__name {
  padding: 0;
  margin: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.5em;
  text-transform: uppercase;
}
.host__desc {
  font-size: 1em;
}

.direction-card {
  position: relative;
  z-index: 1000;
  display: table;
  width: 100%;
  height: 400px;
  overflow: hidden;
}
.direction-card__map {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1000;
  max-height: 400px;
  overflow: hidden;
}
.direction-card__map img {
  min-height: 100%;
  min-width: 100%;
  max-width: 1000%;
  max-height: 1000%;
}
.direction-card__content {
  position: relative;
  display: table-cell;
  vertical-align: middle;
  width: 100%;
  height: 100%;
  text-align: center;
  color: white;
  z-index: 2000;
}
.direction-card__label {
  font-family: "solitas_norm_regular_i", sans-serif;
  font-size: 1.125em;
}
.direction-card__heading-text {
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.5em;
  text-transform: uppercase;
}

.activity-card {
  position: relative;
  display: block;
  background-color: #ffffff;
}
.activity-card__image, .activity-card__content, .activity-card__footer {
  position: relative;
}
.activity-card__content {
  display: block;
  padding: 1.25em;
}
.activity-card__footer {
  display: block;
  padding: 1.25em;
}
.activity-card__footer-inner {
  background-color: #f3f6fa;
  padding: 0.625em;
}
.activity-card__float {
  position: absolute;
  bottom: 0.625em;
  right: 0.625em;
}
.activity-card__float .float__icon {
  display: inline-block;
  margin: 5px;
  width: 2.25em;
  height: 2.25em;
  -webkit-border-radius: 2.25em;
  border-radius: 2.25em;
  line-height: 2em;
  text-align: center;
  background-color: rgba(0, 21, 59, 0.2);
  border: 2px solid rgba(255, 255, 255, 0.6);
  color: #ffffff;
}
.activity-card__float .float__icon.float__icon--heart.has--active {
  background-color: #d03e2a;
  border-color: #d03e2a;
}
.activity-card__float .float__icon.float__icon--share.has--active {
  background-color: #76b043;
  border-color: #76b043;
}
.activity-card__heading {
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 1.25em;
  line-height: 1.2em;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.activity-card__cat {
  font-family: "solitas_norm_book", sans-serif;
  font-size: 0.875em;
  line-height: 1.14286em;
  letter-spacing: 2.5px;
  text-transform: uppercase;
  color: #d03e2a;
}
.activity-card__host {
  font-family: "solitas_norm_book_i", sans-serif;
  font-size: 1em;
}
.activity-card__desc {
  max-width: 25em;
  line-height: 1.5em;
  display: none;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.activity-card__rating {
  color: #76b043;
}
.activity-card__status {
  font-family: "solitas_norm_book_i", sans-serif;
  font-size: 1em;
}
.activity-card__status:after {
  display: table;
  clear: both;
  content: " ";
}
.activity-card__price-number {
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.5em;
  line-height: 1.16667em;
}
.activity-card__price-text {
  font-family: "solitas_norm_book", sans-serif;
  font-size: 0.875em;
  line-height: 1.125em;
  letter-spacing: 5px;
  text-transform: uppercase;
  color: #abc0dc;
}
.activity-card.activity-card--open .activity-card__rating {
  color: #76b043;
}
.activity-card.activity-card--open .activity-card__status [class*="-icon"] {
  color: #76b043;
}
.activity-card.activity-card--closed .activity-card__rating {
  color: #d03e2a;
}
.activity-card.activity-card--closed .activity-card__status [class*="-icon"] {
  color: #d03e2a;
}
.activity-card.activity-card--cancel .activity-card__rating {
  color: #00153b;
}
.activity-card.activity-card--cancel .activity-card__status [class*="-icon"] {
  color: #abc0dc;
}
@media screen and (min-width: 80em) {
  .activity-card__float .float__icon.float__icon--heart:hover, .activity-card__float .float__icon.float__icon--heart:active {
    background-color: #d03e2a;
    border-color: #d03e2a;
  }
  .activity-card__float .float__icon.float__icon--share:hover, .activity-card__float .float__icon.float__icon--share:active {
    background-color: #76b043;
    border-color: #76b043;
  }
}
@media screen and (max-width: 63.9375em) {
  .activity-card__content-addition {
    display: none;
  }
  .activity-card__price-number {
    font-family: "solitas_norm_medium", sans-serif;
    font-size: 1.125em;
    line-height: 1.33333em;
  }
}

.activity-card__list {
  margin-top: -1.69492%;
}
.activity-card__list .activity-card {
  margin-top: 1.69492%;
}
.activity-card__list:after {
  display: table;
  clear: both;
  content: " ";
}
.activity-card__list.grid--style .activity-card__footer {
  padding-top: 0;
}
.activity-card__list.grid--style .activity-card__status {
  display: inline-block;
}
.activity-card__list.grid--style .activity-card__status-text {
  float: left;
}
.activity-card__list.grid--style .activity-card__status-icon {
  float: right;
  margin-left: 0.625em;
}
@media screen and (min-width: 48em) {
  .activity-card__list.grid--style .activity-card {
    width: 49.15254%;
    float: left;
    margin-right: 1.69492%;
  }
  .activity-card__list.grid--style .activity-card:nth-child(2n) {
    margin-right: 0;
  }
  .activity-card__list.grid--style .activity-card:nth-child(2n+1) {
    clear: left;
  }
}
@media screen and (min-width: 64em) {
  .activity-card__list.grid--style .activity-card {
    width: 32.20339%;
    float: left;
    margin-right: 1.69492%;
  }
  .activity-card__list.grid--style .activity-card:nth-child(2n) {
    margin-right: 1.69492%;
  }
  .activity-card__list.grid--style .activity-card:nth-child(2n+1) {
    clear: none;
  }
  .activity-card__list.grid--style .activity-card:nth-child(3n) {
    margin-right: 0;
  }
  .activity-card__list.grid--style .activity-card:nth-child(3n+1) {
    clear: left;
  }
}
@media screen and (min-width: 80em) {
  .activity-card__list.grid--style .activity-card {
    width: 23.72881%;
    float: left;
    margin-right: 1.69492%;
  }
  .activity-card__list.grid--style .activity-card:nth-child(2n), .activity-card__list.grid--style .activity-card:nth-child(3n) {
    margin-right: 1.69492%;
  }
  .activity-card__list.grid--style .activity-card:nth-child(2n+1), .activity-card__list.grid--style .activity-card:nth-child(3n+1) {
    clear: none;
  }
  .activity-card__list.grid--style .activity-card:nth-child(4n) {
    margin-right: 0;
  }
  .activity-card__list.grid--style .activity-card:nth-child(4n+1) {
    clear: left;
  }
}
@media screen and (min-width: 48em) {
  .activity-card__list.list--style .activity-card {
    display: table;
    table-layout: fixed;
  }
  .activity-card__list.list--style .activity-card__image, .activity-card__list.list--style .activity-card__content, .activity-card__list.list--style .activity-card__footer {
    position: relative;
    display: table-cell;
    vertical-align: top;
  }
  .activity-card__list.list--style .activity-card__image img, .activity-card__list.list--style .activity-card__content img, .activity-card__list.list--style .activity-card__footer img {
    width: 100%;
  }
  .activity-card__list.list--style .activity-card__image {
    width: 32.20339%;
  }
  .activity-card__list.list--style .activity-card__footer {
    width: 23.72881%;
  }
  .activity-card__list.list--style .activity-card__status-icon .icon {
    width: 1.8em;
    height: 1.8em;
  }
  .activity-card__list.list--style .activity-card__status-icon, .activity-card__list.list--style .activity-card__status-text {
    display: block;
    text-align: center;
  }
  .activity-card__list.list--style .activity-card__price-number, .activity-card__list.list--style .activity-card__price-text {
    display: block;
    text-align: center;
  }
}
@media screen and (min-width: 80em) {
  .activity-card__list.list--style .activity-card__image {
    width: 20%;
  }
  .activity-card__list.list--style .activity-card__footer {
    width: 18.36735%;
  }
  .activity-card__list.list--style .activity-card__content {
    @inlcude clearfix;
  }
  .activity-card__list.list--style .activity-card__content > div {
    display: block;
    float: left;
  }
  .activity-card__list.list--style .activity-card__content > div:first-child {
    width: 65%;
    padding-right: 1.25em;
    border-right: 1px solid rgba(0, 21, 59, 0.1);
  }
  .activity-card__list.list--style .activity-card__content > div:last-child {
    padding-left: 1.25em;
  }
  .activity-card__list.list--style .activity-card__desc {
    display: block;
  }
}
@media screen and (max-width: 47.9375em) {
  .activity-card__list.list--style .activity-card__footer {
    padding-top: 0;
  }
  .activity-card__list.list--style .activity-card__status {
    display: inline-block;
  }
  .activity-card__list.list--style .activity-card__status-icon {
    float: right;
    margin-left: 0.625em;
  }
}

.activity-media__image {
  width: 13.7931%;
}
.activity-media__heading {
  margin-bottom: 0;
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 1.25em;
  line-height: 1.2;
}
.activity-media__cat {
  display: inline-block;
  width: 100%;
  font-size: 0.875em;
  line-height: 1.2;
  letter-spacing: 0.3125em;
  text-transform: uppercase;
}
.activity-media__price {
  color: #b3b3b3;
}
@media screen and (min-width: 48em) {
  .activity-media__content {
    padding-left: 1.25em;
  }
  .activity-media__heading, .activity-media__cat {
    max-width: 70%;
  }
}

.activity-media__list {
  margin-top: -1.25em;
}
.activity-media__list:after {
  display: table;
  clear: both;
  content: " ";
}
.activity-media__list .activity-media {
  margin-top: 1.25em;
  padding-bottom: 1.25em;
  border-bottom: 1px solid #e1e9f3;
}
@media screen and (min-width: 80em) {
  .activity-media__list .activity-media {
    width: 48.27586%;
    float: left;
    margin-right: 3.44828%;
  }
  .activity-media__list .activity-media:nth-child(2n) {
    margin-right: 0;
  }
  .activity-media__list .activity-media:nth-child(2n+1) {
    clear: left;
  }
}

.category-card {
  position: relative;
  background-color: currentColor;
  color: #ffffff;
}
.category-card__image {
  position: relative;
  margin: 0;
  padding: 0;
  padding-bottom: 100%;
  overflow: hidden;
}
.category-card__image img {
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: scale(1) translate(-50%, -50%);
  transform: scale(1) translate(-50%, -50%);
  max-width: 100%;
  -webkit-transition: transform 0.3s linear;
  transition: -webkit-transform 0.3s linear;
  transition: transform 0.3s linear;
}
.category-card__content {
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  padding: 2em 1em;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.25em;
  line-height: 1.2;
  letter-spacing: 0;
  text-transform: uppercase;
  text-align: center;
  -webkit-transition: letter-spacing 0.3s linear;
  transition: letter-spacing 0.3s linear;
}
.category-card__content:before {
  position: absolute;
  top: 0;
  left: 0;
  width: 0;
  height: 1px;
  background-color: rgba(255, 255, 255, 0.3);
  content: "";
  -webkit-transition: width 0.3s linear;
  transition: width 0.3s linear;
}
.category-card:hover .category-card__image img {
  -webkit-transform: scale(1.06) translate(-50%, -50%);
  transform: scale(1.06) translate(-50%, -50%);
}
.category-card:hover .category-card__content {
  letter-spacing: 4px;
}
.category-card:hover .category-card__content:before {
  width: 100%;
}

.category-card__list:after {
  display: table;
  clear: both;
  content: " ";
}
.category-card__list.list--carousel {
  margin-left: -0.625rem;
  margin-right: -0.625rem;
}
.category-card__list.list--carousel .category-card {
  padding-left: 0.625rem;
  padding-right: 0.625rem;
}
.category-card__list.list--carousel.slick-slider {
  overflow: visible;
}
.category-card__list.list--carousel .slick-dots {
  bottom: -2.5em;
}
.category-card__list.list--2 {
  margin-top: -2.5641%;
}
.category-card__list.list--2 .category-card {
  margin-top: 2.5641%;
}
@media screen and (min-width: 48em) and (min-width: 48em) {
  .category-card__list.list--2 .category-card {
    width: 48.71795%;
    float: left;
    margin-right: 2.5641%;
  }
  .category-card__list.list--2 .category-card:nth-child(2n) {
    margin-right: 0;
  }
  .category-card__list.list--2 .category-card:nth-child(2n+1) {
    clear: left;
  }
}

.category-card__list.list--3 {
  margin-top: -2.27273%;
}
.category-card__list.list--3 .category-card {
  margin-top: 2.27273%;
}
@media screen and (min-width: 48em) {
  .category-card__list.list--3 .category-card {
    width: 48.71795%;
    float: left;
    margin-right: 2.5641%;
  }
  .category-card__list.list--3 .category-card:nth-child(2n) {
    margin-right: 0;
  }
  .category-card__list.list--3 .category-card:nth-child(2n+1) {
    clear: left;
  }
}
@media screen and (min-width: 80em) {
  .category-card__list.list--3 .category-card {
    width: 31.81818%;
    float: left;
    margin-right: 2.27273%;
  }
  .category-card__list.list--3 .category-card:nth-child(2n) {
    margin-right: 2.27273%;
  }
  .category-card__list.list--3 .category-card:nth-child(2n+1) {
    clear: none;
  }
  .category-card__list.list--3 .category-card:nth-child(3n) {
    margin-right: 0;
  }
  .category-card__list.list--3 .category-card:nth-child(3n+1) {
    clear: left;
  }
}
.category-card__list.list--4 {
  margin-top: -1.69492%;
}
.category-card__list.list--4 .category-card {
  margin-top: 1.69492%;
}
@media screen and (min-width: 48em) {
  .category-card__list.list--4 .category-card {
    width: 48.71795%;
    float: left;
    margin-right: 2.5641%;
  }
  .category-card__list.list--4 .category-card:nth-child(2n) {
    margin-right: 0;
  }
  .category-card__list.list--4 .category-card:nth-child(2n+1) {
    clear: left;
  }
}
@media screen and (min-width: 80em) {
  .category-card__list.list--4 .category-card {
    width: 23.72881%;
    float: left;
    margin-right: 1.69492%;
  }
  .category-card__list.list--4 .category-card:nth-child(2n) {
    margin-right: 1.69492%;
  }
  .category-card__list.list--4 .category-card:nth-child(2n+1) {
    clear: none;
  }
  .category-card__list.list--4 .category-card:nth-child(4n) {
    margin-right: 0;
  }
  .category-card__list.list--4 .category-card:nth-child(4n+1) {
    clear: left;
  }
}

.island-card {
  position: relative;
  display: block;
}
.island-card__image, .island-card__content {
  display: table-cell;
  vertical-align: middle;
  width: 62.5em;
}
.island-card__image {
  width: 49.15254%;
}
.island-card__image img {
  width: 100%;
}
.island-card__content {
  padding: 1.25em;
  border: 1px solid #e1e9f3;
  border-left-width: 0;
}
.island-card__heading {
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.5em;
  line-height: 1.2em;
  letter-spacing: 0.02em;
  text-transform: uppercase;
}
.island-card__text {
  font-family: "solitas_norm_light", sans-serif;
  font-size: 1.125em;
  line-height: 1.33333em;
}
@media screen and (max-width: 47.9375em) {
  .island-card__image, .island-card__content {
    display: block;
    width: auto;
  }
  .island-card__content {
    border: 1px solid #e1e9f3;
    border-top-width: 0;
  }
}

.island-card__list {
  margin-top: -1.25em;
}
.island-card__list .island-card {
  margin-top: 1.25em;
}
@media screen and (min-width: 80em) {
  .island-card__list .island-card {
    width: 49.15254%;
    float: left;
    margin-right: 1.69492%;
  }
  .island-card__list .island-card:nth-child(2n+1) {
    margin-right: 0;
  }
  .island-card__list .island-card:nth-child(2n) {
    clear: left;
  }
  .island-card__list .island-card.island-card--first {
    padding: 3.75em 0 1.875em;
    width: 100%;
  }
  .island-card__list .island-card.island-card--first:before {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 23.72881%;
    z-index: 1000;
    height: 100%;
    width: 100%;
    max-width: 57.62712%;
    content: " ";
    background-color: #e1e9f3;
  }
  .island-card__list .island-card.island-card--first .island-card__image,
  .island-card__list .island-card.island-card--first .island-card__content {
    position: relative;
    vertical-align: bottom;
    z-index: 2000;
  }
  .island-card__list .island-card.island-card--first .island-card__content {
    border-width: 0;
    padding-right: 0;
    padding-bottom: 0;
  }
  .island-card__list .island-card.island-card--first .island-card__text {
    max-width: 50%;
  }
}

.escapade__item {
  display: block;
  padding: 0.625em 1.25em;
  border: 1px solid #e1e9f3;
  border-bottom-width: 0;
}
.escapade__item:last-child {
  border-bottom-width: 1px;
}
.escapade__item:after {
  display: table;
  clear: both;
  content: " ";
}
.escapade__item:hover {
  background-color: #ecf1f7;
}
.escapade__item .escapade__heading {
  display: block;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1em;
  line-height: 1.75em;
  text-transform: uppercase;
}
.escapade__item .escapade__price {
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 1em;
  line-height: 1.75em;
  letter-spacing: 0.02em;
}
.escapade__item .escapade__duration-number {
  display: block;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.5em;
  line-height: 1.16667em;
  color: #d03e2a;
}
.escapade__item .escapade__duration-text {
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 0.875em;
  line-height: 1.2;
  letter-spacing: 0.3125em;
  text-transform: uppercase;
}
.escapade__item.featured__item .escapade__heading {
  font-size: 1.25em;
  line-height: 1.4em;
}
.escapade__item > div:last-child {
  color: #b3b3b3;
}
@media screen and (min-width: 48em) {
  .escapade__item > div:last-child {
    text-align: right;
  }
}

.escapade__list {
  display: block;
  margin-top: -1.25em;
}
.escapade__list:after {
  display: table;
  clear: both;
  content: " ";
}
.escapade__list .escapade__col {
  margin-top: 1.25em;
}
@media screen and (min-width: 80em) {
  .escapade__list .escapade__col {
    width: 32.20339%;
    float: left;
    margin-right: 1.69492%;
  }
  .escapade__list .escapade__col:nth-child(3n) {
    margin-right: 0;
  }
  .escapade__list .escapade__col:nth-child(3n+1) {
    clear: left;
  }
}

.point__list {
  position: relative;
  display: block;
  margin-top: -1.875em;
}
.point__list:after {
  display: table;
  clear: both;
  content: " ";
}
.point__item {
  margin-top: 1.875em;
}
@media screen and (min-width: 64em) {
  .point__item {
    width: 47.36842%;
    float: left;
    margin-right: 5.26316%;
  }
  .point__item:nth-child(2n) {
    margin-right: 0;
  }
  .point__item:nth-child(2n+1) {
    clear: left;
  }
}
.point__image {
  width: 31.03448%;
}
.point__icon {
  display: inline-block;
  width: 8.125em;
  height: 8.125em;
  -webkit-border-radius: 8.125em;
  border-radius: 8.125em;
  overflow: hidden;
  text-align: center;
}
.point__heading {
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.5em;
  line-height: 1.2em;
  letter-spacing: 0.02em;
  text-transform: uppercase;
  color: #67c9d3;
}
.point__text {
  font-family: "solitas_norm_light", sans-serif;
  font-size: 1.125em;
  line-height: 1.77778em;
}

.tip__list {
  position: relative;
  display: block;
  margin-top: -1.875em;
}
.tip__list:after {
  display: table;
  clear: both;
  content: " ";
}
.tip__item {
  margin-top: 1.875em;
}
.tip__image {
  width: 31.03448%;
}
.tip__icon {
  display: inline-block;
  width: 11.875em;
  height: 11.875em;
  -webkit-border-radius: 11.875em;
  border-radius: 11.875em;
  overflow: hidden;
  text-align: center;
}
.tip__heading {
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.5em;
  line-height: 1.2em;
  letter-spacing: 0.02em;
  text-transform: uppercase;
}
.tip__text {
  font-family: "solitas_norm_light", sans-serif;
  font-size: 1.125em;
  line-height: 1.77778em;
}

@media screen and (min-width: 80em) {
  .tip__item {
    width: 28.57143%;
    float: left;
    margin-right: 7.14286%;
  }
  .tip__item:nth-child(3n) {
    margin-right: 0;
  }
  .tip__item:nth-child(3n+1) {
    clear: left;
  }
  .tip__item .tip__image,
  .tip__item .tip__content {
    display: block;
    width: 100%;
    padding-left: 0;
    text-align: center;
  }
  .tip__item .tip__image:not(:last-child),
  .tip__item .tip__content:not(:last-child) {
    margin-bottom: 2.5em;
  }
}
.calendar__actions {
  padding: 0.625em 1.25em;
  background-color: #00153b;
  color: #ffffff;
}
.calendar__actions a {
  font-size: 0.75em;
  text-transform: uppercase;
  letter-spacing: 5px;
}
.calendar__dates {
  margin: 1.25em;
  border-spacing: 0 2px;
  border-collapse: separate;
  width: calc(100% - 2.5em);
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 0.8125em;
  text-align: center;
}
.calendar__dates td, .calendar__dates th {
  text-align: inherit;
}
.calendar__dates td {
  background-color: rgba(0, 21, 59, 0.08);
}
.calendar__dates td:first-child {
  -webkit-border-radius: 2.30769em 0 0 2.30769em;
  border-radius: 2.30769em 0 0 2.30769em;
}
.calendar__dates td:last-child {
  -webkit-border-radius: 0 2.30769em 2.30769em 0;
  border-radius: 0 2.30769em 2.30769em 0;
}
.calendar__dates__body .no-avail span {
  opacity: 0.3;
}
.calendar__dates__body .start span,
.calendar__dates__body .end span,
.calendar__dates__body .selection span {
  display: inline-block;
  width: 1.75em;
  height: 1.75em;
  line-height: 2em;
  -webkit-border-radius: 50%;
  border-radius: 50%;
  margin: 2px;
}
.calendar__dates__body .selection {
  background-color: #ffffff !important;
  cursor: pointer;
}
.calendar__dates__body .selection:hover span {
  background-color: #e1e9f3;
}
.calendar__dates__body .start span {
  background-color: #d03e2a !important;
  color: #ffffff;
}
.calendar__dates__body .end span {
  background-color: #76b043 !important;
  color: #ffffff;
}
.calendar__dates__body .has--select span {
  background-color: #00153b !important;
  color: #ffffff !important;
}
.calendar__heading {
  padding: 0;
  margin: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1em;
}

.rating__block {
  font-family: "solitas_norm_light", sans-serif;
  font-size: 0.875em;
  color: currentColor;
}
.rating__block .rating__numbers {
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 4em;
  line-height: 0.75em;
  letter-spacing: 0.02em;
  text-transform: uppercase;
  margin: 0;
  padding: 0;
}

.rating {
  position: relative;
  display: inline-block;
  vertical-align: middle;
  width: 8.92857em;
  fill: #ffffff;
}
.rating .rating__overlay {
  position: relative;
  z-index: 3;
  display: block;
  width: 100%;
  padding-bottom: 20%;
  background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWlkWU1pZCIgdmlld0JveD0iMCAwIDEyNSAyNSI+CiAgPHBhdGggZmlsbD0iI2ZmZmZmZiIgZD0iTTAuMDAwLDI1LjAwMCBMMC4wMDAsLTAuMDAwIEwxMjUuMDAwLC0wLjAwMCBMMTI1LjAwMCwyNS4wMDAgTDAuMDAwLDI1LjAwMCBaTTIzLjU5NCw5LjM0NSBMMTYuODY5LDguMzY3IEwxMy44NTQsMi4yNzIgQzEzLjczNCwyLjAxNyAxMy41MDYsMS43MjIgMTMuMTk4LDEuNzIyIEMxMi44OTAsMS43MjIgMTIuNjYyLDIuMDE3IDEyLjU0MSwyLjI3MiBMOS41MjcsOC4zNjcgTDIuODAyLDkuMzQ1IEMyLjQ2Nyw5LjM5OSAyLjA1MSw5LjU2MCAyLjA1MSw5Ljk2MiBDMi4wNTEsMTAuMjAzIDIuMjI1LDEwLjQzMSAyLjM4NiwxMC42MDUgTDcuMjYzLDE1LjM0NyBMNi4xMTEsMjIuMDQ2IEM2LjA5NywyMi4xNDAgNi4wODQsMjIuMjIwIDYuMDg0LDIyLjMxNCBDNi4wODQsMjIuNjYyIDYuMjU4LDIyLjk4NCA2LjY0NywyMi45ODQgQzYuODM0LDIyLjk4NCA3LjAwOCwyMi45MTcgNy4xODIsMjIuODIzIEwxMy4xOTgsMTkuNjYxIEwxOS4yMTMsMjIuODIzIEMxOS4zNzQsMjIuOTE3IDE5LjU2MSwyMi45ODQgMTkuNzQ5LDIyLjk4NCBDMjAuMTM4LDIyLjk4NCAyMC4yOTgsMjIuNjYyIDIwLjI5OCwyMi4zMTQgQzIwLjI5OCwyMi4yMjAgMjAuMjk4LDIyLjE0MCAyMC4yODUsMjIuMDQ2IEwxOS4xMzMsMTUuMzQ3IEwyMy45OTYsMTAuNjA1IEMyNC4xNzAsMTAuNDMxIDI0LjM0NCwxMC4yMDMgMjQuMzQ0LDkuOTYyIEMyNC4zNDQsOS41NjAgMjMuOTE2LDkuMzk5IDIzLjU5NCw5LjM0NSBaTTQ4LjI3OSw5LjM0NSBMNDEuNTU0LDguMzY3IEwzOC41NDAsMi4yNzIgQzM4LjQxOSwyLjAxNyAzOC4xOTEsMS43MjIgMzcuODgzLDEuNzIyIEMzNy41NzUsMS43MjIgMzcuMzQ3LDIuMDE3IDM3LjIyNywyLjI3MiBMMzQuMjEyLDguMzY3IEwyNy40ODcsOS4zNDUgQzI3LjE1Miw5LjM5OSAyNi43MzcsOS41NjAgMjYuNzM3LDkuOTYyIEMyNi43MzcsMTAuMjAzIDI2LjkxMSwxMC40MzEgMjcuMDcyLDEwLjYwNSBMMzEuOTQ4LDE1LjM0NyBMMzAuNzk2LDIyLjA0NiBDMzAuNzgzLDIyLjE0MCAzMC43NjksMjIuMjIwIDMwLjc2OSwyMi4zMTQgQzMwLjc2OSwyMi42NjIgMzAuOTQzLDIyLjk4NCAzMS4zMzIsMjIuOTg0IEMzMS41MTksMjIuOTg0IDMxLjY5NCwyMi45MTcgMzEuODY4LDIyLjgyMyBMMzcuODgzLDE5LjY2MSBMNDMuODk5LDIyLjgyMyBDNDQuMDU5LDIyLjkxNyA0NC4yNDcsMjIuOTg0IDQ0LjQzNCwyMi45ODQgQzQ0LjgyMywyMi45ODQgNDQuOTg0LDIyLjY2MiA0NC45ODQsMjIuMzE0IEM0NC45ODQsMjIuMjIwIDQ0Ljk4NCwyMi4xNDAgNDQuOTcwLDIyLjA0NiBMNDMuODE4LDE1LjM0NyBMNDguNjgxLDEwLjYwNSBDNDguODU1LDEwLjQzMSA0OS4wMzAsMTAuMjAzIDQ5LjAzMCw5Ljk2MiBDNDkuMDMwLDkuNTYwIDQ4LjYwMSw5LjM5OSA0OC4yNzksOS4zNDUgWk03Mi45NjUsOS4zNDUgTDY2LjIzOSw4LjM2NyBMNjMuMjI1LDIuMjcyIEM2My4xMDQsMi4wMTcgNjIuODc3LDEuNzIyIDYyLjU2OSwxLjcyMiBDNjIuMjYwLDEuNzIyIDYyLjAzMywyLjAxNyA2MS45MTIsMi4yNzIgTDU4Ljg5OCw4LjM2NyBMNTIuMTcyLDkuMzQ1IEM1MS44MzcsOS4zOTkgNTEuNDIyLDkuNTYwIDUxLjQyMiw5Ljk2MiBDNTEuNDIyLDEwLjIwMyA1MS41OTYsMTAuNDMxIDUxLjc1NywxMC42MDUgTDU2LjYzNCwxNS4zNDcgTDU1LjQ4MSwyMi4wNDYgQzU1LjQ2OCwyMi4xNDAgNTUuNDU1LDIyLjIyMCA1NS40NTUsMjIuMzE0IEM1NS40NTUsMjIuNjYyIDU1LjYyOSwyMi45ODQgNTYuMDE3LDIyLjk4NCBDNTYuMjA1LDIyLjk4NCA1Ni4zNzksMjIuOTE3IDU2LjU1MywyMi44MjMgTDYyLjU2OSwxOS42NjEgTDY4LjU4NCwyMi44MjMgQzY4Ljc0NSwyMi45MTcgNjguOTMyLDIyLjk4NCA2OS4xMjAsMjIuOTg0IEM2OS41MDgsMjIuOTg0IDY5LjY2OSwyMi42NjIgNjkuNjY5LDIyLjMxNCBDNjkuNjY5LDIyLjIyMCA2OS42NjksMjIuMTQwIDY5LjY1NiwyMi4wNDYgTDY4LjUwNCwxNS4zNDcgTDczLjM2NywxMC42MDUgQzczLjU0MSwxMC40MzEgNzMuNzE1LDEwLjIwMyA3My43MTUsOS45NjIgQzczLjcxNSw5LjU2MCA3My4yODYsOS4zOTkgNzIuOTY1LDkuMzQ1IFpNOTcuNjUwLDkuMzQ1IEw5MC45MjUsOC4zNjcgTDg3LjkxMCwyLjI3MiBDODcuNzkwLDIuMDE3IDg3LjU2MiwxLjcyMiA4Ny4yNTQsMS43MjIgQzg2Ljk0NiwxLjcyMiA4Ni43MTgsMi4wMTcgODYuNTk4LDIuMjcyIEw4My41ODMsOC4zNjcgTDc2Ljg1OCw5LjM0NSBDNzYuNTIzLDkuMzk5IDc2LjEwNyw5LjU2MCA3Ni4xMDcsOS45NjIgQzc2LjEwNywxMC4yMDMgNzYuMjgyLDEwLjQzMSA3Ni40NDIsMTAuNjA1IEw4MS4zMTksMTUuMzQ3IEw4MC4xNjcsMjIuMDQ2IEM4MC4xNTMsMjIuMTQwIDgwLjE0MCwyMi4yMjAgODAuMTQwLDIyLjMxNCBDODAuMTQwLDIyLjY2MiA4MC4zMTQsMjIuOTg0IDgwLjcwMywyMi45ODQgQzgwLjg5MCwyMi45ODQgODEuMDY0LDIyLjkxNyA4MS4yMzksMjIuODIzIEw4Ny4yNTQsMTkuNjYxIEw5My4yNjksMjIuODIzIEM5My40MzAsMjIuOTE3IDkzLjYxOCwyMi45ODQgOTMuODA1LDIyLjk4NCBDOTQuMTk0LDIyLjk4NCA5NC4zNTQsMjIuNjYyIDk0LjM1NCwyMi4zMTQgQzk0LjM1NCwyMi4yMjAgOTQuMzU0LDIyLjE0MCA5NC4zNDEsMjIuMDQ2IEw5My4xODksMTUuMzQ3IEw5OC4wNTIsMTAuNjA1IEM5OC4yMjYsMTAuNDMxIDk4LjQwMCwxMC4yMDMgOTguNDAwLDkuOTYyIEM5OC40MDAsOS41NjAgOTcuOTcyLDkuMzk5IDk3LjY1MCw5LjM0NSBaTTEyMi4zMzYsOS4zNDUgTDExNS42MTAsOC4zNjcgTDExMi41OTYsMi4yNzIgQzExMi40NzUsMi4wMTcgMTEyLjI0OCwxLjcyMiAxMTEuOTM5LDEuNzIyIEMxMTEuNjMxLDEuNzIyIDExMS40MDMsMi4wMTcgMTExLjI4MywyLjI3MiBMMTA4LjI2OSw4LjM2NyBMMTAxLjU0Myw5LjM0NSBDMTAxLjIwOCw5LjM5OSAxMDAuNzkzLDkuNTYwIDEwMC43OTMsOS45NjIgQzEwMC43OTMsMTAuMjAzIDEwMC45NjcsMTAuNDMxIDEwMS4xMjgsMTAuNjA1IEwxMDYuMDA0LDE1LjM0NyBMMTA0Ljg1MiwyMi4wNDYgQzEwNC44MzksMjIuMTQwIDEwNC44MjUsMjIuMjIwIDEwNC44MjUsMjIuMzE0IEMxMDQuODI1LDIyLjY2MiAxMDUuMDAwLDIyLjk4NCAxMDUuMzg4LDIyLjk4NCBDMTA1LjU3NiwyMi45ODQgMTA1Ljc1MCwyMi45MTcgMTA1LjkyNCwyMi44MjMgTDExMS45MzksMTkuNjYxIEwxMTcuOTU1LDIyLjgyMyBDMTE4LjExNSwyMi45MTcgMTE4LjMwMywyMi45ODQgMTE4LjQ5MSwyMi45ODQgQzExOC44NzksMjIuOTg0IDExOS4wNDAsMjIuNjYyIDExOS4wNDAsMjIuMzE0IEMxMTkuMDQwLDIyLjIyMCAxMTkuMDQwLDIyLjE0MCAxMTkuMDI2LDIyLjA0NiBMMTE3Ljg3NCwxNS4zNDcgTDEyMi43MzgsMTAuNjA1IEMxMjIuOTEyLDEwLjQzMSAxMjMuMDg2LDEwLjIwMyAxMjMuMDg2LDkuOTYyIEMxMjMuMDg2LDkuNTYwIDEyMi42NTcsOS4zOTkgMTIyLjMzNiw5LjM0NSBaIiAvPgo8L3N2Zz4K);
  background-position: top left;
  background-size: cover;
}
.rating .rating__bg,
.rating .rating__score {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 2;
  background-color: currentColor;
}
.rating .rating__bg {
  z-index: 1;
  opacity: 0.2;
}
.rating.rating--large {
  font-size: 1.2em;
}
.rating.rating--large .rating__bg,
.rating.rating--large .rating__score {
  left: 0;
  right: 0;
}
.rating.rating--small {
  font-size: 0.8em;
}
.rating.rating--light .rating__overlay {
  background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWlkWU1pZCIgdmlld0JveD0iMCAwIDEyNSAyNSI+CiAgPHBhdGggZmlsbD0iI2YzZjZmYSIgZD0iTTAuMDAwLDI1LjAwMCBMMC4wMDAsLTAuMDAwIEwxMjUuMDAwLC0wLjAwMCBMMTI1LjAwMCwyNS4wMDAgTDAuMDAwLDI1LjAwMCBaTTIzLjU5NCw5LjM0NSBMMTYuODY5LDguMzY3IEwxMy44NTQsMi4yNzIgQzEzLjczNCwyLjAxNyAxMy41MDYsMS43MjIgMTMuMTk4LDEuNzIyIEMxMi44OTAsMS43MjIgMTIuNjYyLDIuMDE3IDEyLjU0MSwyLjI3MiBMOS41MjcsOC4zNjcgTDIuODAyLDkuMzQ1IEMyLjQ2Nyw5LjM5OSAyLjA1MSw5LjU2MCAyLjA1MSw5Ljk2MiBDMi4wNTEsMTAuMjAzIDIuMjI1LDEwLjQzMSAyLjM4NiwxMC42MDUgTDcuMjYzLDE1LjM0NyBMNi4xMTEsMjIuMDQ2IEM2LjA5NywyMi4xNDAgNi4wODQsMjIuMjIwIDYuMDg0LDIyLjMxNCBDNi4wODQsMjIuNjYyIDYuMjU4LDIyLjk4NCA2LjY0NywyMi45ODQgQzYuODM0LDIyLjk4NCA3LjAwOCwyMi45MTcgNy4xODIsMjIuODIzIEwxMy4xOTgsMTkuNjYxIEwxOS4yMTMsMjIuODIzIEMxOS4zNzQsMjIuOTE3IDE5LjU2MSwyMi45ODQgMTkuNzQ5LDIyLjk4NCBDMjAuMTM4LDIyLjk4NCAyMC4yOTgsMjIuNjYyIDIwLjI5OCwyMi4zMTQgQzIwLjI5OCwyMi4yMjAgMjAuMjk4LDIyLjE0MCAyMC4yODUsMjIuMDQ2IEwxOS4xMzMsMTUuMzQ3IEwyMy45OTYsMTAuNjA1IEMyNC4xNzAsMTAuNDMxIDI0LjM0NCwxMC4yMDMgMjQuMzQ0LDkuOTYyIEMyNC4zNDQsOS41NjAgMjMuOTE2LDkuMzk5IDIzLjU5NCw5LjM0NSBaTTQ4LjI3OSw5LjM0NSBMNDEuNTU0LDguMzY3IEwzOC41NDAsMi4yNzIgQzM4LjQxOSwyLjAxNyAzOC4xOTEsMS43MjIgMzcuODgzLDEuNzIyIEMzNy41NzUsMS43MjIgMzcuMzQ3LDIuMDE3IDM3LjIyNywyLjI3MiBMMzQuMjEyLDguMzY3IEwyNy40ODcsOS4zNDUgQzI3LjE1Miw5LjM5OSAyNi43MzcsOS41NjAgMjYuNzM3LDkuOTYyIEMyNi43MzcsMTAuMjAzIDI2LjkxMSwxMC40MzEgMjcuMDcyLDEwLjYwNSBMMzEuOTQ4LDE1LjM0NyBMMzAuNzk2LDIyLjA0NiBDMzAuNzgzLDIyLjE0MCAzMC43NjksMjIuMjIwIDMwLjc2OSwyMi4zMTQgQzMwLjc2OSwyMi42NjIgMzAuOTQzLDIyLjk4NCAzMS4zMzIsMjIuOTg0IEMzMS41MTksMjIuOTg0IDMxLjY5NCwyMi45MTcgMzEuODY4LDIyLjgyMyBMMzcuODgzLDE5LjY2MSBMNDMuODk5LDIyLjgyMyBDNDQuMDU5LDIyLjkxNyA0NC4yNDcsMjIuOTg0IDQ0LjQzNCwyMi45ODQgQzQ0LjgyMywyMi45ODQgNDQuOTg0LDIyLjY2MiA0NC45ODQsMjIuMzE0IEM0NC45ODQsMjIuMjIwIDQ0Ljk4NCwyMi4xNDAgNDQuOTcwLDIyLjA0NiBMNDMuODE4LDE1LjM0NyBMNDguNjgxLDEwLjYwNSBDNDguODU1LDEwLjQzMSA0OS4wMzAsMTAuMjAzIDQ5LjAzMCw5Ljk2MiBDNDkuMDMwLDkuNTYwIDQ4LjYwMSw5LjM5OSA0OC4yNzksOS4zNDUgWk03Mi45NjUsOS4zNDUgTDY2LjIzOSw4LjM2NyBMNjMuMjI1LDIuMjcyIEM2My4xMDQsMi4wMTcgNjIuODc3LDEuNzIyIDYyLjU2OSwxLjcyMiBDNjIuMjYwLDEuNzIyIDYyLjAzMywyLjAxNyA2MS45MTIsMi4yNzIgTDU4Ljg5OCw4LjM2NyBMNTIuMTcyLDkuMzQ1IEM1MS44MzcsOS4zOTkgNTEuNDIyLDkuNTYwIDUxLjQyMiw5Ljk2MiBDNTEuNDIyLDEwLjIwMyA1MS41OTYsMTAuNDMxIDUxLjc1NywxMC42MDUgTDU2LjYzNCwxNS4zNDcgTDU1LjQ4MSwyMi4wNDYgQzU1LjQ2OCwyMi4xNDAgNTUuNDU1LDIyLjIyMCA1NS40NTUsMjIuMzE0IEM1NS40NTUsMjIuNjYyIDU1LjYyOSwyMi45ODQgNTYuMDE3LDIyLjk4NCBDNTYuMjA1LDIyLjk4NCA1Ni4zNzksMjIuOTE3IDU2LjU1MywyMi44MjMgTDYyLjU2OSwxOS42NjEgTDY4LjU4NCwyMi44MjMgQzY4Ljc0NSwyMi45MTcgNjguOTMyLDIyLjk4NCA2OS4xMjAsMjIuOTg0IEM2OS41MDgsMjIuOTg0IDY5LjY2OSwyMi42NjIgNjkuNjY5LDIyLjMxNCBDNjkuNjY5LDIyLjIyMCA2OS42NjksMjIuMTQwIDY5LjY1NiwyMi4wNDYgTDY4LjUwNCwxNS4zNDcgTDczLjM2NywxMC42MDUgQzczLjU0MSwxMC40MzEgNzMuNzE1LDEwLjIwMyA3My43MTUsOS45NjIgQzczLjcxNSw5LjU2MCA3My4yODYsOS4zOTkgNzIuOTY1LDkuMzQ1IFpNOTcuNjUwLDkuMzQ1IEw5MC45MjUsOC4zNjcgTDg3LjkxMCwyLjI3MiBDODcuNzkwLDIuMDE3IDg3LjU2MiwxLjcyMiA4Ny4yNTQsMS43MjIgQzg2Ljk0NiwxLjcyMiA4Ni43MTgsMi4wMTcgODYuNTk4LDIuMjcyIEw4My41ODMsOC4zNjcgTDc2Ljg1OCw5LjM0NSBDNzYuNTIzLDkuMzk5IDc2LjEwNyw5LjU2MCA3Ni4xMDcsOS45NjIgQzc2LjEwNywxMC4yMDMgNzYuMjgyLDEwLjQzMSA3Ni40NDIsMTAuNjA1IEw4MS4zMTksMTUuMzQ3IEw4MC4xNjcsMjIuMDQ2IEM4MC4xNTMsMjIuMTQwIDgwLjE0MCwyMi4yMjAgODAuMTQwLDIyLjMxNCBDODAuMTQwLDIyLjY2MiA4MC4zMTQsMjIuOTg0IDgwLjcwMywyMi45ODQgQzgwLjg5MCwyMi45ODQgODEuMDY0LDIyLjkxNyA4MS4yMzksMjIuODIzIEw4Ny4yNTQsMTkuNjYxIEw5My4yNjksMjIuODIzIEM5My40MzAsMjIuOTE3IDkzLjYxOCwyMi45ODQgOTMuODA1LDIyLjk4NCBDOTQuMTk0LDIyLjk4NCA5NC4zNTQsMjIuNjYyIDk0LjM1NCwyMi4zMTQgQzk0LjM1NCwyMi4yMjAgOTQuMzU0LDIyLjE0MCA5NC4zNDEsMjIuMDQ2IEw5My4xODksMTUuMzQ3IEw5OC4wNTIsMTAuNjA1IEM5OC4yMjYsMTAuNDMxIDk4LjQwMCwxMC4yMDMgOTguNDAwLDkuOTYyIEM5OC40MDAsOS41NjAgOTcuOTcyLDkuMzk5IDk3LjY1MCw5LjM0NSBaTTEyMi4zMzYsOS4zNDUgTDExNS42MTAsOC4zNjcgTDExMi41OTYsMi4yNzIgQzExMi40NzUsMi4wMTcgMTEyLjI0OCwxLjcyMiAxMTEuOTM5LDEuNzIyIEMxMTEuNjMxLDEuNzIyIDExMS40MDMsMi4wMTcgMTExLjI4MywyLjI3MiBMMTA4LjI2OSw4LjM2NyBMMTAxLjU0Myw5LjM0NSBDMTAxLjIwOCw5LjM5OSAxMDAuNzkzLDkuNTYwIDEwMC43OTMsOS45NjIgQzEwMC43OTMsMTAuMjAzIDEwMC45NjcsMTAuNDMxIDEwMS4xMjgsMTAuNjA1IEwxMDYuMDA0LDE1LjM0NyBMMTA0Ljg1MiwyMi4wNDYgQzEwNC44MzksMjIuMTQwIDEwNC44MjUsMjIuMjIwIDEwNC44MjUsMjIuMzE0IEMxMDQuODI1LDIyLjY2MiAxMDUuMDAwLDIyLjk4NCAxMDUuMzg4LDIyLjk4NCBDMTA1LjU3NiwyMi45ODQgMTA1Ljc1MCwyMi45MTcgMTA1LjkyNCwyMi44MjMgTDExMS45MzksMTkuNjYxIEwxMTcuOTU1LDIyLjgyMyBDMTE4LjExNSwyMi45MTcgMTE4LjMwMywyMi45ODQgMTE4LjQ5MSwyMi45ODQgQzExOC44NzksMjIuOTg0IDExOS4wNDAsMjIuNjYyIDExOS4wNDAsMjIuMzE0IEMxMTkuMDQwLDIyLjIyMCAxMTkuMDQwLDIyLjE0MCAxMTkuMDI2LDIyLjA0NiBMMTE3Ljg3NCwxNS4zNDcgTDEyMi43MzgsMTAuNjA1IEMxMjIuOTEyLDEwLjQzMSAxMjMuMDg2LDEwLjIwMyAxMjMuMDg2LDkuOTYyIEMxMjMuMDg2LDkuNTYwIDEyMi42NTcsOS4zOTkgMTIyLjMzNiw5LjM0NSBaIiAvPgo8L3N2Zz4K);
}
.rating.rating--dark .rating__overlay {
  background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWlkWU1pZCIgdmlld0JveD0iMCAwIDEyNSAyNSI+CiAgPHBhdGggZmlsbD0iIzAwMTQzQiIgZD0iTTAuMDAwLDI1LjAwMCBMMC4wMDAsLTAuMDAwIEwxMjUuMDAwLC0wLjAwMCBMMTI1LjAwMCwyNS4wMDAgTDAuMDAwLDI1LjAwMCBaTTIzLjU5NCw5LjM0NSBMMTYuODY5LDguMzY3IEwxMy44NTQsMi4yNzIgQzEzLjczNCwyLjAxNyAxMy41MDYsMS43MjIgMTMuMTk4LDEuNzIyIEMxMi44OTAsMS43MjIgMTIuNjYyLDIuMDE3IDEyLjU0MSwyLjI3MiBMOS41MjcsOC4zNjcgTDIuODAyLDkuMzQ1IEMyLjQ2Nyw5LjM5OSAyLjA1MSw5LjU2MCAyLjA1MSw5Ljk2MiBDMi4wNTEsMTAuMjAzIDIuMjI1LDEwLjQzMSAyLjM4NiwxMC42MDUgTDcuMjYzLDE1LjM0NyBMNi4xMTEsMjIuMDQ2IEM2LjA5NywyMi4xNDAgNi4wODQsMjIuMjIwIDYuMDg0LDIyLjMxNCBDNi4wODQsMjIuNjYyIDYuMjU4LDIyLjk4NCA2LjY0NywyMi45ODQgQzYuODM0LDIyLjk4NCA3LjAwOCwyMi45MTcgNy4xODIsMjIuODIzIEwxMy4xOTgsMTkuNjYxIEwxOS4yMTMsMjIuODIzIEMxOS4zNzQsMjIuOTE3IDE5LjU2MSwyMi45ODQgMTkuNzQ5LDIyLjk4NCBDMjAuMTM4LDIyLjk4NCAyMC4yOTgsMjIuNjYyIDIwLjI5OCwyMi4zMTQgQzIwLjI5OCwyMi4yMjAgMjAuMjk4LDIyLjE0MCAyMC4yODUsMjIuMDQ2IEwxOS4xMzMsMTUuMzQ3IEwyMy45OTYsMTAuNjA1IEMyNC4xNzAsMTAuNDMxIDI0LjM0NCwxMC4yMDMgMjQuMzQ0LDkuOTYyIEMyNC4zNDQsOS41NjAgMjMuOTE2LDkuMzk5IDIzLjU5NCw5LjM0NSBaTTQ4LjI3OSw5LjM0NSBMNDEuNTU0LDguMzY3IEwzOC41NDAsMi4yNzIgQzM4LjQxOSwyLjAxNyAzOC4xOTEsMS43MjIgMzcuODgzLDEuNzIyIEMzNy41NzUsMS43MjIgMzcuMzQ3LDIuMDE3IDM3LjIyNywyLjI3MiBMMzQuMjEyLDguMzY3IEwyNy40ODcsOS4zNDUgQzI3LjE1Miw5LjM5OSAyNi43MzcsOS41NjAgMjYuNzM3LDkuOTYyIEMyNi43MzcsMTAuMjAzIDI2LjkxMSwxMC40MzEgMjcuMDcyLDEwLjYwNSBMMzEuOTQ4LDE1LjM0NyBMMzAuNzk2LDIyLjA0NiBDMzAuNzgzLDIyLjE0MCAzMC43NjksMjIuMjIwIDMwLjc2OSwyMi4zMTQgQzMwLjc2OSwyMi42NjIgMzAuOTQzLDIyLjk4NCAzMS4zMzIsMjIuOTg0IEMzMS41MTksMjIuOTg0IDMxLjY5NCwyMi45MTcgMzEuODY4LDIyLjgyMyBMMzcuODgzLDE5LjY2MSBMNDMuODk5LDIyLjgyMyBDNDQuMDU5LDIyLjkxNyA0NC4yNDcsMjIuOTg0IDQ0LjQzNCwyMi45ODQgQzQ0LjgyMywyMi45ODQgNDQuOTg0LDIyLjY2MiA0NC45ODQsMjIuMzE0IEM0NC45ODQsMjIuMjIwIDQ0Ljk4NCwyMi4xNDAgNDQuOTcwLDIyLjA0NiBMNDMuODE4LDE1LjM0NyBMNDguNjgxLDEwLjYwNSBDNDguODU1LDEwLjQzMSA0OS4wMzAsMTAuMjAzIDQ5LjAzMCw5Ljk2MiBDNDkuMDMwLDkuNTYwIDQ4LjYwMSw5LjM5OSA0OC4yNzksOS4zNDUgWk03Mi45NjUsOS4zNDUgTDY2LjIzOSw4LjM2NyBMNjMuMjI1LDIuMjcyIEM2My4xMDQsMi4wMTcgNjIuODc3LDEuNzIyIDYyLjU2OSwxLjcyMiBDNjIuMjYwLDEuNzIyIDYyLjAzMywyLjAxNyA2MS45MTIsMi4yNzIgTDU4Ljg5OCw4LjM2NyBMNTIuMTcyLDkuMzQ1IEM1MS44MzcsOS4zOTkgNTEuNDIyLDkuNTYwIDUxLjQyMiw5Ljk2MiBDNTEuNDIyLDEwLjIwMyA1MS41OTYsMTAuNDMxIDUxLjc1NywxMC42MDUgTDU2LjYzNCwxNS4zNDcgTDU1LjQ4MSwyMi4wNDYgQzU1LjQ2OCwyMi4xNDAgNTUuNDU1LDIyLjIyMCA1NS40NTUsMjIuMzE0IEM1NS40NTUsMjIuNjYyIDU1LjYyOSwyMi45ODQgNTYuMDE3LDIyLjk4NCBDNTYuMjA1LDIyLjk4NCA1Ni4zNzksMjIuOTE3IDU2LjU1MywyMi44MjMgTDYyLjU2OSwxOS42NjEgTDY4LjU4NCwyMi44MjMgQzY4Ljc0NSwyMi45MTcgNjguOTMyLDIyLjk4NCA2OS4xMjAsMjIuOTg0IEM2OS41MDgsMjIuOTg0IDY5LjY2OSwyMi42NjIgNjkuNjY5LDIyLjMxNCBDNjkuNjY5LDIyLjIyMCA2OS42NjksMjIuMTQwIDY5LjY1NiwyMi4wNDYgTDY4LjUwNCwxNS4zNDcgTDczLjM2NywxMC42MDUgQzczLjU0MSwxMC40MzEgNzMuNzE1LDEwLjIwMyA3My43MTUsOS45NjIgQzczLjcxNSw5LjU2MCA3My4yODYsOS4zOTkgNzIuOTY1LDkuMzQ1IFpNOTcuNjUwLDkuMzQ1IEw5MC45MjUsOC4zNjcgTDg3LjkxMCwyLjI3MiBDODcuNzkwLDIuMDE3IDg3LjU2MiwxLjcyMiA4Ny4yNTQsMS43MjIgQzg2Ljk0NiwxLjcyMiA4Ni43MTgsMi4wMTcgODYuNTk4LDIuMjcyIEw4My41ODMsOC4zNjcgTDc2Ljg1OCw5LjM0NSBDNzYuNTIzLDkuMzk5IDc2LjEwNyw5LjU2MCA3Ni4xMDcsOS45NjIgQzc2LjEwNywxMC4yMDMgNzYuMjgyLDEwLjQzMSA3Ni40NDIsMTAuNjA1IEw4MS4zMTksMTUuMzQ3IEw4MC4xNjcsMjIuMDQ2IEM4MC4xNTMsMjIuMTQwIDgwLjE0MCwyMi4yMjAgODAuMTQwLDIyLjMxNCBDODAuMTQwLDIyLjY2MiA4MC4zMTQsMjIuOTg0IDgwLjcwMywyMi45ODQgQzgwLjg5MCwyMi45ODQgODEuMDY0LDIyLjkxNyA4MS4yMzksMjIuODIzIEw4Ny4yNTQsMTkuNjYxIEw5My4yNjksMjIuODIzIEM5My40MzAsMjIuOTE3IDkzLjYxOCwyMi45ODQgOTMuODA1LDIyLjk4NCBDOTQuMTk0LDIyLjk4NCA5NC4zNTQsMjIuNjYyIDk0LjM1NCwyMi4zMTQgQzk0LjM1NCwyMi4yMjAgOTQuMzU0LDIyLjE0MCA5NC4zNDEsMjIuMDQ2IEw5My4xODksMTUuMzQ3IEw5OC4wNTIsMTAuNjA1IEM5OC4yMjYsMTAuNDMxIDk4LjQwMCwxMC4yMDMgOTguNDAwLDkuOTYyIEM5OC40MDAsOS41NjAgOTcuOTcyLDkuMzk5IDk3LjY1MCw5LjM0NSBaTTEyMi4zMzYsOS4zNDUgTDExNS42MTAsOC4zNjcgTDExMi41OTYsMi4yNzIgQzExMi40NzUsMi4wMTcgMTEyLjI0OCwxLjcyMiAxMTEuOTM5LDEuNzIyIEMxMTEuNjMxLDEuNzIyIDExMS40MDMsMi4wMTcgMTExLjI4MywyLjI3MiBMMTA4LjI2OSw4LjM2NyBMMTAxLjU0Myw5LjM0NSBDMTAxLjIwOCw5LjM5OSAxMDAuNzkzLDkuNTYwIDEwMC43OTMsOS45NjIgQzEwMC43OTMsMTAuMjAzIDEwMC45NjcsMTAuNDMxIDEwMS4xMjgsMTAuNjA1IEwxMDYuMDA0LDE1LjM0NyBMMTA0Ljg1MiwyMi4wNDYgQzEwNC44MzksMjIuMTQwIDEwNC44MjUsMjIuMjIwIDEwNC44MjUsMjIuMzE0IEMxMDQuODI1LDIyLjY2MiAxMDUuMDAwLDIyLjk4NCAxMDUuMzg4LDIyLjk4NCBDMTA1LjU3NiwyMi45ODQgMTA1Ljc1MCwyMi45MTcgMTA1LjkyNCwyMi44MjMgTDExMS45MzksMTkuNjYxIEwxMTcuOTU1LDIyLjgyMyBDMTE4LjExNSwyMi45MTcgMTE4LjMwMywyMi45ODQgMTE4LjQ5MSwyMi45ODQgQzExOC44NzksMjIuOTg0IDExOS4wNDAsMjIuNjYyIDExOS4wNDAsMjIuMzE0IEMxMTkuMDQwLDIyLjIyMCAxMTkuMDQwLDIyLjE0MCAxMTkuMDI2LDIyLjA0NiBMMTE3Ljg3NCwxNS4zNDcgTDEyMi43MzgsMTAuNjA1IEMxMjIuOTEyLDEwLjQzMSAxMjMuMDg2LDEwLjIwMyAxMjMuMDg2LDkuOTYyIEMxMjMuMDg2LDkuNTYwIDEyMi42NTcsOS4zOTkgMTIyLjMzNiw5LjM0NSBaIiAvPgo8L3N2Zz4K);
}

.review {
  display: block;
}
.review__name {
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1em;
  line-height: 1.2;
  text-transform: uppercase;
}
.review__date {
  display: block;
  font-family: "solitas_norm_light", sans-serif;
  text-transform: none;
}
.review__text {
  font-family: "solitas_norm_light", sans-serif;
  font-size: 1.0625em;
  color: #55647f;
}

.review__list .review {
  margin-top: 1.25em;
}

.founder__image {
  width: 37.5%;
}
.founder__content {
  padding-top: 0.625em;
}
.founder__name {
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.5em;
  text-transform: uppercase;
}
.founder__desi {
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 1em;
  letter-spacing: 0.3125em;
  text-transform: uppercase;
  color: #67c9d3;
}
.founder__desc {
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1em;
  line-height: 2em;
}
.founder__desc p {
  margin-bottom: 0;
}
.founder__list {
  margin-top: -1.25em;
}
.founder__list:after {
  display: table;
  clear: both;
  content: " ";
}
.founder__list .founder__item {
  margin-top: 1.25em;
}
@media screen and (min-width: 80em) {
  .founder__list .founder__item {
    width: 49.15254%;
    float: left;
    margin-right: 1.69492%;
  }
  .founder__list .founder__item:nth-child(2n) {
    margin-right: 0;
  }
  .founder__list .founder__item:nth-child(2n+1) {
    clear: left;
  }
}

.investor__list {
  display: block;
  text-align: center;
}
.investor__item {
  position: relative;
  display: inline-block;
  width: 12.5em;
  height: 12.5em;
  -webkit-border-radius: 12.5em;
  border-radius: 12.5em;
  overflow: hidden;
  background-color: #ffffff;
}
.investor__item:not(:last-child) {
  margin-right: 1.69492%;
}
.investor__item img {
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}

.faq__category {
  margin-bottom: 1.875em;
}
.faq__category__heading {
  display: block;
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.5em;
  font-weight: normal;
  line-height: 2.66667em;
  letter-spacing: 0.02em;
  text-transform: uppercase;
  color: #67c9d3;
}
.faq__category__heading:not(:last-child) {
  margin-bottom: 1.25em;
}
.faq__category .faq__question__item {
  position: relative;
  margin-bottom: 1.25em;
  padding-bottom: 1.25em;
  border-bottom: 1px solid #e1e9f3;
}
.faq__category .faq__question__item .toggle {
  position: absolute;
  right: 0;
  top: 8px;
  z-index: 2000;
  cursor: pointer;
  width: 20px;
  height: 20px;
  padding: 0;
  background-color: rgba(0, 21, 59, 0);
  color: #cfdbeb;
  border: none;
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
  -webkit-transform-origin: center center;
  transform-origin: center center;
  -webkit-transition: transform 0.3s linear;
  transition: transform 0.3s linear;
}
.faq__category .faq__question__item .toggle:focus, .faq__category .faq__question__item .toggle:hover {
  outline: none;
  color: #00153b;
}
.faq__category .faq__question__item .toggle.has--close {
  -webkit-transform: rotate(0);
  transform: rotate(0);
  color: #d03e2a;
}
.faq__category .faq__question__item .toggle:before, .faq__category .faq__question__item .toggle:after {
  display: block;
  content: " ";
  position: absolute;
  top: 0;
  left: 9px;
  width: 4px;
  height: 22px;
  border-radius: 1px;
  background: currentColor;
  -webkit-transform-origin: center center;
  transform-origin: center center;
  -webkit-transition: all 0.3s linear;
  transition: all 0.3s linear;
}
.faq__category .faq__question__item .toggle:before {
  -webkit-transform: rotate(135deg);
  transform: rotate(135deg);
}
.faq__category .faq__question__item .toggle:after {
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
}
.faq__category .faq__question__item .question {
  display: block;
  margin: 0;
  padding: 0;
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 1.25em;
  font-weight: normal;
  line-height: 1.8em;
  letter-spacing: 0.02em;
}
.faq__category .faq__question__item .answer {
  display: none;
  margin-top: 0.625em;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1em;
  font-weight: normal;
  line-height: 1.77778em;
  letter-spacing: 0.02em;
}

.cancellation__tab .tab__nav a.button, .cancellation__tab .tab__nav .img-uploader a.upload-label, .img-uploader .cancellation__tab .tab__nav a.upload-label {
  width: auto;
  background-color: #ffffff;
  color: #00153b;
}
.cancellation__tab .tab__nav a.button.current, .cancellation__tab .tab__nav .img-uploader a.current.upload-label, .img-uploader .cancellation__tab .tab__nav a.current.upload-label {
  background-color: #0599b2;
  color: #ffffff;
}
@media screen and (max-width: 47.9375em) {
  .cancellation__tab .tab__nav {
    padding: 0.625em 0;
  }
}

.error__block {
  position: relative;
  display: block;
  text-align: center;
  overflow: hidden;
  background-color: #67c9d3;
  color: #ffffff;
}
.error__block__inner {
  display: table;
  width: 100%;
  height: 62.5em;
  height: 100vh;
}
.error__block .circle {
  position: absolute;
  display: block;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  display: block;
  -webkit-border-radius: 100%;
  border-radius: 100%;
  background-color: currentColor;
  opacity: 0.1;
}
.error__block .circle:nth-child(1) {
  width: 100%;
  padding-bottom: 100%;
}
.error__block .circle:nth-child(2) {
  width: 80%;
  padding-bottom: 80%;
}
.error__block .circle:nth-child(3) {
  width: 50%;
  padding-bottom: 50%;
}
.error__block__content {
  position: relative;
  display: table-cell;
  vertical-align: middle;
  width: 100%;
  height: 100%;
  z-index: 5;
}
.error__block .error__image svg {
  display: inline-block;
  width: 100%;
  height: 100%;
  max-width: 50em;
  max-height: 22.8125em;
  fill: currentColor;
}
@media screen and (-webkit-min-device-pixel-ratio: 0) {
  .error__block .error__image svg {
    /* Safari 5+ ONLY */
  }
  .error__block .error__image svg ::i-block-chrome, .error__block .error__image svg {
    width: 50em;
    height: 22.8125em;
  }
}
.error__block .error__heading {
  margin: 0;
  padding: 0;
  font-family: "solitas_ext_demi", sans-serif;
  font-size: 27.5em;
  font-size: 20vw;
  line-height: 1;
}
@media screen and (max-width: 101.1875em) {
  .error__block .error__heading {
    font-size: 13.75em;
    font-size: 25vw;
  }
}
@media screen and (max-width: 79.9375em) {
  .error__block .error__heading {
    font-size: 6.875em;
    font-size: 30vw;
  }
}
.error__block .error__sub-heading {
  font-family: "solitas_ext_light", sans-serif;
  font-size: 1.5em;
  line-height: 1;
  letter-spacing: 0.3125em;
  text-transform: uppercase;
}
@media screen and (max-width: 79.9375em) {
  .error__block .error__sub-heading {
    font-size: 1.25em;
  }
}
@media screen and (max-width: 63.9375em) {
  .error__block .error__sub-heading {
    font-size: 1.0625em;
  }
}

@media screen and (min-width: 80em) {
  .search__section .form .field-set {
    margin-bottom: 0;
  }
}

.island__section .floated__island-map {
  position: absolute;
  top: -2.5em;
  right: 0;
}

.escapade__section .escapade__container {
  display: block;
  background-color: #ffffff;
  padding: 0.625em;
}
@media screen and (min-width: 48em) {
  .escapade__section .escapade__container {
    padding: 2.5em;
  }
}

.seen__section {
  background-color: #67c9d3;
  color: #ffffff;
}

.promotion__section {
  min-height: 25em !important;
  background-color: #67c9d3;
  background-position: center center;
  background-size: cover;
  background-repeat: no-repeat;
  color: #ffffff;
}
.promotion__section .promotion__content .heading-text {
  font-family: "solitas_ext_book", sans-serif;
  font-size: 3em;
}
.promotion__section .promotion__action {
  display: block;
  margin-top: 1.875em;
}
@media screen and (max-width: 47.9375em) {
  .promotion__section {
    min-height: inherit !important;
  }
  .promotion__section .promotion__content .heading-text {
    font-size: 1.5em;
  }
}

.book-card {
  position: relative;
  background-color: #f3f6fa;
}
.book-card__inner {
  display: block;
  padding: 1.25em;
}
.book-card__heading {
  margin: 0;
  padding: 0;
  margin-bottom: 1em;
  font-size: 1.25em;
  text-align: center;
  text-transform: uppercase;
}
.book-card__calendar {
  margin-left: -1.25em;
  margin-right: -1.25em;
}
.book-card__calendar .calendar__actions {
  padding-left: 1.875em;
  padding-right: 1.875em;
}
.book-card__calendar .calendar__dates td {
  background-color: #e1e9f3;
}
.book-card__participant {
  display: block;
  margin-left: -1.25em;
  margin-right: -1.25em;
  padding: 1.25em;
  background-color: #e1e9f3;
}
.book-card__participant__heading {
  margin: 0;
  padding: 0;
  margin-bottom: 0.625em;
  font-size: 1em;
  text-transform: uppercase;
}
.book-card__participant__form {
  position: relative;
  display: block;
  margin: 0;
  padding: 0;
  border-width: 0;
  /*font-family: _font(family, norm-medium);
  font-size: em(24px);*/
}
.book-card__participant__form input[type="text"] {
  min-height: calc(2.08333em + 2px);
  line-height: 2.08333em;
  text-align: center;
  background-color: transparent;
  border-width: 0;
  border-bottom: 4px solid #ffffff;
}
.book-card__participant__form a {
  position: absolute;
  top: 50%;
  -webkit-transform: translate(0, -50%);
  transform: translate(0, -50%);
  display: block;
  width: 1.5em;
  height: 1.5em;
  -webkit-border-radius: 1.5em;
  border-radius: 1.5em;
  line-height: 1.5em;
  text-align: center;
  background-color: #ffffff;
}
.book-card__participant__form a.fl--left {
  left: 0;
}
.book-card__participant__form a.fl--right {
  right: 0;
}
.book-card__price {
  display: block;
  padding: 1.25em 0;
}
.book-card__price__heading {
  padding: 0;
  margin: 0;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 3em;
  line-height: 1.2;
}
.book-card__price__heading sup {
  font-size: 0.5em;
}

.highlights__block {
  padding-bottom: 5em;
}

.additional-info__block .additional-info__list {
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1em;
  line-height: 2em;
}
.additional-info__block .additional-info__list li ul li:first-child {
  font-family: "solitas_norm_medium", sans-serif;
  font-weight: normal;
  white-space: nowrap;
  text-transform: uppercase;
}
.additional-info__block .additional-info__list > li:nth-child(1) {
  color: #76b043;
}
.additional-info__block .additional-info__list > li:nth-child(2) {
  color: #d03e2a;
}
.additional-info__block .additional-info__list > li:nth-child(3) {
  color: #0599b2;
}

.review__block {
  border-left: 1px solid #e1e9f3;
}

.gallery__block .gallery__tab .tab__nav {
  margin-bottom: 1.875em;
}
.gallery__block .gallery__tab .tab__nav li:not(:last-child) {
  margin-right: 3.75em;
}
.gallery__block .gallery__tab .tab__nav a:not(.current) {
  opacity: 0.3;
}
.gallery__block .gallery__carousel .carousel__item {
  padding-right: 1.25em;
}
.gallery__block .gallery__carousel.slick-slider,
.gallery__block .gallery__carousel .slick-list {
  overflow: visible;
}
.gallery__block .gallery__carousel .slick-dots {
  bottom: -2.5em;
  text-align: left;
}
.gallery__block .gallery__video-iframe {
  position: relative;
  display: block;
  width: 100%;
  padding-bottom: 56%;
}
.gallery__block .gallery__video-iframe iframe {
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
}

.host-activities__block .heading-text.heading-text--small {
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1em;
  text-transform: uppercase;
}
.host-activities__block .gallery .gallery__item {
  max-width: 31.81818%;
}

@media screen and (min-width: 80em) {
  .host__block .media .media__figure {
    width: 41.17647%;
    text-align: center;
  }

  .host-activities__block .gallery {
    padding-right: 9.48276%;
  }
}
@media screen and (max-width: 79.9375em) {
  .info__block:not(.no--style) {
    padding-bottom: 1.875em;
    margin-bottom: 1.875em;
    border-bottom: 1px solid rgba(0, 21, 59, 0.1);
  }

  .highlights__block,
  .additional-info__block {
    padding-bottom: 2.5em;
  }

  .review__block {
    border-left-width: 0;
  }

  .host-activities__block {
    margin-top: 1.25em;
  }
}
@media screen and (max-width: 47.9375em) {
  .highlights__block {
    padding-bottom: 1.25em;
  }

  .additional-info__block .additional-info__list, .additional-info__block .additional-info__list li {
    display: block;
  }
}
.section__listing {
  padding: 2.5em 0;
}
.section__listing .listing__filter {
  margin-bottom: 1.875em;
}
.section__listing .listing__filter .filter__heading {
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.125em;
  line-height: 1.2;
  text-transform: uppercase;
  margin-bottom: 1.25em;
}
.section__listing .listing__filter .filter__block:not(:last-child) {
  padding-bottom: 0.625em;
  margin-bottom: 1.875em;
  border-bottom: 1px solid #e1e9f3;
}
.section__listing .listing__filter .filter__block .list.list--horizontal li {
  padding-right: 1.875em;
}
.section__listing .listing__filter-button {
  display: inline-block;
  width: 100%;
  padding: 0 1.42857em;
  line-height: 3.57143em;
  min-height: calc(3.57143em + 2px);
  min-width: 9.28571em;
  border-radius: 0;
  font-size: 0.875em;
  color: inherit;
  background-color: #fff;
  border: 1px solid #e3e9f0;
  transition: border 0.3s linear, background 0.3s linear;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  text-transform: uppercase;
  background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI0OCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB3aWR0aD0iNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTE0IDIwbDEwIDEwIDEwLTEweiIvPjxwYXRoIGQ9Ik0wIDBoNDh2NDhoLTQ4eiIgZmlsbD0ibm9uZSIvPjwvc3ZnPg==);
  background-repeat: no-repeat;
  background-position: 95% 50%;
  background-size: 24px;
}
.section__listing .listing__filter-button::-ms-expand {
  display: none;
}
.section__listing .listing__heading {
  padding-bottom: 1.25em;
  margin-bottom: 1.25em;
  border-bottom: 1px solid #e1e9f3;
}

.order__section .summary__block {
  padding: 1.25em;
  margin-bottom: 1.25em;
  background-color: #e1e9f3;
}
.order__section .summary__block .summary__total {
  border-top: 1px solid #bdcee4;
  margin-top: 0.83125em;
  padding-top: 0.83125em;
  font-family: "solitas_norm_medium", sans-serif;
}
.order__section .booking__block {
  padding: 1.25em;
  background-color: #ffffff;
}
.order__section .booking__block .block__heading-text {
  margin-bottom: 1.875em;
}
.order__section .booking__block .booking-media__image {
  width: 13.7931%;
}
.order__section .booking__block .booking-media__heading {
  margin-bottom: 0.625em;
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 1.25em;
  line-height: 1.2;
}
.order__section .booking__block .booking-media__text {
  display: block;
  width: 100%;
  font-size: 1em;
  line-height: 1.2;
  margin: 0;
  padding: 0;
  color: #666666;
}
.order__section .booking__block .booking-media__addon .tag {
  margin-right: 4px;
}
.order__section .booking__block .booking-media__addon .tag .tag__remove {
  font-family: "solitas_norm_medium", sans-serif;
  color: #d03e2a;
}
@media screen and (min-width: 48em) {
  .order__section .booking__block .booking-media__content {
    padding-left: 1.25em;
  }
}
.order__section .booking__block .booking-media__list {
  margin-top: -1.25em;
}
.order__section .booking__block .booking-media__list:after {
  display: table;
  clear: both;
  content: " ";
}
.order__section .booking__block .booking-media__list .booking-media {
  margin-top: 1.25em;
  padding-bottom: 1.25em;
}
.order__section .booking__block .booking-media__list .booking-media:not(:last-child) {
  border-bottom: 1px solid #e1e9f3;
}
@media screen and (max-width: 63.9375em) {
  .order__section .span {
    width: 100%;
  }
}

.section__login .disclaimer,
.section__join .disclaimer,
.section__forgot .disclaimer {
  display: block;
  padding: 0.625em;
  text-align: center;
}

@media screen and (min-width: 48em) {
  .section__login,
  .section__join,
  .section__forgot {
    position: relative;
  }
  .section__login .disclaimer,
  .section__join .disclaimer,
  .section__forgot .disclaimer {
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    display: block;
    height: 2.5em;
    line-height: 1.25em;
    text-align: center;
  }
}
.cart__item .item__heading {
  font-family: "solitas_norm_regular", sans-serif;
  font-size: 1.25em;
  line-height: 1.2;
  margin-bottom: 0.5em;
}
.cart__item .item__cat {
  display: inline-block;
  width: 100%;
  font-size: 0.875em;
  line-height: 1.2;
  letter-spacing: 0.3125em;
  text-transform: uppercase;
  color: #d03e2a;
}
.cart__item .item__addon .tag {
  margin-right: 4px;
}
.cart__item .item__addon .tag .tag__remove {
  font-family: "solitas_norm_medium", sans-serif;
  color: #d03e2a;
}
.cart__item .item__action .button, .cart__item .item__action .img-uploader .upload-label, .img-uploader .cart__item .item__action .upload-label {
  font-size: 0.6875em;
}
.cart__item .option__heading {
  display: block;
  margin-bottom: 0.625em;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1em;
  line-height: 1.2;
}
.cart__item .option__action {
  display: inline-block;
  width: 100%;
  max-width: 7.5em;
}
.cart__item .option__action select {
  min-width: inherit;
  max-width: inherit;
}
.cart__item .total__cal {
  display: inline-block;
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1em;
  line-height: 1.2;
  text-transform: uppercase;
}
.cart__item .total__amt {
  display: block;
  font-family: "solitas_norm_medium", sans-serif;
  font-size: 1.375em;
  line-height: 1.2;
}
.cart__item:hover .item__action .button:first-child, .cart__item:hover .item__action .img-uploader .upload-label:first-child, .img-uploader .cart__item:hover .item__action .upload-label:first-child {
  background-color: #67c9d3;
  color: #ffffff;
}
.cart__item:hover .item__action .button:last-child, .cart__item:hover .item__action .img-uploader .upload-label:last-child, .img-uploader .cart__item:hover .item__action .upload-label:last-child {
  background-color: #d03e2a;
  color: #ffffff;
}
.cart__item:hover .item__action .button:hover, .cart__item:hover .item__action .img-uploader .upload-label:hover, .img-uploader .cart__item:hover .item__action .upload-label:hover {
  background-color: #00153b;
}

.cart__list .cart__item {
  padding-bottom: 1.875em;
  margin-bottom: 1.875em;
  border-bottom: 1px solid #e1e9f3;
}

.cart__section .section__header, .payment__section .section__header {
  font-family: "solitas_norm_book", sans-serif;
  font-size: 1em;
  line-height: 1.2;
  text-transform: uppercase;
}
.cart__section .promocode__block,
.cart__section .summary__block, .payment__section .promocode__block,
.payment__section .summary__block {
  padding-top: 1.875em;
  margin-top: 1.875em;
  border-top: 1px solid #e1e9f3;
}
.cart__section .promocode__block .block__heading-text,
.cart__section .summary__block .block__heading-text, .payment__section .promocode__block .block__heading-text,
.payment__section .summary__block .block__heading-text {
  font-size: 1.375em;
}
@media screen and (min-width: 48em) {
  .cart__section .promocode__block:last-child,
  .cart__section .summary__block:last-child, .payment__section .promocode__block:last-child,
  .payment__section .summary__block:last-child {
    padding-left: 6.77966%;
    border-left: 1px solid #e1e9f3;
  }
  .cart__section .promocode__block:first-child,
  .cart__section .summary__block:first-child, .payment__section .promocode__block:first-child,
  .payment__section .summary__block:first-child {
    padding-right: 6.77966%;
  }
}
.cart__section .summary__block, .payment__section .summary__block {
  text-transform: uppercase;
}
.cart__section .summary__block .summary__total, .payment__section .summary__block .summary__total {
  padding-top: 1.25em;
  margin-top: 1.25em;
  margin-bottom: 1.25em;
  border-top: 1px solid #e1e9f3;
}
.cart__section .summary__block .summary__total:after, .payment__section .summary__block .summary__total:after {
  display: table;
  clear: both;
  content: " ";
}

.payment__section .summary__block {
  padding: 1.25em;
  margin-bottom: 1.25em;
  background-color: #e1e9f3;
}
.payment__section .summary__block .summary__total {
  border-top: 1px solid #bdcee4;
  margin-bottom: 0;
}
@media screen and (max-width: 63.9375em) {
  .payment__section .span {
    width: 100%;
  }
}

.is--home .site-header .visible-on--mobile,
.is--home .site-header .visible-on--tablet,
.is--home .site-header .visible-on--desktop {
  display: none !important;
}
@media screen and (min-width: 48em) {
  .is--home .site-header .visible-on--tablet {
    display: inline-block !important;
  }
  .is--home .site-main {
    padding-top: 0 !important;
  }
  .is--home .site-main__header__content {
    padding-top: 6.5625em;
  }
  .is--home .site-main__header__content .section.section--vcenter {
    padding-top: 3.75em;
    padding-bottom: 3.75em;
  }
}
@media screen and (min-width: 64em) {
  .is--home .site-main__header__content {
    height: 48em;
    height: 100vh;
  }
  .is--home .site-main__header__content .section.section--vcenter {
    padding-bottom: 0;
  }
}
@media screen and (min-width: 80em) {
  .is--home .site-header .h__brand,
  .is--home .site-header .h__navigation {
    height: 10.625em;
    line-height: 10.625em;
  }
  .is--home .site-header .visible-on--desktop {
    display: inline-block !important;
  }
  .is--home .site-header .visible-on--tablet {
    display: none !important;
  }
  .is--home .site-header .h__brand {
    overflow: hidden;
    white-space: nowrap;
  }
  .is--home .site-header .h__brand .h__logo {
    margin-left: -6.875em;
  }
  .is--home .site-header .h__brand-main {
    -webkit-transform: translate(0, 0);
    transform: translate(0, 0);
  }
  .is--home.is--bottom .site-header.site-header--transparent {
    background-color: #ffffff;
    color: #00153b;
  }
  .is--home.is--bottom .site-header .h__brand,
  .is--home.is--bottom .site-header .h__navigation {
    height: 5.3125em;
    line-height: 5.3125em;
  }
  .is--home.is--bottom .site-header .h__brand .h__logo {
    margin-left: 0;
  }
  .is--home.is--bottom .site-header .h__brand-main {
    -webkit-transform: translate(0, -150%);
    transform: translate(0, -150%);
  }
}
@media screen and (max-width: 79.9375em) {
  .is--home .site-main__body .span {
    width: 100%;
  }
}
@media screen and (max-width: 47.9375em) {
  .is--home .site-header .visible-on--mobile {
    display: inline-block !important;
  }
}

.is--detail .site-main__header__content .heading-text {
  font-family: "solitas_ext_medium", sans-serif;
}
.is--detail .site-main__header__content .regular-text {
  font-family: "solitas_ext_light", sans-serif;
}
@media screen and (min-width: 48em) {
  .is--detail .site-main {
    padding-top: 0;
  }
  .is--detail .site-main__header__content {
    padding-top: 6.5625em;
  }
  .is--detail .site-main__header__content .section.section--vcenter {
    padding-bottom: 10em;
  }
}
@media screen and (min-width: 64em) {
  .is--detail .site-main__header__content {
    height: 48em;
    height: 100vh;
  }
}
@media screen and (max-width: 79.9375em) {
  .is--detail .site-main__body .span {
    width: 100%;
  }
}

@media screen and (min-width: 48em) {
  .is--island .site-main {
    padding-top: 0;
  }
  .is--island .site-main__header__content {
    padding-top: 6.5625em;
  }
  .is--island .site-main__header__content .section.section--vcenter {
    padding-bottom: 10em;
  }
}
@media screen and (min-width: 64em) {
  .is--island .site-main__header__content {
    height: 48em;
    height: 100vh;
  }
}
@media screen and (max-width: 79.9375em) {
  .is--island .site-main__body .span {
    width: 100%;
  }
}

@media screen and (min-width: 48em) {
  .is--guestuser .site-main,
  .is--login .site-main,
  .is--join .site-main,
  .is--forgot .site-main,
  .is--404 .site-main {
    padding-top: 0;
  }
  .is--guestuser .section.section--vcenter,
  .is--login .section.section--vcenter,
  .is--join .section.section--vcenter,
  .is--forgot .section.section--vcenter,
  .is--404 .section.section--vcenter {
    min-height: 51.25em;
    height: 51.25em;
    height: 100vh;
  }
}
.is--dashboard .site-main .section {
  padding-top: 0;
}
.is--dashboard .site-main .section .section__header {
  padding-top: 1.25em;
  padding-bottom: 1.25em;
}

@media screen and (min-width: 64em) {
  .is--cms .site-main .site-main__header .site-main__header__image + .site-main__header__content {
    height: 31.25em;
  }
}

@media screen and (min-width: 64em) {
  .is--contact .site-main__header__image .img, .is--contactus .site-main__header__image .img {
    max-height: 31.25em;
  }
  .is--contact .site-main__header__content, .is--contactus .site-main__header__content {
    height: 31.25em;
  }
}

.is--review .review__list > li:not(:last-child) .review {
  margin-bottom: 2.5em;
  padding-bottom: 2.5em;
  border-bottom: 1px solid #cfdbeb;
}

/* Slider */
/*.slick-list {
    .slick-loading & {
        background: #fff slick-image-url("ajax-loader.gif") center center no-repeat;
    }
}*/
/* Icons */
/*@if $slick-font-family == "slick" {
    @font-face {
        font-family: "slick";
        src: slick-font-url("slick.eot");
        src: slick-font-url("slick.eot?#iefix") format("embedded-opentype"), slick-font-url("slick.woff") format("woff"), slick-font-url("slick.ttf") format("truetype"), slick-font-url("slick.svg#slick") format("svg");
        font-weight: normal;
        font-style: normal;
    }
}*/
/* Arrows */
/*.slick-prev,
.slick-next {
    position: relative;
    display: inline-block;
    line-height: 20px;
    font-size: 16px;
    text-align: center;
    cursor: pointer;
    background: transparent;
    color: transparent;
    top: 50%;
    margin-top: -10px\9; lte IE 8
    -webkit-transform: translate(0, -50%);
    -ms-transform: translate(0, -50%);
    transform: translate(0, -50%);
    padding: 10px;
    border: $slick-arrow-border;
    outline: none;
    opacity: 0.8;
    -webkit-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
    &:hover, &:focus {
        outline: none;
        color: transparent;
        opacity: 1;
        &:before {
            opacity: $slick-opacity-on-hover;
        }
    }
    &.slick-disabled:before {
        opacity: $slick-opacity-not-active;
    }
}

.slick-prev:before, .slick-next:before {
    font-family: $slick-font-family;
    color: $slick-arrow-color;
    opacity: $slick-opacity-default;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

.slick-prev {
    left: -50px;
    [dir="rtl"] & {
        left: auto;
        right: -50px;
    }
    &:before {
        content: $slick-prev-character;
        [dir="rtl"] & {
            content: $slick-next-character;
        }
    }
}

.slick-next {
    right: -50px;
    [dir="rtl"] & {
        left: -50px;
        right: auto;
    }
    &:before {
        content: $slick-next-character;
        [dir="rtl"] & {
            content: $slick-prev-character;
        }
    }
}
.slick-slider:hover .slick-prev{
    left: 15px;
    [dir="rtl"] & {left: auto; right: 15px;}
}
.slick-slider:hover .slick-next{
    right: 15px;
    [dir="rtl"] & {right: auto; left: 15px;}
}*/
.slick-prev,
.slick-next {
  position: absolute;
  bottom: 0;
  z-index: 100;
  display: inline-block;
  vertical-align: middle;
  width: 30px;
  height: 15px;
  -webkit-transform-origin: center center;
  transform-origin: center center;
  background: none;
  border-width: 0;
  text-indent: 9999;
  font-size: 0;
}
.slick-prev:before,
.slick-next:before {
  position: absolute;
  top: 50%;
  left: 0;
  -webkit-transform: translate(0, -100%);
  transform: translate(0, -100%);
  width: 100%;
  height: 1px;
  background-color: currentcolor;
  content: " ";
}
.slick-prev:after,
.slick-next:after {
  position: absolute;
  top: 50%;
  right: 0;
  width: 30%;
  height: 60%;
  -webkit-transform: rotate(45deg) translate(0, -50%);
  transform: rotate(45deg) translate(0, -50%);
  -webkit-transform-origin: top center;
  transform-origin: top center;
  content: " ";
  border-top: 1px solid currentcolor;
  border-right: 1px solid currentcolor;
}

.slick-prev {
  left: 50%;
  -webkit-transform: rotate(180deg) translateX(200%);
  transform: rotate(180deg) translateX(200%);
}
.slick-prev:before {
  -webkit-transform: translate(0, 0);
  transform: translate(0, 0);
}

.slick-next {
  right: 50%;
  -webkit-transform: rotate(0deg) translateX(200%);
  transform: rotate(0deg) translateX(200%);
}

/* Dots */
.slick-dots {
  position: absolute;
  z-index: 99;
  bottom: 0;
  list-style: none;
  display: block;
  text-align: center;
  padding: 0;
  width: 100%;
}
.slick-dots li {
  position: relative;
  display: inline-block;
  height: 15px;
  width: 15px;
  margin: 0 3px;
  padding: 0;
  cursor: pointer;
}
.slick-dots li button {
  display: block;
  width: 1em;
  height: 1em;
  -webkit-border-radius: 100%;
  border-radius: 100%;
  padding: 0;
  text-indent: 9999;
  background-color: #ffffff;
  border: 2px solid #cfdbeb;
  color: transparent;
  cursor: pointer;
}
.slick-dots li button:hover {
  background-color: #cfdbeb;
}
.slick-dots li.slick-active button {
  background-color: #cfdbeb;
}

/* Slider */
.slick-slider {
  position: relative;
  display: block;
  overflow: hidden;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -ms-touch-action: pan-y;
  touch-action: pan-y;
  -webkit-tap-highlight-color: transparent;
}

.slick-list {
  position: relative;
  overflow: hidden;
  display: block;
  margin: 0;
  padding: 0;
}
.slick-list:focus {
  outline: none;
}
.slick-list.dragging {
  cursor: pointer;
  cursor: hand;
}

.slick-slider .slick-track,
.slick-slider .slick-list {
  -webkit-transform: translate3d(0, 0, 0);
  -moz-transform: translate3d(0, 0, 0);
  -ms-transform: translate3d(0, 0, 0);
  -o-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0);
}

.slick-track {
  position: relative;
  left: 0;
  top: 0;
  display: block;
}
.slick-track:before, .slick-track:after {
  content: "";
  display: table;
}
.slick-track:after {
  clear: both;
}
.slick-loading .slick-track {
  visibility: hidden;
}

.slick-slide {
  float: left;
  height: 100%;
  min-height: 1px;
  outline: none;
  display: none;
}
[dir="rtl"] .slick-slide {
  float: right;
}
.slick-slide img {
  display: inline-block;
}
.slick-slide.slick-loading img {
  display: none;
}
.slick-slide.dragging img {
  pointer-events: none;
}
.slick-initialized .slick-slide {
  display: block;
}
.slick-loading .slick-slide {
  visibility: hidden;
}
.slick-vertical .slick-slide {
  display: block;
  height: auto;
  border: 1px solid transparent;
}

.slick-arrow.slick-hidden {
  display: none;
}

/*!
	Modaal - accessible modals - v0.2.9
	by Humaan, for all humans.
	http://humaan.com
 */
.modaal-accessible-hide {
  position: absolute !important;
  clip: rect(1px 1px 1px 1px);
  /* IE6, IE7 */
  clip: rect(1px, 1px, 1px, 1px);
  padding: 0 !important;
  border: 0 !important;
  height: 1px !important;
  width: 1px !important;
  overflow: hidden;
}

.modaal-overlay {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 6000;
  opacity: 0;
}

.modaal-wrapper {
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 7000;
  overflow: auto;
  opacity: 1;
  box-sizing: border-box;
  transition: all 0.3s ease-in-out;
    /*& .modaal-container {
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		-webkit-backface-visibility: hidden;
	}*/
}
.modaal-wrapper * {
  box-sizing: border-box;
}
.modaal-wrapper .modaal-close {
  background: transparent;
  padding: 0;
  -webkit-appearance: none;
}
.modaal-wrapper.modaal-start_none {
  display: none;
  opacity: 1;
}
.modaal-wrapper.modaal-start_fade {
  opacity: 0;
}
.modaal-wrapper *[tabindex="0"] {
  outline: none !important;
}
.modaal-wrapper.modaal-fullscreen {
  overflow: hidden;
}
.modaal-wrapper.modaal-fullscreen .modaal-content-container {
  padding: 0;
}

.modaal-outer-wrapper {
  display: table;
  position: relative;
  width: 100%;
  height: 100%;
}
.modaal-fullscreen .modaal-outer-wrapper {
  display: block;
}

.modaal-inner-wrapper {
  display: table-cell;
  width: 100%;
  height: 100%;
  position: relative;
  vertical-align: middle;
  text-align: center;
  padding: 80px 25px;
}
.modaal-fullscreen .modaal-inner-wrapper {
  padding: 0;
  display: block;
  vertical-align: top;
}

.modaal-container {
  position: relative;
  display: inline-block;
  width: 100%;
  margin: auto;
  text-align: left;
  color: #00153b;
  max-width: 62.5em;
  border-radius: 0px;
  background: #f3f6fa;
  box-shadow: 0 4px 15px rgba(0, 21, 59, 0.2);
  cursor: auto;
}
.modaal-container.is_loading {
  height: 100px;
  width: 100px;
  overflow: hidden;
}
.modaal-fullscreen .modaal-container {
  max-width: none;
  height: 100%;
  overflow: auto;
}

.modaal-close {
  position: fixed;
  right: 20px;
  top: 20px;
  z-index: 7001;
  color: #f3f6fa;
  cursor: pointer;
  opacity: 1;
  width: 58px;
  height: 58px;
  border: 4px solid transparent;
  background: transparent;
  border-radius: 100%;
  transition: all 0.2s ease-in-out;
}
.modaal-close:focus, .modaal-close:hover {
  outline: none;
  border: 4px solid #ffffff;
  /*&:before,
  &:after { background: #b93d0c; }*/
}
.modaal-close span {
  position: absolute !important;
  clip: rect(1px 1px 1px 1px);
  /* IE6, IE7 */
  clip: rect(1px, 1px, 1px, 1px);
  padding: 0 !important;
  border: 0 !important;
  height: 1px !important;
  width: 1px !important;
  overflow: hidden;
}
.modaal-close:before, .modaal-close:after {
  display: block;
  content: " ";
  position: absolute;
  top: 14px;
  left: 23px;
  width: 4px;
  height: 22px;
  border-radius: 1px;
  background: #ffffff;
  transition: background 0.2s ease-in-out;
}
.modaal-close:before {
  -webkit-transform: rotate(-45deg);
  transform: rotate(-45deg);
}
.modaal-close:after {
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
}
.modaal-fullscreen .modaal-close {
  background: #bdcee4;
  right: 30px;
  top: 10px;
}

.modaal-content-container {
  padding: 0;
}

.modaal-confirm-wrap {
  padding: 30px 0 0;
  text-align: center;
  font-size: 0;
}

.modaal-confirm-btn {
  font-size: 14px;
  display: inline-block;
  margin: 0 10px;
  vertical-align: middle;
  cursor: pointer;
  border: none;
  background: transparent;
}
.modaal-confirm-btn.modaal-ok {
  padding: 10px 15px;
  color: #f3f6fa;
  background: #67c9d3;
  border-radius: 3px;
  transition: background 0.2s ease-in-out;
}
.modaal-confirm-btn.modaal-ok:hover {
  background: #35acb8;
}
.modaal-confirm-btn.modaal-cancel {
  text-decoration: underline;
}
.modaal-confirm-btn.modaal-cancel:hover {
  text-decoration: none;
  color: #35acb8;
}

@keyframes instaReveal {
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}
@-o-keyframes instaReveal {
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}
@-moz-keyframes instaReveal {
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}
@-webkit-keyframes instaReveal {
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}
@-ms-keyframes instaReveal {
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}
.modaal-instagram .modaal-container {
  width: auto;
  background: transparent;
  box-shadow: none !important;
}
.modaal-instagram .modaal-content-container {
  padding: 0;
  background: transparent;
}
.modaal-instagram .modaal-content-container > blockquote {
  width: 1px !important;
  height: 1px !important;
  opacity: 0 !important;
}
.modaal-instagram iframe {
  opacity: 0;
  margin: -6px !important;
  border-radius: 0 !important;
  width: 1000px !important;
  max-width: 800px !important;
  box-shadow: none !important;
  animation: instaReveal 1s linear forwards;
}

.modaal-image .modaal-container {
  width: auto;
  max-width: 1500px;
}

.modaal-gallery-wrap {
  position: relative;
  color: #f3f6fa;
}

.modaal-gallery-item {
  display: none;
}
.modaal-gallery-item img {
  display: block;
}
.modaal-gallery-item.is_active {
  display: block;
}

.modaal-gallery-label {
  position: absolute;
  left: 0;
  width: 100%;
  margin: 20px 0 0;
  font-size: 18px;
  text-align: center;
  color: #ffffff;
}
.modaal-gallery-label:focus {
  outline: none;
}

.modaal-gallery-control {
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  opacity: 1;
  cursor: pointer;
  color: #f3f6fa;
  width: 50px;
  height: 50px;
  background: transparent;
  border: none;
  border-radius: 100%;
  transition: all 0.2s ease-in-out;
}
.modaal-gallery-control.is_hidden {
  opacity: 0;
  cursor: default;
}
.modaal-gallery-control:focus, .modaal-gallery-control:hover {
  outline: none;
  background: #ffffff;
}
.modaal-gallery-control:focus:before, .modaal-gallery-control:focus:after, .modaal-gallery-control:hover:before, .modaal-gallery-control:hover:after {
  background: #bdcee4;
}
.modaal-gallery-control span {
  position: absolute !important;
  clip: rect(1px 1px 1px 1px);
  /* IE6, IE7 */
  clip: rect(1px, 1px, 1px, 1px);
  padding: 0 !important;
  border: 0 !important;
  height: 1px !important;
  width: 1px !important;
  overflow: hidden;
}
.modaal-gallery-control:before, .modaal-gallery-control:after {
  display: block;
  content: " ";
  position: absolute;
  top: 16px;
  left: 25px;
  width: 4px;
  height: 18px;
  border-radius: 4px;
  background: #ffffff;
  transition: background 0.2s ease-in-out;
}
.modaal-gallery-control:before {
  margin: -5px 0 0;
  transform: rotate(-45deg);
}
.modaal-gallery-control:after {
  margin: 5px 0 0;
  transform: rotate(45deg);
}

.modaal-gallery-next {
  left: 100%;
  margin-left: 40px;
}

.modaal-gallery-prev {
  right: 100%;
  margin-right: 40px;
}
.modaal-gallery-prev:before, .modaal-gallery-prev:after {
  left: 22px;
}
.modaal-gallery-prev:before {
  margin: 5px 0 0;
  transform: rotate(-45deg);
}
.modaal-gallery-prev:after {
  margin: -5px 0 0;
  transform: rotate(45deg);
}

.modaal-video-wrap {
  margin: auto 50px;
  position: relative;
}

.modaal-video-container {
  position: relative;
  padding-bottom: 56.25%;
  height: 0;
  overflow: hidden;
  max-width: 100%;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
  background: #000;
  max-width: 1300px;
  margin-left: auto;
  margin-right: auto;
}
.modaal-video-container iframe,
.modaal-video-container object,
.modaal-video-container embed {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}

.modaal-iframe .modaal-container {
  width: auto;
  max-width: none;
}

.modaal-iframe-elem {
  width: 100%;
  display: block;
}

@media only screen and (min-width: 1400px) {
  .modaal-video-container {
    padding-bottom: 0;
    height: 731px;
  }
}
@media only screen and (max-width: 1140px) {
  .modaal-gallery-item img {
    width: 100%;
  }

  .modaal-gallery-control {
    top: auto;
    bottom: 20px;
    transform: none;
    background: rgba(0, 0, 0, 0.7);
  }
  .modaal-gallery-control:before, .modaal-gallery-control:after {
    background: #ffffff;
  }

  .modaal-gallery-next {
    left: auto;
    right: 20px;
  }

  .modaal-gallery-prev {
    left: 20px;
    right: auto;
  }
}
@media screen and (max-width: 900px) {
  .modaal-instagram iframe {
    width: 500px !important;
  }
}
@media screen and (max-height: 1100px) {
  .modaal-instagram iframe {
    width: 700px !important;
  }
}
@media screen and (max-height: 1000px) {
  .modaal-inner-wrapper {
    padding-top: 60px;
    padding-bottom: 60px;
  }

  .modaal-instagram iframe {
    width: 600px !important;
  }
}
@media screen and (max-height: 900px) {
  .modaal-instagram iframe {
    width: 500px !important;
  }

  .modaal-video-container {
    max-width: 900px;
    max-height: 510px;
  }
}
@media only screen and (max-width: 600px) {
  .modaal-instagram iframe {
    width: 280px !important;
  }
}
@media only screen and (max-height: 1024px) {
  .modaal-gallery-item img {
    width: auto !important;
    max-height: 85vh;
  }
}
@media only screen and (max-height: 820px) {
  .modaal-gallery-label {
    display: none;
  }
}
.modaal-loading-spinner {
  background: none;
  position: absolute;
  width: 200px;
  height: 200px;
  top: 50%;
  left: 50%;
  margin: -100px 0 0 -100px;
  transform: scale(0.25);
}

@-ms-keyframes modaal-loading-spinner {
  0% {
    opacity: 1;
    -ms-transform: scale(1.5);
    -moz-transform: scale(1.5);
    -webkit-transform: scale(1.5);
    -o-transform: scale(1.5);
    transform: scale(1.5);
  }
  100% {
    opacity: .1;
    -ms-transform: scale(1);
    -moz-transform: scale(1);
    -webkit-transform: scale(1);
    -o-transform: scale(1);
    transform: scale(1);
  }
}
@-moz-keyframes modaal-loading-spinner {
  0% {
    opacity: 1;
    -ms-transform: scale(1.5);
    -moz-transform: scale(1.5);
    -webkit-transform: scale(1.5);
    -o-transform: scale(1.5);
    transform: scale(1.5);
  }
  100% {
    opacity: .1;
    -ms-transform: scale(1);
    -moz-transform: scale(1);
    -webkit-transform: scale(1);
    -o-transform: scale(1);
    transform: scale(1);
  }
}
@-webkit-keyframes modaal-loading-spinner {
  0% {
    opacity: 1;
    -ms-transform: scale(1.5);
    -moz-transform: scale(1.5);
    -webkit-transform: scale(1.5);
    -o-transform: scale(1.5);
    transform: scale(1.5);
  }
  100% {
    opacity: .1;
    -ms-transform: scale(1);
    -moz-transform: scale(1);
    -webkit-transform: scale(1);
    -o-transform: scale(1);
    transform: scale(1);
  }
}
@-o-keyframes modaal-loading-spinner {
  0% {
    opacity: 1;
    -ms-transform: scale(1.5);
    -moz-transform: scale(1.5);
    -webkit-transform: scale(1.5);
    -o-transform: scale(1.5);
    transform: scale(1.5);
  }
  100% {
    opacity: .1;
    -ms-transform: scale(1);
    -moz-transform: scale(1);
    -webkit-transform: scale(1);
    -o-transform: scale(1);
    transform: scale(1);
  }
}
@keyframes modaal-loading-spinner {
  0% {
    opacity: 1;
    -ms-transform: scale(1.5);
    -moz-transform: scale(1.5);
    -webkit-transform: scale(1.5);
    -o-transform: scale(1.5);
    transform: scale(1.5);
  }
  100% {
    opacity: .1;
    -ms-transform: scale(1);
    -moz-transform: scale(1);
    -webkit-transform: scale(1);
    -o-transform: scale(1);
    transform: scale(1);
  }
}
.modaal-loading-spinner > div {
  width: 24px;
  height: 24px;
  margin-left: 4px;
  margin-top: 4px;
  position: absolute;
}

.modaal-loading-spinner > div > div {
  width: 100%;
  height: 100%;
  border-radius: 15px;
  background: #ffffff;
}

.modaal-loading-spinner > div:nth-of-type(1) > div {
  -ms-animation: modaal-loading-spinner 1s linear infinite;
  -moz-animation: modaal-loading-spinner 1s linear infinite;
  -webkit-animation: modaal-loading-spinner 1s linear infinite;
  -o-animation: modaal-loading-spinner 1s linear infinite;
  animation: modaal-loading-spinner 1s linear infinite;
  -ms-animation-delay: 0s;
  -moz-animation-delay: 0s;
  -webkit-animation-delay: 0s;
  -o-animation-delay: 0s;
  animation-delay: 0s;
}

.modaal-loading-spinner > div:nth-of-type(2) > div, .modaal-loading-spinner > div:nth-of-type(3) > div {
  -ms-animation: modaal-loading-spinner 1s linear infinite;
  -moz-animation: modaal-loading-spinner 1s linear infinite;
  -webkit-animation: modaal-loading-spinner 1s linear infinite;
  -o-animation: modaal-loading-spinner 1s linear infinite;
}

.modaal-loading-spinner > div:nth-of-type(1) {
  -ms-transform: translate(84px, 84px) rotate(45deg) translate(70px, 0);
  -moz-transform: translate(84px, 84px) rotate(45deg) translate(70px, 0);
  -webkit-transform: translate(84px, 84px) rotate(45deg) translate(70px, 0);
  -o-transform: translate(84px, 84px) rotate(45deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(45deg) translate(70px, 0);
}

.modaal-loading-spinner > div:nth-of-type(2) > div {
  animation: modaal-loading-spinner 1s linear infinite;
  -ms-animation-delay: .12s;
  -moz-animation-delay: .12s;
  -webkit-animation-delay: .12s;
  -o-animation-delay: .12s;
  animation-delay: .12s;
}

.modaal-loading-spinner > div:nth-of-type(2) {
  -ms-transform: translate(84px, 84px) rotate(90deg) translate(70px, 0);
  -moz-transform: translate(84px, 84px) rotate(90deg) translate(70px, 0);
  -webkit-transform: translate(84px, 84px) rotate(90deg) translate(70px, 0);
  -o-transform: translate(84px, 84px) rotate(90deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(90deg) translate(70px, 0);
}

.modaal-loading-spinner > div:nth-of-type(3) > div {
  animation: modaal-loading-spinner 1s linear infinite;
  -ms-animation-delay: .25s;
  -moz-animation-delay: .25s;
  -webkit-animation-delay: .25s;
  -o-animation-delay: .25s;
  animation-delay: .25s;
}

.modaal-loading-spinner > div:nth-of-type(4) > div, .modaal-loading-spinner > div:nth-of-type(5) > div {
  -ms-animation: modaal-loading-spinner 1s linear infinite;
  -moz-animation: modaal-loading-spinner 1s linear infinite;
  -webkit-animation: modaal-loading-spinner 1s linear infinite;
  -o-animation: modaal-loading-spinner 1s linear infinite;
}

.modaal-loading-spinner > div:nth-of-type(3) {
  -ms-transform: translate(84px, 84px) rotate(135deg) translate(70px, 0);
  -moz-transform: translate(84px, 84px) rotate(135deg) translate(70px, 0);
  -webkit-transform: translate(84px, 84px) rotate(135deg) translate(70px, 0);
  -o-transform: translate(84px, 84px) rotate(135deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(135deg) translate(70px, 0);
}

.modaal-loading-spinner > div:nth-of-type(4) > div {
  animation: modaal-loading-spinner 1s linear infinite;
  -ms-animation-delay: .37s;
  -moz-animation-delay: .37s;
  -webkit-animation-delay: .37s;
  -o-animation-delay: .37s;
  animation-delay: .37s;
}

.modaal-loading-spinner > div:nth-of-type(4) {
  -ms-transform: translate(84px, 84px) rotate(180deg) translate(70px, 0);
  -moz-transform: translate(84px, 84px) rotate(180deg) translate(70px, 0);
  -webkit-transform: translate(84px, 84px) rotate(180deg) translate(70px, 0);
  -o-transform: translate(84px, 84px) rotate(180deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(180deg) translate(70px, 0);
}

.modaal-loading-spinner > div:nth-of-type(5) > div {
  animation: modaal-loading-spinner 1s linear infinite;
  -ms-animation-delay: .5s;
  -moz-animation-delay: .5s;
  -webkit-animation-delay: .5s;
  -o-animation-delay: .5s;
  animation-delay: .5s;
}

.modaal-loading-spinner > div:nth-of-type(6) > div, .modaal-loading-spinner > div:nth-of-type(7) > div {
  -ms-animation: modaal-loading-spinner 1s linear infinite;
  -moz-animation: modaal-loading-spinner 1s linear infinite;
  -webkit-animation: modaal-loading-spinner 1s linear infinite;
  -o-animation: modaal-loading-spinner 1s linear infinite;
}

.modaal-loading-spinner > div:nth-of-type(5) {
  -ms-transform: translate(84px, 84px) rotate(225deg) translate(70px, 0);
  -moz-transform: translate(84px, 84px) rotate(225deg) translate(70px, 0);
  -webkit-transform: translate(84px, 84px) rotate(225deg) translate(70px, 0);
  -o-transform: translate(84px, 84px) rotate(225deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(225deg) translate(70px, 0);
}

.modaal-loading-spinner > div:nth-of-type(6) > div {
  animation: modaal-loading-spinner 1s linear infinite;
  -ms-animation-delay: .62s;
  -moz-animation-delay: .62s;
  -webkit-animation-delay: .62s;
  -o-animation-delay: .62s;
  animation-delay: .62s;
}

.modaal-loading-spinner > div:nth-of-type(6) {
  -ms-transform: translate(84px, 84px) rotate(270deg) translate(70px, 0);
  -moz-transform: translate(84px, 84px) rotate(270deg) translate(70px, 0);
  -webkit-transform: translate(84px, 84px) rotate(270deg) translate(70px, 0);
  -o-transform: translate(84px, 84px) rotate(270deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(270deg) translate(70px, 0);
}

.modaal-loading-spinner > div:nth-of-type(7) > div {
  animation: modaal-loading-spinner 1s linear infinite;
  -ms-animation-delay: .75s;
  -moz-animation-delay: .75s;
  -webkit-animation-delay: .75s;
  -o-animation-delay: .75s;
  animation-delay: .75s;
}

.modaal-loading-spinner > div:nth-of-type(7) {
  -ms-transform: translate(84px, 84px) rotate(315deg) translate(70px, 0);
  -moz-transform: translate(84px, 84px) rotate(315deg) translate(70px, 0);
  -webkit-transform: translate(84px, 84px) rotate(315deg) translate(70px, 0);
  -o-transform: translate(84px, 84px) rotate(315deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(315deg) translate(70px, 0);
}

.modaal-loading-spinner > div:nth-of-type(8) > div {
  -ms-animation: modaal-loading-spinner 1s linear infinite;
  -moz-animation: modaal-loading-spinner 1s linear infinite;
  -webkit-animation: modaal-loading-spinner 1s linear infinite;
  -o-animation: modaal-loading-spinner 1s linear infinite;
  animation: modaal-loading-spinner 1s linear infinite;
  -ms-animation-delay: .87s;
  -moz-animation-delay: .87s;
  -webkit-animation-delay: .87s;
  -o-animation-delay: .87s;
  animation-delay: .87s;
}

.modaal-loading-spinner > div:nth-of-type(8) {
  -ms-transform: translate(84px, 84px) rotate(360deg) translate(70px, 0);
  -moz-transform: translate(84px, 84px) rotate(360deg) translate(70px, 0);
  -webkit-transform: translate(84px, 84px) rotate(360deg) translate(70px, 0);
  -o-transform: translate(84px, 84px) rotate(360deg) translate(70px, 0);
  transform: translate(84px, 84px) rotate(360deg) translate(70px, 0);
}

/*# sourceMappingURL=main.css.map */
