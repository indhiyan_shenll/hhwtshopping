<main id="MAIN" class="site-main site-main--dark with--sidebar">
            <div class="site-main__body">
                 <?php require_once(dirname(dirname(__FILE__)).'/_partial/sub-header.php')?>
              
                <section class="section">
                    <div class="container container--static">
					<header class="section__header section-header--bordered">
                        <h6 class="header__heading-text fl--left"><?php echo Info::t_lang('NOTIFICATIONS')?></h6>
                    </header>
                    <div class="section__body">
                        <div class="container container--fluid">
                            <div class="span__row">
                                <div class="span span--12 message-list">
                               
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </section>
            </div>
        </main>