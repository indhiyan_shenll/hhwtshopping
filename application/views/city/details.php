<script src='https://api.mapbox.com/mapbox.js/v2.4.0/mapbox.js'></script>
<?php
defined('SYSTEM_INIT') or die('Invalid Usage.');
 $cityId = $cityInfo['city_id'];
?>

<!-- Wrapper -->

<main id="MAIN" class="site-main site-main--light city-page">
    <div class="container hhwt-single-category">
        <div class="col-md-6 col-sm-6 col-xs-12 hhwt-destinations-category">
            <p><a href="javascript:void(0)">Home</a><span class="fa fa-angle-right icon-right"></span><a href="javascript:void(0)">Destinations</a><span class="fa fa-angle-right icon-right"></span><a href="javascript:void(0)">Tokyo</a></p>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <ul class="city-experience-menu">
                <li><a href="javascript:void(0)">About Tokyo</a></li>
                <li><a href="javascript:void(0)">Top Experiences</a></li>
                <li><a href="javascript:void(0)">Latest Experiences</a></li>
            </ul>
        </div>
    </div>
    <div class="container-fluid city-detail-fluid">
        <div class="container city-detail-container">
            <h1 class="city-welcome-title">Welcome to Tokyo!</h1>
            <p class="city-welcome-note">Tradition collides with pop culture in Tokyo, where you can reverently wander ancient temples before catching the eye-opening fashion trends in Harajuku. Wake up before the sun to catch the lively fish auction at the Tsukiji Market, then refresh with a walk beneath the cherry blossom trees that line the Sumida River. Don't forget to eat as much sushi, udon noodles, and wagashi (Japanese sweets) as your belly can handle.</p>
            <select class="form-control hhwt-exp-type" name="hhwt-exp-type">
                <option value="">Select Experiences' Category</option>
                <option value="top">Top Experiences</option>
                <option value="latest">Latest Experiences</option>
            </select>
        </div>
    </div>
    <!-- top slider -->
    <div class="container-fluid hhwt-destination-fluid">
        <div class="container hhwt-destination-container">
            <h4 class="col-sm-10 col-xs-12 hhwt-destination-title">Top Experiences in Tokyo</h4>
        </div>
        <div id="hhwt-post" class="carousel slide" data-ride="carousel">
            <div class="custom-container carousel-inner" role="listbox">
                <div class="item active">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">11 Days Tokyo-Kyoto with Trekking in Jap</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$300</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>80 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/01.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">Beyond Tokyo:Roof of Japan Experience</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$90</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>91 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/02.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Tokyo Minakami Onsan F&E</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>70 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/03.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Basic Tokyo Mt Fuji & Disney Fun</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>102 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/04.png" class="img-responsive thumb">
                        </article>
                    </div>
                </div>
                <div class="item">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">11 Days Tokyo-Kyoto with Trekking in Jap</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$300</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>80 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/01.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">Beyond Tokyo:Roof of Japan Experience</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$90</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>91 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/02.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Tokyo Minakami Onsan F&E</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>70 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/03.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Basic Tokyo Mt Fuji & Disney Fun</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>102 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/04.png" class="img-responsive thumb">
                        </article>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#hhwt-post" role="button" data-slide="prev">
                <span class="fa fa-chevron-left" aria-hidden="true"></span>
            </a>
            <a class="right carousel-control" href="#hhwt-post" role="button" data-slide="next">
                <span class="fa fa-chevron-right" aria-hidden="true"></span>
            </a>
        </div>
    </div>
    <!-- latest slider -->
    <div class="container-fluid hhwt-destination-fluid">
        <div class="container hhwt-destination-container">
            <h4 class="col-sm-10 col-xs-12 hhwt-destination-title">Latest Experiences in Tokyo</h4>
        </div>
        <div id="hhwt-post-1" class="carousel slide" data-ride="carousel">
            <div class="custom-container carousel-inner" role="listbox">
                <div class="item active">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">11 Days Tokyo-Kyoto with Trekking in Jap</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$300</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>80 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/01.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">Beyond Tokyo:Roof of Japan Experience</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$90</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>91 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/02.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Tokyo Minakami Onsan F&E</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>70 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/03.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Basic Tokyo Mt Fuji & Disney Fun</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>102 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/04.png" class="img-responsive thumb">
                        </article>
                    </div>
                </div>
                <div class="item">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">11 Days Tokyo-Kyoto with Trekking in Jap</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$300</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>80 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/01.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">Beyond Tokyo:Roof of Japan Experience</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$90</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>91 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/02.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Tokyo Minakami Onsan F&E</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>70 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/03.png" class="img-responsive thumb">
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-3 hidden-xs">
                        <article class="halal-post simple-small">
                            <header>
                                <span class="category"><a href="javascript:void(0)">Tokyo</a><i class="fa fa-heart-o heart-icons" aria-hidden="true"></i></span>
                                <h3><a href="javascript:void(0)">5 Days Basic Tokyo Mt Fuji & Disney Fun</a></h3>
                                <div class="col-md-12 col-sm-12 col-xs-12 hhwt-review-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-points">
                                        <h3>S$100</h3>
                                        <p>perperson</p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 hhwt-reviews">
                                        <img src="../images/star.png">
                                        <p>102 reviews</p>
                                    </div>
                                </div>
                            </header>
                            <img src="../images/04.png" class="img-responsive thumb">
                        </article>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#hhwt-post-1" role="button" data-slide="prev">
                <span class="fa fa-chevron-left" aria-hidden="true"></span>
            </a>
            <a class="right carousel-control" href="#hhwt-post-1" role="button" data-slide="next">
                <span class="fa fa-chevron-right" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</main>


<script>
    var city_id = <?php echo FatUtility::int($selectedcityId) ?>;
</script>  


