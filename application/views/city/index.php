<?php defined('SYSTEM_INIT') or die('Invalid Usage.'); ?>

<!-- Wrapper -->
<main id="MAIN" class="site-main site-main--light">
    <div class="container hhwt-single-category">
        <div class="col-md-6 col-sm-6 col-xs-12 hhwt-destinations-category">
            <p><a href="javascript:void(0)">Home</a><span class="fa fa-angle-right icon-right"></span><a href="javascript:void(0)">Destinations</a></p>
            <h3><a href="javascript:void(0)">Destinations</a></h3>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 hhwt-search hhwt-search-category">
            <div class="search-box input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search Destinations...">
            </div>
        </div>
    </div>

    <div class="container-fluid hhwt-destinations-fluid">
        <div class="container hhwt-destination-container">
            <div class="col-md-12 col-sm-12 col-xs-12 hhwt-dest-head">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <article class="modern-medium">
                        <img src="../images/1.png" class="img-responsive">
                        <header>
                            <p><a href="javascript:void(0)">Australia</a></p>
                            <h3><a href="javascript:void(0)">Melbourne</a></h3>
                        </header>
                    </article>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <article class="modern-medium">
                        <img src="../images/2.png" class="img-responsive">
                        <header>
                            <p><a href="javascript:void(0)">Australia</a></p>
                            <h3><a href="javascript:void(0)">Melbourne</a></h3>
                        </header>
                    </article>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <article class="modern-medium">
                        <img src="../images/3.png" class="img-responsive">
                        <header>
                            <p><a href="javascript:void(0)">Australia</a></p>
                            <h3><a href="javascript:void(0)">Melbourne</a></h3>
                        </header>
                    </article>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <article class="modern-medium">
                        <img src="../images/4.png" class="img-responsive">
                        <header>
                            <p><a href="javascript:void(0)">Australia</a></p>
                            <h3><a href="javascript:void(0)">Melbourne</a></h3>
                        </header>
                    </article>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 hhwt-dest-head">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <article class="modern-medium">
                        <img src="../images/1.png" class="img-responsive">
                        <header>
                            <p><a href="javascript:void(0)">Australia</a></p>
                            <h3><a href="javascript:void(0)">Melbourne</a></h3>
                        </header>
                    </article>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <article class="modern-medium">
                        <img src="../images/2.png" class="img-responsive">
                        <header>
                            <p><a href="javascript:void(0)">Australia</a></p>
                            <h3><a href="javascript:void(0)">Melbourne</a></h3>
                        </header>
                    </article>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <article class="modern-medium">
                        <img src="../images/3.png" class="img-responsive">
                        <header>
                            <p><a href="javascript:void(0)">Australia</a></p>
                            <h3><a href="javascript:void(0)">Melbourne</a></h3>
                        </header>
                    </article>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <article class="modern-medium">
                        <img src="../images/4.png" class="img-responsive">
                        <header>
                            <p><a href="javascript:void(0)">Australia</a></p>
                            <h3><a href="javascript:void(0)">Melbourne</a></h3>
                        </header>
                    </article>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 hhwt-dest-head">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <article class="modern-medium">
                        <img src="../images/1.png" class="img-responsive">
                        <header>
                            <p><a href="javascript:void(0)">Australia</a></p>
                            <h3><a href="javascript:void(0)">Melbourne</a></h3>
                        </header>
                    </article>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <article class="modern-medium">
                        <img src="../images/2.png" class="img-responsive">
                        <header>
                            <p><a href="javascript:void(0)">Australia</a></p>
                            <h3><a href="javascript:void(0)">Melbourne</a></h3>
                        </header>
                    </article>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <article class="modern-medium">
                        <img src="../images/3.png" class="img-responsive">
                        <header>
                            <p><a href="javascript:void(0)">Australia</a></p>
                            <h3><a href="javascript:void(0)">Melbourne</a></h3>
                        </header>
                    </article>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <article class="modern-medium">
                        <img src="../images/4.png" class="img-responsive">
                        <header>
                            <p><a href="javascript:void(0)">Australia</a></p>
                            <h3><a href="javascript:void(0)">Melbourne</a></h3>
                        </header>
                    </article>
                </div>
            </div>
        </div>
    </div>
</main>