(function() {
	login = function(frm, v) {
		v.validate();
		if (!v.isValid()) return false;
		jsonStrictNotifyMessage();
		fcom.ajax(fcom.makeUrl('GuestUser', 'login'),fcom.frmData(frm), function(ajaxRes) {

			json = $.parseJSON(ajaxRes);
			if(json.status == '1') {
				localStorage.setItem("currentUser", JSON.stringify(json.sharedUserDetails));
				jsonSuccessMessage(json.msg);
				location.reload();
			} else {				
				jsonErrorMessage(json.msg);
			}
		});
	};
})();