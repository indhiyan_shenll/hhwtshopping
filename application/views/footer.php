<?php
defined('SYSTEM_INIT') or die('Invalid Usage.');
if ($include_custom_css_js) { 
?>
<footer>
    <div class="container-fluid hhwt-footer-fluid">
        <div class="container hhwt-footer-container">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 hhwt-footer-categories">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 hhwt-footer-grid">
                    <h4 class="titles">Cities</h4>
                    <ul class="links">
                        <li ><a href="javascript:void(0)">Link</a></li>
                        <li ><a href="javascript:void(0)">Link</a></li>
                        <li ><a href="javascript:void(0)">Link</a></li>
                        <li ><a href="javascript:void(0)">Link</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 hhwt-footer-grid">
                    <h4 class="titles">Blogs</h4>
                    <ul class="links">
                    <li><a href="javascript:void(0)">Link</a></li>
                    <li><a href="javascript:void(0)">Link</a></li>
                    <li><a href="javascript:void(0)">Link</a></li>
                    <li><a href="javascript:void(0)">Link</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 hhwt-footer-grid">
                    <h4 class="titles">More</h4>
                    <ul class="links">
                        <li><a href="javascript:void(0)">Link</a></li>
                        <li><a href="javascript:void(0)">Link</a></li>
                        <li><a href="javascript:void(0)">Link</a></li>
                        <li><a href="javascript:void(0)">Link</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 hhwt-footer-follow">
                <h4>Follow Us</h4>
                <div class="col-lg-offset-0 col-lg-4 col-md-offset-0 col-md-4 col-sm-offset-0 col-sm-4 col-xs-offset-3 col-xs-2">
                    <a href="javascript:void(0)"><i class="fa fa-facebook-f social_fb"></i></a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-2">
                    <a href="javascript:void(0)"><i class="fa fa-instagram social"></i></a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-2">
                    <a href="javascript:void(0)"><i class="fa fa-twitter social"></i></a>
                </div>
            </div>
            <div class="col-lg-offset-1 col-lg-3 col-md-4 col-sm-4 hhwt-footer-download">
                <div class="col-lg-12 col-md-12 col-sm-12 hhwt-footer-download-title">
                    <h4>Download Our Mobile App</h4>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 hhwt-footer-download-btn">
                    <a href="javascript:void(0)"><img src="<?php echo '/'.CONF_ROOT_DIR.'/' ?>images/ios_footer_btn.png" title="IOS APP Button1"></a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 hhwt-footer-download-btn">
                    <a href="javascript:void(0)"><img alt="Android APP Button1" src="<?php echo '/'.CONF_ROOT_DIR.'/' ?>images/android_footer_btn.png"></a>
                </div>
            </div>
            <div class="hhwt-footer-logo col-xs-12">
                <img src="<?php echo '/'.CONF_ROOT_DIR.'/' ?>images/footer_logo.png">
            </div>
            <div class="hhwt-footer-copyright col-xs-12">
                <p>© 2017 Hello Travel Pte Ltd.All Rights Reserved.</p>
            </div>
            <div class="hhwt-footer-agree col-xs-12">
                <a href="javascript:void(0)" target="_blank">End User License Agreement</a>
                <span>|</span>
                <a href="javascript:void(0)" target="_blank">Privacy Policy</a>
            </div>
        </div>
    </div>
</footer>
<script>
$(document).on("click","#destinations .carousel-control.left",function() {
    $('#destinations').carousel('prev');
});

$(document).on("click","#destinations .carousel-control.right",function() {
    $('#destinations').carousel('next');
});
</script>
<script src="<?php echo SERVER_PROTOCOL.$_SERVER['SERVER_NAME'].'/'.CONF_ROOT_DIR.'/'; ?>js/jquery.min.js"></script>
<script src="<?php echo SERVER_PROTOCOL.$_SERVER['SERVER_NAME'].'/'.CONF_ROOT_DIR.'/'; ?>js/bootstrap.min.js"></script>
<script src="<?php echo SERVER_PROTOCOL.$_SERVER['SERVER_NAME'].'/'.CONF_ROOT_DIR.'/'; ?>js/modal.js"></script>
<script src="<?php echo SERVER_PROTOCOL.$_SERVER['SERVER_NAME'].'/'.CONF_ROOT_DIR.'/'; ?>js/jquery.validate.min.js"></script>
<?php } else { ?>

<!-- Footer -->
<?php if (isset($controller) && $controller == "guest") { ?>
    <footer id="FOOTER" class="site-footer site-footer--bg" style="background-image:url(<?php echo FatUtility::generateUrl('Image', 'fatImages', array('footer-bg.jpg', 'fullwidthbanner', 2000, 500)); ?>);">
    <?php
    } elseif (isset($class) && $class == "is--dashboard") {
        $usertype = User::getLoggedUserAttribute("user_type");
        ?>
    
        <footer id="FOOTER" class="site-footer site-footer--dark with--sidebar">
            <?php } else { ?>
            <footer id="FOOTER" class="site-footer site-footer--dark">
<?php } ?>
            <div class="container container--static">
                <div class="span__row">
                    <div class="span span--10 span--center">
                        <section class="site-footer__upper">
                            <div class="container container--fluid">
                                <div class="span__row">
                                    <div class="span span--6">
                                        <div class="f__block">
                                            <h6 class="f__block__heading"><?php if (!empty($our_mission)) echo $our_mission['block_title']; ?></h6>
                                            <div class="regular-text innova-editor"><?php if (!empty($our_mission)) echo html_entity_decode($our_mission['block_content']); ?></div>
                                        </div>

                                        <div class="f__block">
                                            <img class="lazyimg" data-lazysrc="<?php echo CONF_WEBROOT_URL; ?>images/app.png" src="<?php echo CONF_WEBROOT_URL; ?>images/app.png" width="360" alt="">
                                        </div>


                                    </div>
                                    <div class="span span--2">
                                        <div class="f__block">
                                            <?php
                                            $browse_cms = Navigation::getNavigations(1);
                                            ?>
                                            <h6 class="f__block__heading"><?php echo Info::t_lang('BROWSE') ?></h6>

                                            <ul class="list list--vertical">
                                                <ul class="list list--vertical">
                                                    <?php if (!empty($browse_cms)) { ?>
                                                        <?php foreach ($browse_cms as $cms) { ?>
                                                            <li><a href="<?php echo $cms['link']; ?>" target = "<?php echo $cms['target'] ?>"><?php echo $cms['caption'] ?></a></li>
                                                        <?php } ?>
<?php } ?>
                                                </ul>

                                        </div>
                                    </div>
                                    <div class="span span--2">
                                        <div class="f__block">
                                            <?php
                                            $about_cms = Navigation::getNavigations(2);
                                            ?>
                                            <h6 class="f__block__heading"><?php echo Info::t_lang('ABOUT') ?></h6>
                                            <ul class="list list--vertical">
                                                <?php if (!empty($about_cms)) { ?>
                                                    <?php foreach ($about_cms as $cms) { ?>
                                                        <li><a href="<?php echo $cms['link']; ?>" target = "<?php echo $cms['target'] ?>"><?php echo $cms['caption'] ?></a></li>
                                                    <?php } ?>
<?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="span span--2">
                                        <div class="f__block f__currency">
                                            <h6 class="f__block__heading"><?php echo Info::t_lang('CURRENCY'); ?></h6>
                                            <form class="form form--theme form--vertical">
                                                <div class="form-element no--margin">
                                                    <div class="form-element__control">
                                                        <select class='js-currency-class'>
                                                            <?php foreach ($currencyopt as $k => $v) { ?>
                                                                <option value="<?php echo $k ?>" <?php if ($k == Info::getCurrentCurrency()) { ?> selected="selected" <?php } ?>><?php echo $v ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <a href="<?php echo Route::getRoute(); ?>" class="f__logo">
                                            <img src="<?php echo FatUtility::generateFullUrl('image', 'companyLogo', array('conf_website_footer_logo')); ?>" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="site-footer__upper">
                            <div class="container container--fluid">
                                <div class="span__row">
                                    <div class="span span--2 f__certified"></div>
                                    <div class="span span--5 span--last f__payment">
                                        <ul class="list list--horizontal">
                                            <li><img src="<?php echo CONF_WEBROOT_URL; ?>images/payment/payments-logo-small.png" alt="jcb"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="site-footer__lower">
                            <div class="container container--fluid">
                                <div class="span__row">
                                    <div class="span span--12 text--center">
                                        <div class="f__block">
                                            <?php echo Helper::fat_shortcode("[fat_sociallinks]"); ?>

                                            <p class="regular-text"><?php echo FatApp::getConfig('conf_copyright_text') ?></p>
                                            <p class="regular-text">
                                                <?php
                                                echo FatDate::nowInTimezone(FatApp::getConfig('conf_timezone'), 'Y-m-d H:i:s');
                                               
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
        </footer>
        <?php
        if (!empty($_SESSION[User::SESSION_ELEMENT_NAME]['email_verify_msg'])) {
            ?>
            <aside  class="alert alert_info fixed"> 
                <div>       
                    <div class="content"><?php echo $_SESSION[User::SESSION_ELEMENT_NAME]['email_verify_msg']; ?></div>         

                </div>
            </aside>
            <?php
        }
        if ((Message::getErrorCount() + Message::getMessageCount()) > 0) {
            ?>
            <aside id="mbsmessage" class="alert alert_success" >    
                <div>               
                    <div class="content"><?php  defined('SYSTEM_INIT') or die('Invalid Usage');
                    echo Message::getHtml(); ?></div>       
                    <a class="close" onclick="$(document).trigger('close.mbsmessage');"><svg class="icon icon--cross"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-cross"></use></svg></a>                 
                </div>              
            </aside>
        <?php } ?>
        <div  style="display:none;" id="main-search">
            <div class="search-card"  >
                <div class="search-card__action">
                    <div class="container container--static">
                        <span class="search-card__action__label">
                            <svg class="icon icon--search"><use xlink:href="#icon-search" /></svg>
                        </span>
                        <label class="search-card__action__input">
                            <input type="text" id="search-autocomplete" value=""  placeholder="<?php echo Info::t_lang('ACTIVITY_AND_WELLNESS_TRAVELS_ON_ISLANDS_IN_ASIA') ?>">
                        </label>
                        <a href="javascript:;" class="search-card__action__close" onclick="closeMainSearch()">
                            <svg class="icon icon--cross"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-cross"></use></svg>
                        </a>
                    </div>
                </div>
                <div class="search-card__result" id="search-card-result-wrapper" style="display:none;" >
                    <section class="section">
                        <div class="section__body">
                            <div class="container container--static">
                                <div class="span__row">
                                    <div class="span span--12">

                                        <div class="activity-media__list" id="search-card__result">
                                        </div>
                                        <nav class="text--center" style="margin-top:1.2em;display:none" id="more-result" >
                                            <a href="javascript:;" onclick="loadMoreMainSearch()" class="button button--fill button--dark"><?php echo Info::t_lang('LOAD_MORE') ?></a>
                                        </nav>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>  
        </div> 
        <!-- mobile menu-->
        <nav class="menu mobile-menu js-mobile-menu">
            <button class="block-heading-text block-heading-text--small js-menu-close has--opened">
                <svg class="icon icon--cross"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-cross"></use></svg> <span><?php echo Info::t_lang('CLOSE') ?></span>
            </button>
        </nav> 
        <div class="overlay js-overlay"></div>
        <!-- mobile menu end -->


      <script>
         
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '<?php echo FatApp::getConfig('CONF_FACEBOOK_TRACKING_ID') ?>');
            fbq('track', "PageView");
           
        </script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=<?php echo FatApp::getConfig('CONF_FACEBOOK_TRACKING_ID') ?>&ev=PageView&noscript=1"
                       /></noscript>
        <!-- End Facebook Pixel Code -->

        <a style="display:none;" title="Google Analytics Alternative" href="http://clicky.com/100963909"><img alt="Google Analytics Alternative" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
        <script src="//static.getclicky.com/js" type="text/javascript"></script>
        <script type="text/javascript">try {
                clicky.init(100963909);
            } catch (e) {
            }</script>
        <noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100963909ns.gif" /></p></noscript>
<?php echo FatApp::getConfig('CONF_WEBSITE_TRACKING_CODE') ?>
<script>
$(document).on("click","#userlogout",function() {

    var is_logged_user = localStorage.getItem('currentUser');    
    if (is_logged_user && typeof is_logged_user != 'undefined')
        localStorage.removeItem('currentUser');
    window.location.href = fcom.makeUrl('user', 'logout');    
});
</script>
<?php } ?>
        </body>

        </html>




