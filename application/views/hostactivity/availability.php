<main id="MAIN" class="site-main site-main--dark with--sidebar">
            <div class="site-main__body">
                <div class="menu-bar">
                    <nav class="fl--left" role="navigation">
                      <p id="bread-crumb-label" class="assistive__text">You are here:</p>
                      <ol class="breadcrumb list list--horizontal" aria-labelledby="bread-crumb-label">
                        <li class="text-heading--label"><a href="#">Account</a></li>
                        <li class="text-heading--label"><a href="#">Activities</a></li>
                        <li class="text-heading--label"><a href="#">Add LISTING</a></li>
                      </ol>
                    </nav>
                    <nav class="menu fl--right" role="navigation">
                        <ul class="list list--horizontal">
                            <li><a href="/" class="active">Profile</a></li>
                            <li><a href="/">Payout</a></li>
                            <li><a href="/">Security Settings</a></li>
                        </ul>
                    </nav>
                </div>
               <section class="section">
                        <div class="container container--static">
                    <header class="section__header section-header--bordered">
                        <div class="container container--fluid container--flex">
                            <h6 class="header__heading-text fl--left">Edit Profile</h6>
                        </div>
                    </header>
                    <div class="section__body">
                        <div class="container container--fluid">
                            <div class="span__row">
                                <div class="span span--3">
                                    <nav class="menu menu--large menu--bordered">
                                        <ul class="list list--vertical">
                                            <li><a href="">Basic Information</a></li>
                                            <li><a href="">Availability</a></li>
                                            <li><a href="">Photos/Videos</a></li>
                                            <li><a href="">Additional Information</a></li>
                                            <li><a href="">Map</a></li>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="span span--9 span-offset--2">
                                    <form class="form form--default form--horizontal">
                                        <div class="row">
                                           <div class="col-12">
                                                <div class="field-set">
                                                    <div class="caption-wraper">
                                                        <label class="field_label">Code</label>
                                                    </div>
                                                    <div class="field-wraper">
                                                        <div class="field_cover">
                                                            <input title="Code" type="text" name="discoupon_code" value="PFOOF">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="field-set">
                                                    <div class="caption-wraper">
                                                        <label class="field_label">Status</label>
                                                    </div>
                                                    <div class="field-wraper">
                                                        <div class="field_cover">
                                                            <select>
                                                                <option value="1" selected="selected">Active</option>
                                                                <option value="0">Inactive</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="field-set">
                                                    <div class="caption-wraper">
                                                        <label class="field_label">Positions</label>
                                                    </div>
                                                    <div class="field-wraper">
                                                        <div class="field_cover">
                                                            <ul class="list list--vertical">
                                                                <li>
                                                                    <label><span class="checkbox"><input type="checkbox" checked="checked"><i class="input-helper"></i></span>Header</label>
                                                                </li>
                                                                <li>
                                                                    <label><span class="checkbox"><input type="checkbox" ><i class="input-helper"></i></span>Footer</label>
                                                                </li>
                                                                <li>
                                                                    <label><span class="checkbox"><input type="checkbox" ><i class="input-helper"></i></span>Footer2</label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="field-set">
                                                    <div class="caption-wraper">
                                                        <label class="field_label">Description</label>
                                                    </div>
                                                    <div class="field-wraper">
                                                        <div class="field_cover">
                                                            <textarea >asd</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="field-set">
                                                    <div class="caption-wraper">
                                                        <label class="field_label"></label>
                                                    </div>
                                                    <div class="field-wraper">
                                                        <div class="field_cover">
                                                            <input type="submit" value="next" class="button button--fill button--green fl--right">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                       </div>
                </section>
            </div>
        </main>