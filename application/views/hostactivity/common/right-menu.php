<?php
defined('SYSTEM_INIT') or die('Invalid Usage');
?>
 <div class="menu-bar">
 <nav class="fl--left" role="navigation">
	  <p id="bread-crumb-label" class="assistive__text">You are here:</p>
	  <?php echo html_entity_decode($breadcrumb); ?>
	</nav>
 <nav class="menu fl--right" role="navigation">
	<ul class="list list--horizontal">
		<li><a href="<?php echo FatUtility::generateUrl('hostactivity','update',array(0))?>" <?php if(isset($action) && $action == 'action' && isset($controller) && $controller == 'hostactivity'){ ?>class="active" <?php } ?> ><?php echo Info::t_lang('ADD_LISTING')?></a></li>
		<li><a href="<?php echo FatUtility::generateUrl('hostactivity')?>" <?php if(isset($action) && $action == 'index' && isset($controller) && $controller == 'hostactivity'){ ?>class="active" <?php } ?> ><?php echo Info::t_lang('MANAGE_LISTING')?></a></li>
		
		
	</ul>
</nav>
</div>