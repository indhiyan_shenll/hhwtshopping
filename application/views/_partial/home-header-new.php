	<div class="container-fluid hhwt-header-fluid">
		<div class="container hhwt-header-container">
			<nav class="navbar hhwt-navbar">
				<div class="hhwt-mob-menu visible-xs">
					<span class="hhwt-mob-search fa fa-search"></span>
					<img class="hhwt-mob-logo" alt="" src="images/header_logo.png">
					<ul class="nav navbar-nav navbar-right hhwt-nav-menu-right">
						<!-- signup -->
						<li class="hhwt-nav-menu-message"><img src="images/message.png"><span  class="fa fa-circle" aria-hidden="true"></span></li>
						<li class="hhwt-nav-menu-icons"><i class="fa fa-bell" aria-hidden="true"></i><i class="fa fa-circle" aria-hidden="true"></i></li>
						<!-- <li class="hhwt-nav-menu-profile"><img src="images/profile_1.png"></li> -->
						<li class="hhwt-nav-menu-profile"><a href="#" data-toggle="dropdown" class=" dropdown-toggle"><span><img alt="" src="images/profile_1.png" class="avatar " height="41" width="41"></span></a>
						<ul role="menu" class=" menucontainer dropdown">
                            <li class="menu-item hhwtmainsubmenu"><a href="javascript:void(0)">Bucket List</a></li>
                            <li class="menu-item  hhwtmainsubmenu"><a href="javascript:void(0)" onclick="hhwt_destory_session()">Logout</a></li>
                        </ul>
					</li>
						<!-- login -->
						<li class="hhwt-trg-login"><a href="javascript:void(0)">Login</a></li>
					</ul>
				</div>
				<div class="hidden-xs navbar-header hhwt-navbar-header">
					<a class="navbar-brand" href="javascript:void(0)">
						<img src="images/header_logo.png">
					</a>
				</div>
				<div class="collapse-search collapse" id="hhwt-mob-nav-search" aria-expanded="true" style="">
					<form role="search" method="get" id="hhwt-searchform-mobile" class="hhwt-searchform" action="">
						<p class="input-group header-search">
						<span class="input-group-addon trg-submit"><i class="fa fa-search"></i></span>
						<input class="form-control search-input" name="search" placeholder="Search..." type="text" value="">
						</p>
					</form>
				</div>
				<div class="collapse in" id="hhwtNavbar">
					<ul class="nav navbar-nav hhwt-nav-menu">
						<li class="hhwt-nav-menu-li">
							<p class="search input-group header-search"><span class="input-group-addon"><i class="fa fa-search"></i></span>
								<input class="form-control search-input" placeholder="Search..." type="text"><span class="input-close">x</span>
							</p>
						</li>
						<li class="hhwt-nav-menu-li hhwt-nav-menu-li-items dropdown"><a class="hidden-xs dropdown-toggle" data-toggle="dropdown" href="#"><span><img src="images/travel.png"></span>Travel Guide</a><a class="visible-xs dropdown-toggle" data-toggle="dropdown" href="#"><span><img src="images/travel.png"></span></span>Guide</a>						
						</li>
						<li class="hhwt-nav-menu-li hhwt-nav-menu-li-items"><a class="hidden-xs" href="javascript:void(0)"><span><img src="images/trip.png"></span>Trip Planner</span></a><a class="visible-xs" href="javascript:void(0)"><span><img src="images/trip.png"></span></span>Planner</a></li>
						<li class="hhwt-nav-menu-li hhwt-nav-menu-li-items"><a class="hidden-xs hidden-sm" href="javascript:void(0)"><span><img src="images/deals.png"></span>Experiences</a><a class="visible-xs" href="javascript:void(0)"><span><img src="images/deals.png"></span>Deals</a></li>
					</ul>
				</div>
				<ul class="nav navbar-nav navbar-right hhwt-nav-menu-right hidden-xs" style="display:none;">
					<!-- signup -->
					<li class="hhwt-nav-menu-message"><img src="images/message.png"><span  class="fa fa-circle" aria-hidden="true"></span></li>
					<li class="hhwt-nav-menu-icons"><i class="fa fa-bell" aria-hidden="true"></i><i class="fa fa-circle" aria-hidden="true"></i></li>
					<li class="hhwt-nav-menu-profile"><a href="#" data-toggle="dropdown" class=" dropdown-toggle"><span><img alt="" src="images/profile_1.png" class="avatar " height="41" width="41"></span><span class="user-name">Hi Test </span><span class="caret"></span></a>
						<ul role="menu" class=" menucontainer dropdown">
                            <li class="menu-item hhwtmainsubmenu"><a href="javascript:void(0)">Bucket List</a></li>
                            <li class="menu-item  hhwtmainsubmenu"><a href="javascript:void(0)" onclick="hhwt_destory_session()">Logout</a></li>
                        </ul>
					</li> 
					

				</ul>
				<ul class="nav navbar-nav navbar-right hhwt-nav-menu-right hhwt-trg-login" style="display:block;">
					<!-- login -->
					<li class="hhwt-trg-login" style="display:block;"><a href="javascript:void(0)">Login</a></li>
				</ul>
			</nav>
		</div>
	</div>

	<!-- Modal Popup -->
	<div class="modal fade" id="hhwt-modal-signup" role="dialog" data-backdrop="static" data-keyboard="false">
	    <div class="modal-dialog hhwt-signup-dialog">      
	        <div class="modal-content hhwt-signup-content">

	            <!-- Sign Up for an Account -->
	            <div class="hhwt-signup-account">
	                <div class="modal-header hhwt-signup-header">
	                    <button type="button" class="close" data-dismiss="modal">&times;</button>                       
	                    <ul class="col-md-11 nav nav-pills hhwt-nav-pills">
	                        <li class="col-md-6 col-xs-5 active"><a data-toggle="pill" href="#hhwt-signup">Sign Up</a></li>
	                        <li class="col-md-6 col-xs-6"><a data-toggle="pill" href="#hhwt-login">Login</a></li> 
	                    </ul>
	                </div>
	                <div class="modal-body hhwt-signup-body">
	                    <div class="tab-content">
	                        <div id="hhwt-signup" class="tab-pane fade in active">
	                            <div class="alert"></div>
	                            <h3>Sign Up for an Account</h3>
	                            <p>Create an account to access exclusive deals, post reviews and photos!</p>                   
	                            <div class="checkbox hhwt-checkbox">
	                                <label class="checkbox-inline" for="subscribe"><input checked="checked" class="ng-untouched ng-pristine ng-valid" id="subscribe" name="subscribe" type="checkbox" value="yes">Subscribe to Newsletter</label>
	                            </div>
	                            <div class="hhwt-facebook">
	                                <button class="btn facebookbutton btn-facebook" type="button"><span class="fa fa-facebook-square"></span> Sign Up with Facebook</button>
	                            </div>
	                            <p class="hhwt-terms-condition">Create an account with your email. <span id="signuptxt">Sign Up here</span></p>
	                            <p class="hhwt-terms-condition">By signing up, you agree to our <span class="signuptxt">Terms of Use</span></p>
	                        </div>
	                        <div id="hhwt-login" class="tab-pane fade">
	                            <div class="alert"></div>
	                            <h3>Welcome Back</h3>
	                            <p>Sign in to with your existing account</p>
	                            <div class="hhwt-facebook">
	                                <button class="btn facebookbutton btn-facebook" type="button"><span class="fa fa-facebook-square"></span>  Log In with Facebook</button>
	                            </div>
	                            <p class="hhwt-terms-condition">Signed up with your email? <span id="login-email">Log In with Email</span></p>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <!-- Email Sign Up form -->
	            <div class="hhwt-signup-form">                  
	                <div class="modal-header hhwt-modal-header col-md-12">  
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                    <div class="hhwt-popup-title">
	                        <span class="hhwt-back"><i aria-hidden="true" class="fa fa-angle-left"></i> Back</span>
	                        <span class="signup-title">Email Sign Up</span>
	                    </div>
	                </div>
	                <div class="hhwt-modal-body">
	                    <form class="hhwt-form-registration" name="form" id="hhwt-form-registration" method="post">
	                        <div class="alert"></div>
	                        <h3>Sign Up with Your Email</h3>
	                        <p>Create an account with your email address</p>
	                        <fieldset id="Step1">
	                            <div class="form-group">
	                                <input type="text" class="form-control" name="register[user_email]"  placeholder="Email">
	                                <label id="register[user_email]-error" class="error" for="register[user_email]"></label>
	                            </div>
	                            <div class="form-group">
	                                <input type="password" class="form-control" name="register[user_password]" placeholder="Password" >
	                                <button type="button" name="next" class="btn next action-button hhwt-next hhwt-submit-btn value="Next"">Sign Up With Email</button>
	                            </div>
	                        </fieldset>
	                        <fieldset id="Step2">
	                            <div class="form-group">
	                                <input type="text" class="form-control" name="register[user_login]" placeholder="Username" >
	                                <label id="register[user_login]-error" class="error" for="register[user_login]"></label>
	                            </div>
	                            <div class="form-group">
	                                <input type="text" class="form-control" name="register[first_name]" placeholder="Firstname" >
	                            </div>
	                            <div class="form-group">
	                                <input type="text" class="form-control" name="register[last_name]" placeholder="Lastname" >
	                            </div>
	                            <button type="button" name="prev" id="account" class="prev action-button hhwt-prev" value="Prev"><i aria-hidden="true" class="fa fa-angle-left"></i>Prev</button>
	                            <div class="form-group">
	                                <button type="submit" class="btn hhwt-submit-btn">Sign Up With Email</button>
	                            </div>
	                        </fieldset>
	                        <p class="hhwt-terms-condition">By signing up, you agree to our <span>Terms of Use</span></p>
	                    </form> 
	                </div>
	            </div>

	            <!-- Email Log In -->
	            <div class="hhwt-login-form">                   
	                <div class="modal-header hhwt-modal-header col-md-12">  
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                    <div class="hhwt-popup-title">
	                        <span class="hhwt-back"><i aria-hidden="true" class="fa fa-angle-left"></i> Back</span>        
	                        <span class="signup-title">Email Log In</span>
	                    </div>                          
	                </div>
	                <div class="hhwt-modal-body">
	                    <form class="hhwt-form-login" name="form" id="hhwt-form-login" method="post"> 
	                        <div class="alert"></div>  
	                        <h3>Log In with Your Email</h3>
	                        <p>Log in here if your signed up with your email</p>
	                        <div class="form-group">                        
	                            <input type="text" class="form-control" name="user_email"  placeholder="Email Address">
	                        </div>
	                        <div class="form-group">
	                            <input type="password" class="form-control" name="user_password" placeholder="Password" >
	                        </div>
	                        <div class="form-group">
	                            <button type="submit" class="btn hhwt-submit-btn">Sign In With Email</button>
	                        </div>
	                        <p class="hhwt-terms-condition" id="forget-password">Forgot your Password?</p>
	                    </form> 
	                </div>
	            </div>

	            <!-- Forgot Password -->
	            <div class="hhwt-forget-form">
	                <div class="modal-header hhwt-modal-header col-md-12">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                    <div class="hhwt-popup-title">
	                        <span id="hhwt-back-forget"><i aria-hidden="true" class="fa fa-angle-left"></i> Back</span>
	                        <span class="signup-title">Forgot Password</span>
	                    </div>
	                </div>
	                <div class="hhwt-modal-body">
	                    <div class="alert"></div>
	                    <form class="hhwt-forget-password" name="hhwt-forget-password" id="hhwt-forget-password" method="post">
	                        <h3>Reset Your Password</h3>
	                        <p>Plesae enter the email address that you used when creating your account.</p>
	                        <div class="form-group">
	                            <input type="email" class="form-control" id="forget-email" name="forgetemail"  placeholder="Email Address">
	                        </div>
	                        <div class="form-group">
	                            <button type="submit" class="btn hhwt-submit-btn">Reset Password</button>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
