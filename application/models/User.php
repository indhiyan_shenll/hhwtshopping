<?php
class User extends MyAppModel {
	
	const DB_TBL = 'tbl_users';
	const DB_TBL_PREFIX = 'user_';
	const SESSION_ELEMENT_NAME = 'UserSession';
	const SESSION_SHARED_USER_DETAILS = 'sharedUserDetails';
	const REGISTER_SUCCESS = '1';
	const LOGIN_SUCCESS = '1';


	
	public function __construct($userId = 0) {
		parent::__construct ( static::DB_TBL, static::DB_TBL_PREFIX . 'id', $userId );
		$this->objMainTableRecord->setSensitiveFields ( array (
				'user_regdate' 
		) );
	}
	public function save() {
		if (! ($this->mainTableRecordId > 0)) {
			$this->setFldValue ( 'user_regdate', date ( 'Y-m-d H:i:s' ) );
		}
		
		return parent::save ();
	}
	public function setLoginCredentials($username, $password, $active = null, $verified = null) {
		if (! ($this->mainTableRecordId > 0)) {
			$this->error = 'Invalid Request! User not initialized.';
			return false;
		}
		
		$record = new TableRecord ( 'tbl_users' );
		$arrFlds = array (
				'user_email' => $username,
				'user_password' => User::encryptPassword ( $password ) 
		);
		
		if (null != $active) {
			$arrFlds ['user_active'] = $active;
		}
		if (null != $verified) {
			$arrFlds ['user_verified'] = $verified;
		}
		
		$record->setFldValue ( 'user_id', $this->mainTableRecordId );
		$record->assignValues ( $arrFlds );
		if (! $record->addNew ( array (), $arrFlds )) {
			$this->error = $record->getError ();
			return false;
		}
		
		return true;
	}
	/*
		return false if record not exist else return record array
	*/
	function getUserByEmail($email){
		$srch = new SearchBase('tbl_users');
		$srch->addCondition('user_email','=', $email);
		$rs = $srch->getResultSet();
		$row = FatApp::getDb()->fetch($rs);
		if(empty($row)) return false;
		return $row;
	}
	
	function getUserByUserId($user_id){
		$srch = new SearchBase('tbl_users');
		$srch->addCondition('user_id','=', $user_id);
		$rs = $srch->getResultSet();
		$row = FatApp::getDb()->fetch($rs);
		if(empty($row)) return false;
		return $row;
	}
	
	public function verifyAccount($v = 1) {
		if (!($this->mainTableRecordId > 0)) {
			$this->error = 'User not set.';
			return false;
		}
		
		$db = FatApp::getDb();
		if (! $db->updateFromArray ( 'tbl_users', array (
				'user_verified' => $v 
		), array (
				'smt' => 'user_id = ?',
				'vals' => array (
						$this->mainTableRecordId 
				) 
		) )) {
			$this->error = $db->getError();
			return false;
		}
		
		// You may want to send some email notification to user that his account is verified.
		
		return true;
	}
	
	public function activateAccount($v = 1) {
		if (!($this->mainTableRecordId > 0)) {
			$this->error = 'User not set.';
			return false;
		}
		
		$db = FatApp::getDb();
		if (! $db->updateFromArray ( 'tbl_users', array (
				'user_active' => $v 
		), array (
				'smt' => 'user_id = ?',
				'vals' => array (
						$this->mainTableRecordId 
				) 
		) )) {
			$this->error = $db->getError();
			return false;
		}
		
		return true;
	}
	
	public function getProfileData() {
		return $this->getAttributesById($this->mainTableRecordId);
	}
	
	
	public static function encryptPassword($pass) {
		return md5(PASSWORD_SALT . $pass . PASSWORD_SALT);
	}
	
	public function logFailedAttempt($ip, $username) {
		$db = FatApp::getDb();
		
		$db->deleteRecords ( 'tbl_failed_login_attempts', array (
				'smt' => 'attempt_time < ?',
				'vals' => array (
						date ( 'Y-m-d H:i:s', strtotime ( "-7 Day" ) ) 
				) 
		) );
		
		$db->insertFromArray('tbl_failed_login_attempts', array(
				'attempt_username'=>$username,
				'attempt_ip'=>$ip,
				'attempt_time'=>date('Y-m-d H:i:s')
		));
		
		// For improvement, we can send an email about the failed attempt here.
	}
	
	public function isBruteForceAttempt($ip, $username) {
		$db = FatApp::getDb();
		
		$srch = new SearchBase('tbl_failed_login_attempts');
		$srch->addCondition('attempt_ip', '=', $ip)->attachCondition('attempt_username', '=', $username);
		$srch->addCondition('attempt_time', '>=', date('Y-m-d H:i:s', strtotime("-4 Minute")));
		$srch->addFld('COUNT(*) AS total');
		
		$rs = $srch->getResultSet();
		
		$row = $db->fetch($rs);
		
		return ($row['total'] > 2);
	}

	public function authenticateUser($user_email)
	{
		$db = FatApp::getDb();
		$srch = new SearchBase('tbl_users');
		$srch->addCondition('user_email', '=', $user_email);
		$rs = $srch->getResultSet();

		if ($rs) {
			$user_data = $db->fetch($rs);
			return $user_data;
		}
		return false;
	}

	public function getCountryId($country) {

		$db = FatApp::getDb();
		$country_id = "";
		if (!empty($country)) {
			$csrch = new SearchBase('tbl_countries');
			$csrch->addCondition('country_name', '=', $country);
			$crs = $csrch->getResultSet();
								
			if ($csrch->recordCount() > 0) {
				$crow = $db->fetch($crs);
				$country_id = $crow['country_id'];
			}
		}
		return $country_id;
	}

	public function setShoppingSession($user_data)
	{
		$_SESSION[static::SESSION_ELEMENT_NAME] = array(
			'user_id'=>$user_data['user_id'],
			'user_name'=>$user_data['user_name'],
			'user_email'=>$user_data['user_email'],
			'user_type'=>$user_data['user_type'],
			'user_ip'=>$user_data['user_ip'],
			'currency'=>Info::getCurrentCurrency()
		);
		return true;
	}

	public function setLocalStorage($user_data)
	{
		$_SESSION[static::SESSION_SHARED_USER_DETAILS] = $user_data;
		return true;
	}
	
	public function remoteCheck($remote_url, $post_data = array()) {

		$curl_response = static::curlPost($remote_url, $post_data);
		if ($curl_response != false) {

			$user_response = json_decode($curl_response, true);
			return $user_response;
		} else {

			FatUtility::dieJsonError(Info::t_lang('SOMETHING_WENT_WRONG._PLEASE_TRY_AGAIN!'));
			return false;
		}
	}
	public function login($username, $password, $ip, $encryptPassword = true) {
		
		$db = FatApp::getDb();
		$userEmail = $username;
		$userPassword = $password;
		/*check user exist in planner API*/
		$login_data = array("email"=>$userEmail, "password"=>$userPassword);
		$userDetails = static::remoteCheck(USER_LOGIN_API_URL, $login_data);			
		if ($userDetails['status'] == static::LOGIN_SUCCESS) {

			$user_exist = static::checkAndAddShopUser($userDetails);
			$user_exist['user_ip'] = $ip;
			
			/* set shopping session */				
			static::setShoppingSession($user_exist);
	
			$this->updateLoginTime($user_exist[User::DB_TBL_PREFIX . 'id']);			

			/* set local storage */
			static::setLocalStorage($userDetails);				

		} else {
			FatUtility::dieJsonError(Info::t_lang('AUTHENTICATION_INVALID'));
			return false;
		}

		return true;
	}

	public function registerUser($registerData, $ip, $encryptPassword = true) {
		
		$db = FatApp::getDb();
		$userEmail = ($registerData['user_email']) ? $registerData['user_email'] : "" ;
		$userPassword = ($registerData['user_password']) ? $registerData['user_password'] : "" ;
		$countryId = ($registerData['user_country_id']) ? $registerData['user_country_id'] : "" ;
		$countryname = "";
		if (!empty($countryId)) {
			$country = Country::getAttributesById($countryId);
			$countryname = $country['country_name'];
		}
		
		$signupData = array(
					'type' => 'shopping',
					'fname' => $registerData['user_firstname'],
					'lname' => $registerData['user_lastname'],
					'email' => $userEmail,
				 	'password' => $userPassword,
					'username' => $registerData['user_name']." ".$registerData['user_name'],					
					'country' => $countryname,
					'subscribe' => ''
					// 'last_login' => Info::currentDatetime()
					// 'name' => $registerData['user_firstname']." ".$registerData['user_lastname'],
					// 'device_name' => '',
					// 'device_version' => ''
				 	
			);

		/*check user exist in planner API*/		
		$userDetails = static::remoteCheck(USER_REGISTER_API_URL, $signupData);
		if ($userDetails['status'] == static::REGISTER_SUCCESS) {

			$user_exist = static::checkAndAddShopUser($userDetails);
			$user_exist['user_ip'] = $ip;

			/* set shopping session */				
			static::setShoppingSession($user_exist);										

			/* set local storage */
			static::setLocalStorage($userDetails);				

		} else {
			FatUtility::dieJsonError('Email already exists!');
			return false;
		}

		return true;
	}

	public function checkAndAddShopUser($userData) {

		$db = FatApp::getDb();
		/*check user exist in shopping*/
		$userEmail = ($userData['email']) ? $userData['email'] : '';
		/*format shopping data*/			
		$displayname = ($userData['name']) ? $userData['name'] : '';
		$firstname = $lastname = "";
		if (!empty($displayname)) {
			$arrname = explode(" ", $displayname);
			$firstname = $arrname[0];
			if (count($arrname)>1)
				$lastname = $arrname[1];
		}			
		$country = ($userData['country']) ? $userData['country'] : "" ;
		$user_image = ($userData["image"]) ? $userData["image"] : '';
		$user_phone = ($userData["phonenumber"]) ? $userData["phonenumber"] : '';	
		$lastlogin = ($userData["lastlogin"]) ? $userData["lastlogin"] : '';
		$user_exist = static::authenticateUser($userEmail);
		if (!$user_exist) {

			$country_id = static::getCountryId($country);

			$data = array(
			    User::DB_TBL_PREFIX . 'email' => $userEmail,
			    User::DB_TBL_PREFIX . 'firstname' => $firstname,
			    User::DB_TBL_PREFIX . 'lastname' => $lastname,			    
			    User::DB_TBL_PREFIX . 'phone' => $user_phone,
			    User::DB_TBL_PREFIX . 'country_id' => $country_id,
			    User::DB_TBL_PREFIX . 'type' => 0,
			    User::DB_TBL_PREFIX . 'active' => 1,
			    User::DB_TBL_PREFIX . 'verified' => 1,
			    User::DB_TBL_PREFIX . 'last_login' => $lastlogin
			);
			$usr = new User();
			$usr->assignValues($data);
			$insertStatus = $usr->save();
			if (!$insertStatus) {
			    FatUtility::dieJsonError(Info::t_lang('SOMETHING_WENT_WRONG._PLEASE_TRY_AGAIN!'));
			}

			$user =  static::getUserByEmail($userEmail);
			$user_id = $user["user_id"];
			$user_exist = array(
				'user_id'=> $user_id,
				'user_name'=> $firstname,
				'user_email'=> $userEmail,
				'user_type'=> 0,
				'currency'=>Info::getCurrentCurrency()
			);
		} else {
			$user_id = $user_exist[User::DB_TBL_PREFIX . 'id'];
		}	
		
		return $user_exist;
	}

	// Added Indhiyan Oct 28, 2017
	public function sharedUserLogin($plannerData, $ip, $encryptPassword = true) {
		/*check user exist in shopping*/
		$user_exist = static::checkAndAddShopUser($plannerData);
		$user_exist['user_ip'] = $ip;
		/* set shopping session */				
		static::setShoppingSession($user_exist);
		$this->updateLoginTime($user_exist[User::DB_TBL_PREFIX . 'id']);			
		return true;
	}

	public function updateLoginTime($user_id = 0){
		if($user_id > 0){
			$this->mainTableRecordId = $user_id;
		}
		$record = new TableRecord(self::DB_TBL);
		$data[self::DB_TBL_PREFIX.'last_login'] = Info::currentDatetime();
		$record->assignValues($data);
		return $record->update(array('smt' => 'user_d = ?', 'vals'=>array($user_id)));
			
	}
	
	public static function isUserLogged($ip = '') {
		if ($ip == '') {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		if (! isset ( $_SESSION [static::SESSION_ELEMENT_NAME] ) 
				|| $_SESSION [static::SESSION_ELEMENT_NAME] ['user_ip'] != $ip 
				|| ! is_numeric ( $_SESSION [static::SESSION_ELEMENT_NAME] ['user_id'] ) 
				|| 0 >= $_SESSION [static::SESSION_ELEMENT_NAME] ['user_id'] ) {
			return false;
		}
		
	
		return true;
	}
	
	public static function getLoggedUserAttribute($attr, $returnNullIfNotLogged = true) {
		if ( ! static::isUserLogged() ) {
			if ( $returnNullIfNotLogged ) return false;
				
			FatUtility::dieWithError('User Not Logged.');
		}
	
		if ( array_key_exists($attr, $_SESSION [static::SESSION_ELEMENT_NAME]) ) {
			return $_SESSION [static::SESSION_ELEMENT_NAME][$attr];
		}
	
		return User::getAttributesById($_SESSION[static::SESSION_ELEMENT_NAME]['user_id'], $attr);
	}
	
	public static function getLoggedUserId($returnZeroIfNotLogged = false) {
		return FatUtility::int(static::getLoggedUserAttribute('user_id', $returnZeroIfNotLogged));
	}
	
	
	function deleteOldPasswordResetRequest(){
		$db = FatApp::getDb();
		if(!$db->deleteRecords('tbl_user_password_resets_requests',array('smt'=>'aprr_expiry < ?','vals'=>array(date('Y-m-d H:i:s'))))){
			$this->error = $db->getError();
			return false;
		}
		return true;
	}
	
	function deletePasswordResetRequest($user_id){
		$db = FatApp::getDb();
		if(!$db->deleteRecords('tbl_user_password_resets_requests',array('smt'=>'appr_user_id = ?','vals'=>array($user_id)))){
			$this->error = $db->getError();
			return false;
		}
		return true;
	}
	
	function getPasswordResetRequest($user_id){
		$search = new SearchBase('tbl_user_password_resets_requests');
		$search->addCondition('appr_user_id','=',$user_id);
		$rs = $search->getResultSet();
		return FatApp::getDb()->fetch($rs);
	}
	
	function addPasswordResetRequest($array){
		$tbl = new TableRecord('tbl_user_password_resets_requests');
		$tbl->assignValues($array);
		if(!$tbl->addNew()){
			$this->error = $db->getError();
			return false;
		}
		return true;
    }
	
	public function isValidVerifyToken($token){
		$db = FatApp::getDb();
		$srch = new SearchBase("tbl_user_verification");
		$srch->addCondition("uverification_token","=",$token);
		$rs = $srch->getResultSet();		
		$record  = $db->fetch($rs);
		if(empty($record))
			return true;
		return false;
	}
	
	function addUserVerifyToken($data){
		$db = FatApp::getDb();
		$success = $db->insertFromArray('tbl_user_verification', $data);
		if($success)
			return true;
		return false;	
	}
	
	function getUserVerifyToken($token){
		$srch = New SearchBase('tbl_user_verification');
		$srch->addCondition('uverification_token','=', $token);
		return FatApp::getDb()->fetch($srch->getResultSet());
	}
	
	function getUserVerifyTokenByUserid($user_id){
		$srch = New SearchBase('tbl_user_verification');
		$srch->addCondition('uverification_user_id','=', $user_id);
		return FatApp::getDb()->fetch($srch->getResultSet());
	}
	public function getTotalUsersCount()
	{
		$srch = new SearchBase(static::DB_TBL);
		// Added Indhiyan Oct 27, 2017
		// SELECT sum(if(user_type = 0,1,0)) total_traveler, sum(if(user_type = 1,1,0)) total_host FROM `tbl_users`
		$srch->addMultipleFields(
				array(
					'sum(if(user_type = 0,1,0)) total_traveler',
					'sum(if(user_type = 1,1,0)) total_host',
				)
			);
		
		$srch->doNotCalculateRecords();
		$srch->doNotLimitRecords();
		$rs = $srch->getResultSet();
		
		return FatApp::getDb()->fetch($rs);
	}		
	

	function curlPost($url, $data = array(), $method=1)
	{
	    $curl = curl_init();

	    // Optional Authentication:
	    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl, CURLOPT_POST, $method);

	    if ($data) {
	    	$params = http_build_query($data);
	        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
	    }

	    $result = curl_exec($curl);
	    $err = curl_error($curl);

	    curl_close($curl);

	    if ($err) {
	    	return false;
	    } else {
		    return $result;
		}
	}
}