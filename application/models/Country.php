<?php

class Country extends MyAppModel {

    const DB_TBL = 'tbl_countries';
    const DB_TBL_PREFIX = 'country_';

    public function __construct($countryId = 0) {
        $countryId = FatUtility::convertToType($countryId, FatUtility::VAR_INT);

        parent::__construct(static::DB_TBL, static::DB_TBL_PREFIX . 'id', $countryId);
        $this->objMainTableRecord->setSensitiveFields(array());
    }

    public static function getSearchObject() {
        $srch = new SearchBase(static::DB_TBL);
		$srch->joinTable(Region::DB_TBL, 'INNER JOIN', Region::DB_TBL_PREFIX . "id=" . self::DB_TBL_PREFIX . "region_id");
        $srch->addOrder(static::DB_TBL_PREFIX . 'name');
        return $srch;
    }

    public static function getCountries() {
        $srch = new SearchBase(static::DB_TBL);
        $srch->addCondition(static::DB_TBL_PREFIX . 'active', '=', 1);
        $srch->addOrder(static::DB_TBL_PREFIX . 'name');
        $srch->addFld("country_id");
        $srch->addFld("country_name");
        $records = FatApp::getDb()->fetchAllAssoc($srch->getResultSet());
        return $records;
    }


    public static function getCountriesPhoneCode() {
        $srch = new SearchBase(static::DB_TBL);
        $srch->addOrder(static::DB_TBL_PREFIX . 'name');
        $srch->addFld("country_id");
        $srch->addFld("country_phone_code");
        $records = FatApp::getDb()->fetchAllAssoc($srch->getResultSet());
        return $records;
    }

	public static function getAllCountryByRegionId($id) {
        $srch = self::getSearchObject();
        $srch->addCondition(Region::DB_TBL_PREFIX . "id", '=', $id);
        $srch->addCondition(static::DB_TBL_PREFIX . "active", '=', 1);
        $srch->addCondition(Region::DB_TBL_PREFIX . "active", '=', 1);
        $srch->addFld(static::DB_TBL_PREFIX . "id");
        $srch->addFld(static::DB_TBL_PREFIX . "name");
        $records = FatApp::getDb()->fetchAllAssoc($srch->getResultSet());
        return $records;
    }
	
	public static function getCities($country_id = 0,$limit=-1) {
		$srch = new SearchBase('tbl_cities');
		$srch->addCondition('city_country_id','=',  $country_id);
		$srch->addCondition('city_active','=',1);
		$srch->addFld('city_id');
		$srch->addFld('city_name');
		if($limit > -1){
			$srch->setPageSize($limit);
		}
		$srch->addOrder('city_display_order','asc');
		$rs = $srch->getResultSet();
		$records = FatApp::getDb()->fetchAllAssoc($rs);
		return $records;
	}
    
}