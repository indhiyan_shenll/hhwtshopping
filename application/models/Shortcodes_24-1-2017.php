<?php

/*
Available shortcodes:

[fat_partnershipform] To get Partnership Form
[fat_contactinfo] To get COntact info
[fat_offices] To get Office addresses
[fat_contactform] TO get COntact Form
[block blkid='22'] To get a block HTML
[fat_founders fatclass='section--top-border'] To get all added Founders
[fat_investors fatclass='section--top-border'] To get all added Investores
[fat_testimonials limit='2' fatclass="test testimonial__section section--top-border" id="asSeenOn"] To get all added Testimonials

*/
class Shortcodes extends MyAppModel
{
	protected $tpl;
	protected $_db;
	public static $instance;
	
	function __construct(){
		$this->tpl = new FatTemplate('', '');
		$this->_db = FatApp::getdb();
    }
	
	public static function getInstance() {
		if (!self::$instance) {
		  self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	private function pregexp()
	{
		return '/\[([a-zA-Z0-9-_: |=,"\'\.]+)]/';
	}
	// Parsing shortcodes in content
	public function parse ($str)
    {
		// Find matches against shortcodes like [block id=1]
		
		$reqExp = $this->pregexp();

        preg_match_all($reqExp, $str, $shortcodes);
        
		if ($shortcodes == NULL)
		{
		    return $str;
        }
        $shortcode_array = array();
        foreach ($shortcodes[1] as $key => $shortcode)
		{
            if (strstr($shortcode, ' '))
			{
				$code = substr($shortcode, 0, strpos($shortcode, ' '));
				$attrStr = str_replace($code . ' ', '', $shortcode);
				$tmp = explode(' ', $attrStr);
				// Info::test($tmp);
                $parameters = array();
                if (count($tmp))
				{
                    foreach ($tmp as $params)
					{
                        $pair = explode('=', $params);
						if(count($pair) > 1)
						{
							$paramVal = str_replace(array("'", '"'), "", $pair[1] );
							$param = trim($pair[0]);
							$parameters[$param] = trim($paramVal);
						}
                    }
                }
                $array = array('shortcode' => $shortcodes[0][$key], 'code' => $code, 'params' => $parameters);
            }
            else
			{
                $array = array('shortcode' => $shortcodes[0][$key], 'code' => $shortcode, 'params' => array());
            }
            // $shortcode_array[$shortcodes[0][$key]] = $array;
            $shortcode_array[] = $array;
        }
        // Info::test($shortcode_array);
		// Replace shortcode instances with HTML strings
		if (count($shortcode_array))
		{
			foreach ($shortcode_array as $search => $shortcodeData)
			{
				switch($shortcodeData['code'])
				{
					case 'block' :
						$str = str_replace($shortcodeData['shortcode'], $this->getBlockContent($shortcodeData['params']), $str);
						break;
					default:
						$func = strtolower($shortcodeData['code']);
						if(method_exists($this, $func)){
							$str = str_replace($shortcodeData['shortcode'],  $this->$func($shortcodeData['params']), $str);
						}
					break;
				}
			}
        }
        // Return the entire parsed string
		return $str;
    }
	
	function getBlockContent($params = array())
	{
		if(count($params))
		{
			if(isset($params['blkid']) && $params['blkid'] > 0)
			{
				$content = $this->getBlock($params['blkid']);
				if($content)
				{
					$str = $this->parse($content['block_content']);
					return $str;
				}
			}
		}
		return '';
	}
	
	private function getBlock($blkid){
		$record = array();
		$srch = new SearchBase('tbl_blocks');
		
		$srch->addCondition('block_id', '=', $blkid);
		$srch->addCondition('block_active', '=', 1);
		
		$srch->doNotCalculateRecords();
		$srch->doNotLimitRecords();
		
		$rs = $srch->getResultSet();
		
		$record = $this->_db->fetch($rs);
		return $record;		
	}
	
	public function fat_testimonials($params = array())
	{
		$srch = Testimonial::getSearchObject(true, true);
		
		if(isset($params['limit']) && $params['limit'] > 0)
		{
			$srch->setPageSize($params['limit']);
		}
		$srch->addCondition(Testimonial::DB_TBL_PREFIX.'status', '=', 1);
		$srch->addOrder(Testimonial::DB_TBL_PREFIX.'display_order');
		$rs = $srch->getResultSet();
		$testimonials = FatApp::getDb()->fetchAll($rs,Testimonial::DB_TBL_PREFIX.'id');
		
		$html = '';
		if(count($testimonials) > 0)
		{
			$cls = 'testimonial__section';
			$id = 'asSeenOn';
			if(!empty($params['fatclass']))
			{
				$cls = implode(' ', explode(',', $params['fatclass']));
			}
			
			if(!empty($params['fatid']))
			{
				$id = $params['fatid'];
			}
			$this->tpl->set('cls', $cls );
			$this->tpl->set('id', $id );
			$this->tpl->set('testimonials', $testimonials );
			$html = $this->tpl->render(false, false, '_partial/templates/fat-testimonials.php', true);
		}
		return $html;
	}
	
	public function fat_team($params = array())
	{
		
	}
	
	public function fat_founders($params = array())
	{
		$founderObj = new Founder();
		$founders = $founderObj->getFounders();
		
		$html = '';
		if(!empty($founders))
		{
			$cls = 'section--top-border founder__section';
			$id = 'founder';
			
			if(!empty($params['fatclass']))
			{
				$cls = implode(' ', explode(',', $params['fatclass']));
			}
			
			if(!empty($params['fatid']))
			{
				$id = $params['fatid'];
			}
			$this->tpl->set('cls', $cls );
			$this->tpl->set('id', $id );
			$this->tpl->set('founders', $founders );
			$html = $this->tpl->render(false, false, '_partial/templates/fat-founders.php', true);
			
		}
		return $html;
	}
	public function fat_investors($params = array())
	{
		$investor = new Investor();
		$investors = $investor->getInvestors();
		$html = '';
		if(!empty($investors))
		{
			$cls = 'section--light investor__section';
			$id = 'investor';
			if(!empty($params['fatclass']))
			{
				$cls = implode(' ', explode(',', $params['fatclass']));
			}
			
			if(!empty($params['fatid']))
			{
				$id = $params['fatid'];
			}
			$this->tpl->set('cls', $cls );
			$this->tpl->set('id', $id );
			$this->tpl->set('investors', $investors );
			$html = $this->tpl->render(false, false, '_partial/templates/fat-investors.php', true);
		}
		return $html;
	}
	
	public function fat_contactinfo($params = array())
	{
		$cls = 'tip__section';
		$id = 'tips';
		if(!empty($params['fatclass']))
		{
			$cls = implode(' ', explode(',', $params['fatclass']));
		}
		
		if(!empty($params['fatid']))
		{
			$id = $params['fatid'];
		}
		
		$this->tpl->set('cls', $cls );
		$this->tpl->set('id', $id );
		$html = $this->tpl->render(false, false, '_partial/templates/fat-contactinfo.php', true);
		
		return $html;
	}
	
	public function fat_offices($params = array())
	{
		$ofc = new Office();
		$offices = $ofc->getOffices();
		$html = '';
		if(count($offices) > 0)
		{
			$cls = 'why-choose__section';
			$id = 'whyChooseUs';
			if(!empty($params['fatclass']))
			{
				$cls = implode(' ', explode(',', $params['fatclass']));
			}
			
			if(!empty($params['fatid']))
			{
				$id = $params['fatid'];
			}
			$this->tpl->set('cls', $cls );
			$this->tpl->set('id', $id );
			$this->tpl->set('offices', $offices );
			$html = $this->tpl->render(false, false, '_partial/templates/fat-offices.php', true);
		}
		return $html;
	}
	
	public function fat_contactform()
	{
		$frm = $this->getContactForm();
		
		$this->tpl->set('frm', $frm);
		$formHtml = $this->tpl->render(false, false, '_partial/templates/fat-contact-form.php', true);
		
		return $formHtml;
	}
	
	public function fat_partnershipform()
	{
		$frm = $this->getPartnershipsForm();
		
		$this->tpl->set('frm', $frm);
		$formHtml = $this->tpl->render(false, false, '_partial/templates/fat-partnerships-form.php', true);
		
		return $formHtml;
	}
	
	public function getContactForm(){
		$frm = new Form('frmContact');
	
		$frm->addRequiredField(Info::t_lang('NAME'),"name");
		$frm->addEmailField(Info::t_lang('EMAIL_ADDRESS'),"email");
		$fld = $frm->addSelectBox(Info::t_lang('HOW_CAN_WE_HELP'),'option',Info::contactUsOptions(),'',array(),'');
		$fld->requirements()->setRequired();
		$fld = $frm->addTextArea(Info::t_lang('MESSAGE'), 'message')->requirements()->setRequired();
	
		$fld=$frm->addRequiredField(Info::t_lang('SECURITY_CODE'), 'security_code','',array('autocomplete'=>'off'))->htmlAfterField='<div class="captcha-wrapper"><img src="'.FatUtility::generateUrl("image","captcha").'" id="image" class="captcha captchapic"/><a href="javascript:void(0);" class ="reloadpic reloadlink reload" onclick="refreshCaptcha(\'image\')"><svg class="icon icon--reload">
				<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-reload"></use>
			</svg></a></div>';
		$fld = $frm->addSubmitButton('&nbsp;', 'btn_submit', Info::t_lang('SUBMIT'));
		return $frm;
	}
	
	public function getPartnershipsForm(){
		$frm = new Form('partnershipsFrm');
		$frm->addRequiredField(Info::t_lang('NAME'),'partner_name');
		$partner_email = $frm->addEmailField(Info::t_lang('EMAIL'),'partner_email');
		$partner_email->setUnique(Partners::DB_TBL,Partners::DB_TBL_PREFIX.'email',Partners::DB_TBL_PREFIX.'id','partner_email','partner_email');
		
		$frm->addTextBox(Info::t_lang('COMPANY'),'partner_company');
		$frm->addTextBox(Info::t_lang('WEBSITE'),'partner_website');
		$frm->addTextArea(Info::t_lang('MESSAGE'),'partner_message');
		$countries = Country::getCountries();
		$partner_country = $frm->addSelectBox(Info::t_lang('COUNTRY'),'partner_country',$countries,'',array(),Info::t_lang('SELECT_COUNTRY'));
		$partner_country->requirements()->setRequired();
		$partner_describe = $frm->addSelectBox(Info::t_lang('HOW_TO_DESCRIBE_YOU'),'partner_describe',Info::PartnerDescribe(),1,array(),'');
		$partner_describe->requirements()->setRequired();
		
		$submit_btn = $frm->addSubmitButton('', 'btn_submit', Info::t_lang('SUBMIT'),array('class'=>'button button--fill button--green'));
		return $frm;
	}
	
	public function fat_mailchimpnewsletter($params = array())
	{
		$params['fieldcols'] = ((isset($params['fieldcols']) && $params['fieldcols'] > 0) ? $params['fieldcols'] : 9);
		
		$frm = Helper::getNewsletterForm($params);
		
		$this->tpl->set('params', $params);
		$this->tpl->set('frm', $frm);
		$formHtml = $this->tpl->render(false, false, '_partial/templates/fat-mailchimp-form.php', true);
		
		return $formHtml;
	}
}

