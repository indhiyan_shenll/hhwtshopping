<?php

class Currency extends MyAppModel {

    const DB_TBL = 'tbl_currency';
    const DB_TBL_PREFIX = 'currency_';
    
    static $defaultCurrencyId = 2;

    public function __construct($tableId = 0) {
        $tableId = FatUtility::convertToType($tableId, FatUtility::VAR_INT);

        parent::__construct(static::DB_TBL, static::DB_TBL_PREFIX . 'id', $tableId);
        $this->objMainTableRecord->setSensitiveFields(array());
        self::$defaultCurrencyId = FatApp::getConfig('conf_default_currency', FatUtility::VAR_INT, '2');
    }

    public static function getSearchObject() {
        $srch = new SearchBase(static::DB_TBL);
        return $srch;
    }

    public static function displayDefaultPrice($price, $isDisplaySymbol = true) {
        $srch = Self::getSearchObject();
        $srch->addCondition('currency_id', '=', self::$defaultCurrencyId);
        $rs = $srch->getResultSet();
        $record = FatApp::getDb()->fetch($rs);

        $currency = $record['currency_symbol'];
        if (!$isDisplaySymbol) {
            $currency = $record['currency_code'];
        }
        $pricestring = '{leftsymbol}{price}{rightsymbol}';
        if ($record['currency_symbol_location'] == 1) {
            $pricestring = str_replace('{leftsymbol}', $currency, $pricestring);
            $pricestring = str_replace('{rightsymbol}', '', $pricestring);
        } else {
            $pricestring = str_replace('{rightsymbol}', $currency, $pricestring);
            $pricestring = str_replace('{leftsymbol}', '', $pricestring);
        }
        //$pricestring = str_replace('{price}',money_format("%i",$price*$record['currency_rate']),$pricestring);
        $formatedprice = str_replace(".00",'',number_format(round($price * $record['currency_rate']), 2));
        $pricestring = str_replace('{price}',$formatedprice, $pricestring);
        return $pricestring;
    }

    public static function displayPrice($price, $isDisplaySymbol = true) {
        $srch = Self::getSearchObject();
        $srch->addCondition('currency_id', '=', Info::getCurrentCurrency());
        $rs = $srch->getResultSet();
        $record = FatApp::getDb()->fetch($rs);

        $currency = $record['currency_symbol'];
        if (!$isDisplaySymbol) {
            $currency = $record['currency_code'];
        }
        $pricestring = '{leftsymbol}{price}{rightsymbol}';
        if ($record['currency_symbol_location'] == 1) {
            $pricestring = str_replace('{leftsymbol}', $currency, $pricestring);
            $pricestring = str_replace('{rightsymbol}', '', $pricestring);
        } else {
            $pricestring = str_replace('{rightsymbol}',  $currency, $pricestring);
            $pricestring = str_replace('{leftsymbol}', '', $pricestring);
        }
        //$pricestring = str_replace('{price}',money_format("%i",$price*$record['currency_rate']),$pricestring);
        $formatedprice = str_replace(".00",'',number_format(round($price * $record['currency_rate']), 2));
        $pricestring = str_replace('{price}',$formatedprice, $pricestring);
        return $pricestring;
    }

    public static function getDefaultCurrency() {
        $srch = Self::getSearchObject();
        $srch->addCondition('currency_id', '=', self::$defaultCurrencyId);
        $rs = $srch->getResultSet();
        return  FatApp::getDb()->fetch($rs);
    }

    public static function price($price) {
        $srch = Self::getSearchObject();
        $srch->addCondition('currency_id', '=', Info::getCurrentCurrency());
        $rs = $srch->getResultSet();
        $record = FatApp::getDb()->fetch($rs);
        $pricestring = $price * $record['currency_rate'];
        return round($pricestring);
    }

    public static function reversePrice($price) {
        $srch = Self::getSearchObject();
        $srch->addCondition('currency_id', '=', Info::getCurrentCurrency());
        $rs = $srch->getResultSet();
        $record = FatApp::getDb()->fetch($rs);
        return money_format("%i", $price / $record['currency_rate']);
    }

    public static function getCurrentCurrencyForForm() {
        $srch = Self::getSearchObject();
        $srch->addFld('currency_id');
        $srch->addFld('currency_name');
        $rs = $srch->getResultSet();

        return FatApp::getDb()->fetchAllAssoc($rs);
    }

}
