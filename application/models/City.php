<?php

class City extends MyAppModel {

    const DB_TBL = 'tbl_cities';
    const DB_TBL_PREFIX = 'city_';

    public function __construct($countryId = 0) {
        $cityId = FatUtility::convertToType($countryId, FatUtility::VAR_INT);
        parent::__construct(static::DB_TBL, static::DB_TBL_PREFIX . 'id', $cityId);
        $this->objMainTableRecord->setSensitiveFields(array());
    }

    public static function getSearchObject() {
        $srch = new SearchBase(static::DB_TBL);
        $srch->joinTable(Country::DB_TBL, 'INNER JOIN', Country::DB_TBL_PREFIX . "id=" . self::DB_TBL_PREFIX . "country_id");
        $srch->addOrder(static::DB_TBL_PREFIX . 'name');
        return $srch;
    }

    public static function getCities() {
        $srch = self::getSearchObject();
        $srch->addOrder(static::DB_TBL_PREFIX . 'name');
        $srch->addFld(static::DB_TBL_PREFIX . "id");
        $srch->addFld(static::DB_TBL_PREFIX . "name");
        $records = FatApp::getDb()->fetchAllAssoc($srch->getResultSet());
        return $records;
    }

    public static function getCityById($id) {
        $srch = self::getSearchObject();
		$srch->joinTable(Region::DB_TBL, 'INNER JOIN', Region::DB_TBL_PREFIX . "id=" . Country::DB_TBL_PREFIX . "region_id");
        $srch->addCondition(self::DB_TBL_PREFIX . "id", '=', $id);
        $srch->addCondition(Country::DB_TBL_PREFIX . "active", '=', 1);
        $srch->addCondition(self::DB_TBL_PREFIX . "active", '=', 1);
        $records = FatApp::getDb()->fetch($srch->getResultSet());
        return $records;
    }

    public static function getAllCitiesByCountryId($id) {
        $srch = self::getSearchObject();
        $srch->addCondition(Country::DB_TBL_PREFIX . "id", '=', $id);
        $srch->addCondition(Country::DB_TBL_PREFIX . "active", '=', 1);
        $srch->addCondition(self::DB_TBL_PREFIX . "active", '=', 1);
        $srch->addFld(static::DB_TBL_PREFIX . "id");
        $srch->addFld(static::DB_TBL_PREFIX . "name");
        $records = FatApp::getDb()->fetchAllAssoc($srch->getResultSet());
        return $records;
    }

    public static function getFeaturedCities($attr = array()) {
        $srch = self::getSearchObject();
        $srch->doNotCalculateRecords();
        $srch->doNotLimitRecords();
        $srch->addCondition(self::DB_TBL_PREFIX . 'active', '=', 1);
        $srch->addCondition(self::DB_TBL_PREFIX . 'featured', '=', 1);
        if (!empty($attr)) {
            if (is_array($attr)) {
                $srch->addMultipleFields($attr);
            }
        }
        $srch->addOrder(self::DB_TBL_PREFIX . 'display_order', 'asc');
        $rs = $srch->getResultSet();

        return FatApp::getDb()->fetchAll($rs, 'city_id');
    }

    public static function getDestinations() {
        $cacheKey = CACHE_HOME_FEATURED_CITIES;

      //  if ($list = FatCache::get($cacheKey, CONF_DEF_CACHE_TIME)) {
       //     return json_decode($list,true);
      //  }

        // Added Indhiyan Oct 27, 2017
        // SELECT count(DISTINCT activities.activity_id) as activities, cities.* FROM tbl_cities AS cities INNER JOIN tbl_activities AS activities ON cities.city_id = activities.activity_city_id 
        // WHERE cities.city_active=1 AND cities.city_featured=1 AND activities.activity_active = 1 AND activities.activity_confirm = 1 AND activities.activity_state >= 2 AND activities.activity_end_Date > NOW() 
        // GROUP BY cities.city_id 
        // ORDER BY cities.city_display_order ASC
        $srch = self::getSearchObject();
		$srch->joinTable(Activity::DB_TBL, "INNER JOIN", CITY::DB_TBL_PREFIX . "id = " . Activity::DB_TBL_PREFIX . "city_id");
        $srch->joinTable(Country::DB_TBL, 'INNER JOIN', Country::DB_TBL_PREFIX . "id=" . self::DB_TBL_PREFIX . "country_id");
        $srch->addCondition(self::DB_TBL_PREFIX . 'active', '=', 1);
  //       $srch->addCondition(self::DB_TBL_PREFIX . 'featured', '=', 1);
		// $srch->addCondition(Activity::DB_TBL_PREFIX . 'active', '=', 1);
  //       $srch->addCondition(Activity::DB_TBL_PREFIX . 'confirm', '=', 1);
	 //    $srch->addCondition(Activity::DB_TBL_PREFIX . 'state', '>=', 2);
	    // $srch->addCondition(Activity::DB_TBL_PREFIX . 'end_Date', '>', "mysql_func_now()","AND",true);
        $srch->addOrder(self::DB_TBL_PREFIX . 'display_order', 'asc');
		$srch->addGroupBy('city_id');
        $srch->setFetchRecordCount(12);
        $srch->doNotCalculateRecords();
		$srch->addMultipleFields(array('tbl_cities.*', 'tbl_countries.country_name as country_name'));
        $rs = $srch->getResultSet();
        
        /* Check resultset rows > 0 */
        $records = FatApp::getDb()->fetchAll($rs);
        if (count($records) > 0) {            
            FatCache::set($cacheKey, json_encode($records,true));
        }
        return $records;
    }

    public static function getFeaturedExperiences() {

        $acts = new Activity();
        $srch = $acts->getSearchObject();
        $srch->joinTable('tbl_users', 'inner join', 'activity_user_id = user_id and user_type = 1 and user_active = 1');
        $srch->joinTable('tbl_cities', 'inner join', 'activity_city_id = city_id and city_active = 1');
        $srch->joinTable('tbl_countries', 'inner join', 'city_country_id = country_id and country_active = 1');
        $srch->joinTable('tbl_activity_languages', 'left join', 'activity_id = activitylanguage_activity_id');
        $srch->joinTable('tbl_languages', 'left join', 'language_id = activitylanguage_language_id');
        $srch->joinTable('tbl_services', 'left join', 'schild.service_id = activity_category_id', 'schild');
        $srch->joinTable('tbl_services', 'left join', 'schild.service_parent_id = sparent.service_id', 'sparent');
        $srch->joinTable('tbl_reviews', 'left join', 'ar.review_type_id = activity_id AND review_active=1 AND review_type=0', 'ar');
        $srch->addFld('schild.service_name as childservice_name');
        $srch->addFld('sparent.service_name as parentservice_name');
        $srch->addFld('sparent.service_id as parentservice_id');
        $srch->addCondition('activity_confirm', '=', 1);
        $srch->addCondition('activity_active', '=', 1);
        $srch->addCondition('city_featured', '=', 1);
        $srch->addGroupBy('activitylanguage_activity_id');
        $srch->addMultipleFields(array('tbl_users.*', 'tbl_activities.*', 'SUBSTRING_INDEX(group_concat(language_name separator ","),",",3) as act_lang,sum(`review_rating`) as rating,count(review_id) as ratingcounter,count(DISTINCT review_id) as reviews', 'tbl_cities.city_featured as city_featured')); 
        $srch->setFetchRecordCount(12);
        $srch->doNotCalculateRecords();       
        $rs = $srch->getResultSet();

        /* Check resultset rows > 0 */
        $records = FatApp::getDb()->fetchAll($rs);
        if (count($records) > 0) {
            foreach ($records as $k => $record) {
                $records[$k]['booking_status'] = Activity::isActivityOpen($record);
            }
        }
        return $records;
    }

}
